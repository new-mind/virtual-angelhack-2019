This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`
Run development server [http://localhost:3000](http://localhost:3000)

### `npm run graphql`
Run graphql server [http://localhost:4000](http://localhost:4000)
