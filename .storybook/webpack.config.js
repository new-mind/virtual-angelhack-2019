const path = require('path');
const getCSSModuleLocalIdent = require('react-dev-utils/getCSSModuleLocalIdent');

module.exports = async ({ config }) => {
  config.module.rules.push({
    test: /\.scss$/,
    exclude: /\.module\.(scss|sass)$/,
    use: [
      'style-loader',
      {
        loader: 'css-loader',
        options: {
          importLoaders: 2,
        },
      },
      'sass-loader',
    ],
    include: path.resolve(__dirname, '../src'),
  }, {
    test: /\.(scss|sass)$/,
    use: [
      'style-loader',
      {
        loader: 'css-loader',
        options: {
          importLoaders: 2,
          modules: true,
          getLocalIdent: getCSSModuleLocalIdent,
        },
      },
      'sass-loader',
    ],
    include: path.resolve(__dirname, '../src'),
    sideEffects: true,
  });

  config.resolve.alias = {
    'react-native': 'react-native-web',
    '@lms': path.join(__dirname, '../src'),
    '@assets': '@lms/assets'
  };

  return config;
};
