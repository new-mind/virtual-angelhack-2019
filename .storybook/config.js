import { configure } from '@storybook/react';

import '!style-loader!css-loader!sass-loader!@lms/styles/base.scss';

const req = require.context('../src', true, /\.stories\.js$/);

function loadStories() {
  req.keys().forEach(filename => req(filename));
}

configure(loadStories, module);
