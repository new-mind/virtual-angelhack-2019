import React from 'react';
import { Formik } from 'formik';
import cn from 'classnames';

import Button from '@lms/components/blocks/Button';
import Form from '@lms/components/blocks/forms';

import styles from './styles.module.scss';

class ProfileForm extends React.PureComponent {
  render() {
    const { name, surname, phone, age } = this.props;
    return (
      <Formik onSubmit={this.props.onSubmit}>
        {({
          handleSubmit,
          handleChange
        }) => (
          <Form className={cn(styles.profile, "my-5")} noValidate onSubmit={handleSubmit}>
            <Form.Group>
              <Form.CustomControl
                type="text"
                name="name"
                defaultValue={name}
                onChange={handleChange}
                placeholder="First name"/>
            </Form.Group>
            <Form.Group>
              <Form.CustomControl
                type="text"
                name="surname"
                defaultValue={surname}
                onChange={handleChange}
                placeholder="Second name"/>
            </Form.Group>
            <Form.Group>
              <Form.CustomControl
                type="text"
                name="phone"
                defaultValue={phone}
                onChange={handleChange}
                placeholder="Mobile phone"/>
            </Form.Group>
            <Form.Group>
              <Form.CustomControl
                type="text"
                name="age"
                defaultValue={age}
                onChange={handleChange}
                placeholder="Age"/>
            </Form.Group>
            <Form.Group>
              <Button type="submit">Save</Button>
            </Form.Group>
          </Form>
        )}
      </Formik>
    )
  }
}

export default ProfileForm;
