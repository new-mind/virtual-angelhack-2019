import React from 'react';
import Card from '@lms/components/blocks/Card';

import styles from './styles.module.scss';
import img from '@lms/assets/bg.png';

class Image extends React.PureComponent {
  render() {
    const { imageUrl } = this.props;

    return (
      <div className={styles.cardImg}>
        <Card.Img
          className={styles.cardImg}
          src={imageUrl || img} alt="Card image" />
      </div>
    );
  }
}

export default Image;
