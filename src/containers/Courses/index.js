import React from 'react';
import { Col } from 'react-bootstrap';
import * as _ from 'lodash';
import cn from 'classnames';
import { NavLink } from 'react-router-dom';

import Card from '@lms/components/blocks/Card';
import Image from '@lms/containers/Courses/Image';

import styles from './styles.module.scss';

const BADGES = [
  { url: require('./images/1.svg'), background: '' },
  { url: require('./images/2.svg'), background: '' },
  { url: require('./images/3.svg'), background: '' },
  { url: require('./images/4.svg'), background: '' },
];

class Courses extends React.Component {
  render() {
    const { courses, variant } = this.props;
    let layout = (i) => ({
      xs: 12,
      lg: Math.ceil(i / 2) % 2 !== 1 ? 7 : 5
    });
    if (variant === 'equal') {
      layout = (i) => ({
        xs: 12,
        lg: 6
      })
    }

    return (
      <>
        {_.map(courses, (course, i) => (
          <Col {...layout(i)} key={course.id}>
            <Card className={cn(styles.card, 'mb-4')} >
              <Image imageUrl={BADGES[_.random(0, BADGES.length - 1)].url} />
              <Card.ImgOverlay>
                <Card.Title><NavLink to={`/courses/${course.id}`}>{course.title}</NavLink></Card.Title>
                <Card.Text className={styles.text}>{_.truncate(course.description, {length: 200})}</Card.Text>
                <div className={styles.buttons}>
                  {this.props.children && this.props.children(course)}
                </div>
              </Card.ImgOverlay>
            </Card>
          </Col>
        ))}
      </>
    )
  }
}

export default Courses;
