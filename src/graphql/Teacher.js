import gql from 'graphql-tag';

export const listTeachers = gql`
  query ListTeachers($filter: TeacherFilterInput, $limit: Int, $nextToken: String) {
    listTeachers(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        email
        name
        surname
        phone
        age
      }
      nextToken
    }
  }
`;

export const inviteTeacher = gql`
  mutation InviteTeacher($input: InviteTeacherInput) {
    inviteTeacher(input: $input) {
      id
      email
      name
      surname
      phone
      age
    }
  }
`;

export const deleteTeacher = gql`
  mutation DeleteTeacher($input: DeleteTeacherInput) {
    deleteTeacher(input: $input) {
      id
      email
      name
      surname
      phone
      age
    }
  }
`;

export const optimistic = {
  __typename: 'Teacher',
  id: -1,
  email: null,
  name: null,
  surname: null,
  phone: null,
  age: null
};
