const LessonOptimistic = {
  __typename: 'Lesson',
  id: -1,
  version: 0,
  draft: null,
  draftVersion: null,
  content: null,
  title: null,
  description: null,
  courseLessonsId: null,
  homework: false,
  contentOrder: []
}
export default LessonOptimistic
