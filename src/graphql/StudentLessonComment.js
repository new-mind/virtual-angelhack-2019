import moment from 'moment';

export const optimistic = {
  __typename: 'StudentLessonComment',
  id: -1,
  userId: -1,
  user: null,
  createdAt: moment().format(),
  organizationId: null
}
