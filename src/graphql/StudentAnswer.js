import * as _ from 'lodash';

// Optimistics
const LinkWidgetItem = (item) => {
  return {
    __typename: 'LinkItem',
    id:         _.get(item, 'id', -1),
    title:      _.get(item, 'title', null),
    url:        _.get(item, 'url', null),
  }
}

const AttachmentWidgetItem = (item) => {
  return {
    __typename: 'AttachmentItem',
    id:         _.get(item, 'id', -1),
    title:      _.get(item, 'title',  null),
    fileName:   _.get(item, 'fileName', null),
    fileKey:    _.get(item, 'fileKey', null),
    identityId: _.get(item, 'identityId', null),
  }
}

export const optimistic = (answer) => {
  switch (answer.type) {
    case 'TextWidget':
      return {
        __typename: 'StudentAnswer_TextWidget',
        id: -1,
        sectionId: -1,
        lessonId: -1,
        organizationId: -1,
        userId: -1,
        createdAt: null,
        updatedAt: null,
        rev: -1,

        data: null,

        ...answer
      }
    case 'LinkWidget':
      return {
        __typename: 'StudentAnswer_LinkWidget',
        id: -1,
        sectionId: -1,
        lessonId: -1,
        organizationId: -1,
        userId: -1,
        createdAt: null,
        updatedAt: null,
        rev: -1,

        ...answer,
        items: _.map(answer.items, LinkWidgetItem),
      }
    case 'AttachmentWidget':
      console.log(_.map(answer.items, AttachmentWidgetItem));
      return {
        __typename: 'StudentAnswer_AttachmentWidget',
        id: -1,
        sectionId: -1,
        lessonId: -1,
        userId: -1,
        organizationId: -1,
        createdAt: null,
        updatedAt: null,
        rev: -1,

        ...answer,
        items: _.map(answer.items, AttachmentWidgetItem),
      }
    default:
      throw new Error(`Unknown response type ${answer.__typename}`);
  }
}
