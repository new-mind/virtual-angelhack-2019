const CourseOptimistic = {
  __typename: 'Course',
  id: -1,
  title: null,
  description: null,
  owner: null,
  imageUrl: null,

  lessons: null,
  students: [],
  teachers: []
};

export default CourseOptimistic
