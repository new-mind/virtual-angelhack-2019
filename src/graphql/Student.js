import gql from 'graphql-tag';

export const listStudents = gql`
query ListStudents($filter: StudentFilterInput, $limit: Int, $nextToken: String, $organizationId: ID!) {
  listStudents(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      email
      name
      surname
      phone
      age

      courseIds(filter: {
        organizationId: $organizationId
      })
    }
    nextToken
  }
}
`;

export const inviteStudent = gql`
mutation InviteStudent($input: InviteStudentInput) {
  inviteStudent(input: $input) {
    id
    email
    name
    surname
    phone
    age
    courseIds
  }
}
`;

export const deleteStudent = gql`
mutation DeleteStudent($input: DeleteStudentInput) {
  deleteStudent(input: $input) {
    id
    email
    name
    surname
    phone
    age
  }
}
`;

export const addCourses = gql`
mutation AddCourses($id: ID!, $input: [ID!]) {
  Student(id: $id) {
    id
    addCourses(input: $input)
  }
}
`

export const deleteCourses = gql`
mutation DeleteCourses($id: ID!, $input: [ID!]) {
  Student(id: $id) {
    id
    deleteCourses(input: $input)
  }
}
`

export const fragment = gql`
  fragment Fragment on Student {
    courseIds(filter: {
      organizationId: $organizationId
    })
  }
`

export const optimistic = {
  __typename: 'Student',
  id: -1,
  email: null,
  name: null,
  surname: null,
  phone: null,
  age: null,
  courseIds: []
}
