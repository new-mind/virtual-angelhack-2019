import * as _ from 'lodash';

// Optimistics
const LinkWidgetItem = (item) => {
  return {
    __typename: 'LinkItem',
    id: -1,
    title: null,
    url: null,
    ...item
  }
};

const AttachmentWidgetItem = (item) => {
  return {
    __typename: 'AttachmentItem',
    id: -1,
    title: null,
    fileName: null,
    fileKey: null,
    identityId: null,
    ...item
  }
};

const MultiChoiceItem = (item) => {
  return {
    __typename: 'MultiChoiceItem',
    id: -1,
    correct: false,
    text: null,
    ...item
  }
};

export const optimistic = (section) => {
  const type = section.type || section.__typename;
  switch (type) {
    case 'TextWidget':
      return {
        __typename: type,
        id: -1,
        title: null,
        data: null,
        ...section
      };
    case 'VideoWidget':
      return {
        __typename: type,
        id: -1,
        title: null,
        url: null,
        width: null,
        height: null,
        ...section
      };
    case 'LinkWidget':
      return {
        __typename: type,
        id: -1,
        title: null,
        ...section,
        items: _.map(section.items, LinkWidgetItem),
      };
    case 'MultiChoiceWidget':
      return {
        __typename: type,
        id: -1,
        title: null,
        question: null,
        imageKey: null,
        ...section,
        answers: _.map(section.answers, MultiChoiceItem),
      };
    case 'AttachmentWidget':
      return {
        __typename: type,
        id: -1,
        title: null,
        ...section,
        items: _.map(section.items, AttachmentWidgetItem),
      };
    case 'ImageWidget':
      return {
        __typename: type,
        id: -1,
        title: null,
        url: null,
        width: null,
        height: null,
        ...section
      };
    case 'ResponseFormWidget':
      return {
        __typename: type,
        id: -1,
        title: null,
        formType: null,
        responses: null,
        ...section
      };
    case 'AudioWidget':
      return {
        __typename: type,
        id: -1,
        title: null,
      };
    case 'FillBlankWidget':
      return {
        __typename: type,
        id: -1,
        title: null,
      };
    case 'FlashCardsWidget':
      return {
        __typename: type,
        id: -1,
        title: null,
      };
    default:
      throw new Error(`Unknown section type ${type}`);
  }
};
