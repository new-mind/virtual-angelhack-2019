import gql from 'graphql-tag';

export const fragments = {
  TextWidget: gql`
    fragment TextWidget on TextWidget {
      id
      title
      data
    }
  `,

  VideoWidget: gql`
    fragment VideoWidget on VideoWidget {
      id
      title
      url
      width
      height
    }
  `,

  LinkWidget: gql`
    fragment LinkWidget on LinkWidget {
      id
      title
      items {
        id
        url
        title
      }
    }
  `,

  MultiChoiceWidget: gql`
    fragment MultiChoiceWidget on MultiChoiceWidget {
      id
      title
      imageKey
      question
      answers {
        id
        correct
        text
      }
    }
  `,

  AttachmentWidget: gql`
    fragment AttachmentWidget on AttachmentWidget {
      id
      title
      items {
        id
        fileKey
        fileName
        identityId
        title
      }
    }
  `,

  ImageWidget: gql`
    fragment ImageWidget on ImageWidget {
      id
      title
      url
      width
      height
    }
  `,

  ResponseFormWidget: gql`
    fragment ResponseFormWidget on ResponseFormWidget {
      id
      title
      formType
    }
  `,

  ResponseFormWidgetWithAnswers: gql`
    fragment ResponseFormWidgetWithAnswers on ResponseFormWidget {
      id
      title
      formType
      answers(lessonId: $id) { #TODO
        ...on StudentAnswer_TextWidget {
          id
          rev
          createdAt
          updatedAt
          data
        }
        ...on StudentAnswer_LinkWidget {
          id
          rev
          createdAt
          updatedAt
          items {
            id
            title
            url
          }
        }
        ...on StudentAnswer_AttachmentWidget {
          id
          rev
          createdAt
          updatedAt
          items {
            id
            title
            fileKey
            fileName
            identityId
          }
        }
      }
    }
  `
};

export const getLesson = gql`
  query GetLesson($id: ID!) {
    getLesson(id: $id) {
      id
      courseId
      title
      description
      homework
      dirty
      rev
      course {
        id
        title
      }

      content {
        ...TextWidget
        ...VideoWidget
        ...LinkWidget
        ...MultiChoiceWidget
        ...AttachmentWidget
        ...ImageWidget
        ...ResponseFormWidget
      }
    }
  }
  ${fragments.TextWidget}
  ${fragments.VideoWidget}
  ${fragments.LinkWidget}
  ${fragments.MultiChoiceWidget}
  ${fragments.AttachmentWidget}
  ${fragments.ImageWidget}
  ${fragments.ResponseFormWidget}
`;

export const createLesson = gql`
  mutation CreateLesson($input: CreateLessonInput!) {
    createLesson(input: $input) {
      id
      courseId
      organizationId
      title
      description
      homework
      dirty
      rev
    }
  }
`;

export const updateLesson = gql`
  mutation UpdateLesson($input: UpdateLessonInput!) {
    updateLesson(input: $input) {
      id
      courseId
      title
      description
      homework
      dirty
      rev
    }
  }
`;

export const publishLesson = gql`
  mutation PublishLesson($id: ID!) {
    publishLesson(id: $id) {
      id
      courseId
      title
      description
      homework
      dirty
      rev
    }
  }
`

export const deleteLesson = gql`
  mutation DeleteLesson($id: ID!) {
    deleteLesson(id: $id) {
      id
      courseId
      organizationId
      title
      description
      homework
      dirty
      rev
    }
  }
`;

export const addSection = gql`
  mutation AddSection($id: ID!, $input: CreateSectionWithPosInput) {
    Lesson(id: $id) {
      addSection(input: $input) {
        section {
          ...TextWidget
          ...VideoWidget
          ...LinkWidget
          ...MultiChoiceWidget
          ...AttachmentWidget
          ...ImageWidget
          ...ResponseFormWidget
        }
        position
      }
    }
  }
  ${fragments.TextWidget}
  ${fragments.VideoWidget}
  ${fragments.LinkWidget}
  ${fragments.MultiChoiceWidget}
  ${fragments.AttachmentWidget}
  ${fragments.ImageWidget}
  ${fragments.ResponseFormWidget}
`;

export const deleteSection = gql`
  mutation DeleteSection($id: ID!, $sectionId: ID!) {
    Lesson(id: $id) {
      deleteSection(id: $sectionId) {
        section {
          ... on TextWidget {
            id
          }
          ... on VideoWidget {
            id
          }
          ... on LinkWidget {
            id
          }
          ... on MultiChoiceWidget {
            id
          }
          ... on AttachmentWidget {
            id
          }
          ... on ImageWidget {
            id
          }
          ... on ResponseFormWidget {
            id
          }
        }
        position
      }
    }
  }
`;

export const updateSection = gql`
  mutation UpdateSection($input: UpdateSectionInput!) {
    updateSection(input: $input) {
      ...TextWidget
      ...VideoWidget
      ...LinkWidget
      ...MultiChoiceWidget
      ...AttachmentWidget
      ...ImageWidget
      ...ResponseFormWidget
    }
  }
  ${fragments.TextWidget}
  ${fragments.VideoWidget}
  ${fragments.LinkWidget}
  ${fragments.MultiChoiceWidget}
  ${fragments.AttachmentWidget}
  ${fragments.ImageWidget}
  ${fragments.ResponseFormWidget}
`;

export const optimistic = {
  __typename: 'Lesson',
  id: -1,
  courseId: -1,
  organizationId: -1,
  title: null,
  description: null,
  homework: false,
  dirty: false,
  rev: -1
}
