export const getLesson = `query GetLesson($id: ID!) {
  getLesson(id: $id) {
    id
    courseLessonsId
    title
    content {
      items {
        id
        widget {
          ... on VideoWidget {
            url
            width
            height
          }
        }
      }
      nextToken
    }
    draft {
      id
      lesson {
        id
        courseLessonsId
        title
      }
      courseLessonsId
      content {
        id
      }
    }
  }
}
`;

export const getStudentInOrganization = `query GetStudentInOrganization($id: ID!) {
  getStudentInOrganization(id: $id) {
    id
    courseAssignments {
      items {
        id
        teacher
        createdAt
        updatedAt
        finishedAt
        course {
          id
          title
          description
          imageUrl
        }
      }
      nextToken
    }
    lessonAssignments {
      items {
        id
        teacher
        createdAt
        updatedAt
        finishedAt
        lesson {
          id
          title
          description
          courseLessonsId
        }
      }
      nextToken
    }
  }
}
`;
export const listStudentsInOrganization = `query ListStudentsInOrganization(
  $filter: ModelStudentInOrganizationFilterInput
  $limit: Int
  $nextToken: String
) {
  listStudentsInOrganization(
    filter: $filter
    limit: $limit
    nextToken: $nextToken
  ) {
    items {
      id
      courseAssignments {
        items {
          id
          course {
            id
            title
            description
            imageUrl
          }
        }
        nextToken
      }
      lessonAssignments {
        items {
          id
          teacher
          createdAt
          updatedAt
          finishedAt
          lesson {
            id
            title
            description
            courseLessonsId
          }
        }
        nextToken
      }
    }
    nextToken
  }
}
`;
