import * as _ from 'lodash';

// Optimistics
const LinkWidgetItem = (item) => {
  return {
    __typename: 'LinkItem',
    id: -1,
    title: null,
    url: null,
    ...item
  }
}

const AttachmentWidgetItem = (item) => {
  return {
    __typename: 'AttachmentItem',
    id: -1,
    title: null,
    fileName: null,
    fileKey: null,
    identityId: null,
    ...item
  }
}

export const optimistic = (response) => {
  const type = response.type || response.__typename;
  switch (type) {
    case 'TextWidget':
      return {
        __typename: 'TextResponse',
        id: -1,
        sectionId: -1,
        lessonId: -1,
        updatedAt: null,
        organizationId: -1,
        type,
        rev: -1,

        data: null,

        ...response
      }
    case 'LinkWidget':
      return {
        __typename: 'LinkResponse',
        id: -1,
        sectionId: -1,
        lessonId: -1,
        updatedAt: null,
        organizationId: -1,
        type,
        rev: -1,

        ...response,
        items: _.map(response.items, LinkWidgetItem),
      }
    case 'AttachmentWidget':
      return {
        __typename: 'AttachmentResponse',
        id: -1,
        sectionId: -1,
        lessonId: -1,
        updatedAt: null,
        organizationId: -1,
        type,
        rev: -1,

        ...response,
        items: _.map(response.items, AttachmentWidgetItem),
      }
    default:
      throw new Error(`Unknown response type ${type}`);
  }
}
