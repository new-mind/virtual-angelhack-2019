import gql from 'graphql-tag';

export * from './Course';
export * from './Lesson';
export * from './Student';
export * from './Section';
export * from './Teacher';

export const optimistic = {
  Course: require('./Course').optimistic,
  Lesson: require('./Lesson').optimistic,
  Student: require('./Student').optimistic,
  StudentLessonComment: require('./StudentLessonComment').optimistic,
  StudentAnswer: require('./StudentAnswer').optimistic,
  Teacher: require('./Teacher').optimistic,
  Section: require('./Section').optimistic,
  Response: require('./Response').optimistic
}

export const getRoles = gql`
  query GetRoles($id: ID!) {
    getRoles(id: $id) {
      admin
      teacher
      student
    }
  }
`;

export const getUserWithRoles = gql`
  query GetUser($id: ID!) {
    getUser(id: $id) {
      id
      name
      surname
      age
      phone
      email
    }
    getRoles(id: $id) {
      admin
      teacher
      student
    }
  }
`

export const getUser = gql`
  query GetUser($id: ID!) {
    getUser(id: $id) {
      id
      name
      surname
      age
      email
    }
  }
`;
