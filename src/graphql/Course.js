import gql from 'graphql-tag';

export const listCourses = gql`
query ListCourses(
  $filter: CourseFilterInput
  $limit: Int
  $nextToken: String
) {
  listCourses(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      title
      description
      imageUrl
    }
    nextToken
  }
}
`;

export const createCourse = gql`
mutation CreateCourse($input: CreateCourseInput!) {
  createCourse(input: $input) {
    id
    title
    description
    lessons {
      items {
        id
        title
        description
        homework
      }
      nextToken
    }
    imageUrl
  }
}
`;

export const deleteCourse = gql`
mutation DeleteCourse($id: ID!) {
  deleteCourse(id: $id) {
    id
    title
    description
    imageUrl
  }
}
`;

export const getCourse = gql`
query GetCourse($id: ID!) {
  getCourse(id: $id) {
    id
    title
    description
    imageUrl
    lessons {
      items {
        id
        title
        description
        homework
        dirty
        rev
      }
      nextToken
    }
  }
}
`;

export const updateCourse = gql`
mutation UpdateCourse($input: UpdateCourseInput!) {
  updateCourse(input: $input) {
    id
    title
    description
    imageUrl
    lessons {
      items {
        id
        title
        description
        homework
      }
      nextToken
    }
  }
}
`;

export const optimistic = {
  __typename: 'Course',
  id: -1,
  organizationId: -1,
  title: null,
  description: null,
  imageUrl: null,
  lessons: []
};
