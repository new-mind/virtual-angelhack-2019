// eslint-disable
// this is an auto generated file. This will be overwritten

export const createEmployee = `mutation CreateEmployee($input: CreateEmployeeInput) {
  createEmployee(input: $input) {
    username
    email
    name
    surname
    phone
    age
    status
    enabled
  }
}
`;
export const updateEmployee = `mutation UpdateEmployee($input: UpdateEmployeeInput) {
  updateEmployee(input: $input) {
    username
    email
    name
    surname
    phone
    age
    status
    enabled
  }
}
`;
export const setEmployeePassword = `mutation SetEmployeePassword($input: PasswordEmployeeInput) {
  setEmployeePassword(input: $input) {
    username
    email
    name
    surname
    phone
    age
    status
    enabled
  }
}
`;
export const disableEmployee = `mutation DisableEmployee($username: String!) {
  disableEmployee(username: $username)
}
`;
export const enableEmployee = `mutation EnableEmployee($username: String!) {
  enableEmployee(username: $username)
}
`;
export const createStudent = `mutation CreateStudent($input: CreateStudentInput) {
  createStudent(input: $input) {
    username
    email
    enabled
    name
    surname
    phone
    age
    status
  }
}
`;
export const deleteStudent = `mutation DeleteStudent($input: DeleteStudentInput) {
  deleteStudent(input: $input) {
    username
    email
    enabled
    name
    surname
    phone
    age
    status
  }
}
`;
export const updateStudent = `mutation UpdateStudent($inptu: UpdateStudentInput) {
  updateStudent(inptu: $inptu) {
    username
    email
    enabled
    name
    surname
    phone
    age
    status
  }
}
`;
export const createCourse = `mutation CreateCourse($input: CreateCourseInput!) {
  createCourse(input: $input) {
    id
    title
    description
    owner
    lessons {
      items {
        id
        courseLessonsId
        title
        description
        contentOrder
        homework
      }
      nextToken
    }
    teachers
    imageUrl
  }
}
`;
export const updateCourse = `mutation UpdateCourse($input: UpdateCourseInput!) {
  updateCourse(input: $input) {
    id
    title
    description
    owner
    lessons {
      items {
        id
        courseLessonsId
        title
        description
        contentOrder
        homework
      }
      nextToken
    }
    teachers
    imageUrl
  }
}
`;
export const deleteCourse = `mutation DeleteCourse($input: DeleteCourseInput!) {
  deleteCourse(input: $input) {
    id
    title
    description
    owner
    lessons {
      items {
        id
        courseLessonsId
        title
        description
        contentOrder
        homework
      }
      nextToken
    }
    teachers
    imageUrl
  }
}
`;
export const createTeacherCourseAssignment = `mutation CreateTeacherCourseAssignment(
  $input: CreateTeacherCourseAssignmentInput!
) {
  createTeacherCourseAssignment(input: $input) {
    teacher {
      id
      admin
      teacher
    }
    course {
      id
      title
      description
      owner
      lessons {
        nextToken
      }
      teachers
      imageUrl
    }
    createdAt
    updatedAt
    finishedAt
  }
}
`;
export const updateTeacherCourseAssignment = `mutation UpdateTeacherCourseAssignment(
  $input: UpdateTeacherCourseAssignmentInput!
) {
  updateTeacherCourseAssignment(input: $input) {
    teacher {
      id
      admin
      teacher
    }
    course {
      id
      title
      description
      owner
      lessons {
        nextToken
      }
      teachers
      imageUrl
    }
    createdAt
    updatedAt
    finishedAt
  }
}
`;
export const deleteTeacherCourseAssignment = `mutation DeleteTeacherCourseAssignment(
  $input: DeleteTeacherCourseAssignmentInput!
) {
  deleteTeacherCourseAssignment(input: $input) {
    teacher {
      id
      admin
      teacher
    }
    course {
      id
      title
      description
      owner
      lessons {
        nextToken
      }
      teachers
      imageUrl
    }
    createdAt
    updatedAt
    finishedAt
  }
}
`;
export const createLesson = `mutation CreateLesson($input: CreateLessonInput!) {
  createLesson(input: $input) {
    id
    courseLessonsId
    title
    description
    content {
      items {
        id
        widget
      }
      nextToken
    }
    contentOrder
    homework
    draft {
      id
      lesson {
        id
        courseLessonsId
        title
        description
        contentOrder
        homework
      }
      courseLessonsId
      contentOrder
      content {
        id
        widget
      }
    }
  }
}
`;
export const updateLesson = `mutation UpdateLesson($input: UpdateLessonInput!) {
  updateLesson(input: $input) {
    id
    courseLessonsId
    title
    description
    content {
      items {
        id
        widget
      }
      nextToken
    }
    contentOrder
    homework
    draft {
      id
      lesson {
        id
        courseLessonsId
        title
        description
        contentOrder
        homework
      }
      courseLessonsId
      contentOrder
      content {
        id
        widget
      }
    }
  }
}
`;
export const deleteLesson = `mutation DeleteLesson($input: DeleteLessonInput!) {
  deleteLesson(input: $input) {
    id
    courseLessonsId
    title
    description
    content {
      items {
        id
        widget
      }
      nextToken
    }
    contentOrder
    homework
    draft {
      id
      lesson {
        id
        courseLessonsId
        title
        description
        contentOrder
        homework
      }
      courseLessonsId
      contentOrder
      content {
        id
        widget
      }
    }
  }
}
`;
export const createLessonDraft = `mutation CreateLessonDraft($input: CreateLessonDraftInput!) {
  createLessonDraft(input: $input) {
    id
    lesson {
      id
      courseLessonsId
      title
      description
      content {
        nextToken
      }
      contentOrder
      homework
      draft {
        id
        courseLessonsId
        contentOrder
      }
    }
    courseLessonsId
    contentOrder
    content {
      id
      widget
    }
  }
}
`;
export const updateLessonDraft = `mutation UpdateLessonDraft($input: UpdateLessonDraftInput!) {
  updateLessonDraft(input: $input) {
    id
    lesson {
      id
      courseLessonsId
      title
      description
      content {
        nextToken
      }
      contentOrder
      homework
      draft {
        id
        courseLessonsId
        contentOrder
      }
    }
    courseLessonsId
    contentOrder
    content {
      id
      widget
    }
  }
}
`;
export const deleteLessonDraft = `mutation DeleteLessonDraft($input: DeleteLessonDraftInput!) {
  deleteLessonDraft(input: $input) {
    id
    lesson {
      id
      courseLessonsId
      title
      description
      content {
        nextToken
      }
      contentOrder
      homework
      draft {
        id
        courseLessonsId
        contentOrder
      }
    }
    courseLessonsId
    contentOrder
    content {
      id
      widget
    }
  }
}
`;
export const createSection = `mutation CreateSection($input: CreateSectionInput!) {
  createSection(input: $input) {
    id
    widget
  }
}
`;
export const updateSection = `mutation UpdateSection($input: UpdateSectionInput!) {
  updateSection(input: $input) {
    id
    widget
  }
}
`;
export const deleteSection = `mutation DeleteSection($input: DeleteSectionInput!) {
  deleteSection(input: $input) {
    id
    widget
  }
}
`;
export const createStudentCourseAssignment = `mutation CreateStudentCourseAssignment(
  $input: CreateStudentCourseAssignmentInput!
) {
  createStudentCourseAssignment(input: $input) {
    id
    student {
      id
      courseAssignments {
        nextToken
      }
      lessonAssignments {
        nextToken
      }
    }
    teacher
    course {
      id
      title
      description
      owner
      lessons {
        nextToken
      }
      teachers
      imageUrl
    }
    createdAt
    updatedAt
    finishedAt
  }
}
`;
export const updateStudentCourseAssignment = `mutation UpdateStudentCourseAssignment(
  $input: UpdateStudentCourseAssignmentInput!
) {
  updateStudentCourseAssignment(input: $input) {
    id
    student {
      id
      courseAssignments {
        nextToken
      }
      lessonAssignments {
        nextToken
      }
    }
    teacher
    course {
      id
      title
      description
      owner
      lessons {
        nextToken
      }
      teachers
      imageUrl
    }
    createdAt
    updatedAt
    finishedAt
  }
}
`;
export const deleteStudentCourseAssignment = `mutation DeleteStudentCourseAssignment(
  $input: DeleteStudentCourseAssignmentInput!
) {
  deleteStudentCourseAssignment(input: $input) {
    id
    student {
      id
      courseAssignments {
        nextToken
      }
      lessonAssignments {
        nextToken
      }
    }
    teacher
    course {
      id
      title
      description
      owner
      lessons {
        nextToken
      }
      teachers
      imageUrl
    }
    createdAt
    updatedAt
    finishedAt
  }
}
`;
export const createStudentLessonAssignment = `mutation CreateStudentLessonAssignment(
  $input: CreateStudentLessonAssignmentInput!
) {
  createStudentLessonAssignment(input: $input) {
    id
    student {
      id
      courseAssignments {
        nextToken
      }
      lessonAssignments {
        nextToken
      }
    }
    teacher
    lesson {
      id
      courseLessonsId
      title
      description
      content {
        nextToken
      }
      contentOrder
      homework
      draft {
        id
        courseLessonsId
        contentOrder
      }
    }
    createdAt
    updatedAt
    finishedAt
  }
}
`;
export const updateStudentLessonAssignment = `mutation UpdateStudentLessonAssignment(
  $input: UpdateStudentLessonAssignmentInput!
) {
  updateStudentLessonAssignment(input: $input) {
    id
    student {
      id
      courseAssignments {
        nextToken
      }
      lessonAssignments {
        nextToken
      }
    }
    teacher
    lesson {
      id
      courseLessonsId
      title
      description
      content {
        nextToken
      }
      contentOrder
      homework
      draft {
        id
        courseLessonsId
        contentOrder
      }
    }
    createdAt
    updatedAt
    finishedAt
  }
}
`;
export const deleteStudentLessonAssignment = `mutation DeleteStudentLessonAssignment(
  $input: DeleteStudentLessonAssignmentInput!
) {
  deleteStudentLessonAssignment(input: $input) {
    id
    student {
      id
      courseAssignments {
        nextToken
      }
      lessonAssignments {
        nextToken
      }
    }
    teacher
    lesson {
      id
      courseLessonsId
      title
      description
      content {
        nextToken
      }
      contentOrder
      homework
      draft {
        id
        courseLessonsId
        contentOrder
      }
    }
    createdAt
    updatedAt
    finishedAt
  }
}
`;
