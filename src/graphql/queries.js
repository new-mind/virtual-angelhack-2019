// eslint-disable
// this is an auto generated file. This will be overwritten

export const listEmployees = `query ListEmployees($limit: Int, $nextToken: String) {
  listEmployees(limit: $limit, nextToken: $nextToken) {
    items {
      username
      email
      name
      surname
      phone
      age
      status
      enabled
    }
    nextToken
  }
}
`;
export const getEmployee = `query GetEmployee($username: String!) {
  getEmployee(username: $username) {
    username
    email
    name
    surname
    phone
    age
    status
    enabled
  }
}
`;
export const listStudents = `query ListStudents($limit: Int, $nextToken: String) {
  listStudents(limit: $limit, nextToken: $nextToken) {
    items {
      username
      email
      enabled
      name
      surname
      phone
      age
      status
    }
    nextToken
  }
}
`;
export const getStudent = `query GetStudent($username: String!) {
  getStudent(username: $username) {
    username
    email
    enabled
    name
    surname
    phone
    age
    status
  }
}
`;
export const getCourse = `query GetCourse($id: ID!) {
  getCourse(id: $id) {
    id
    title
    description
    owner
    lessons {
      items {
        id
        courseLessonsId
        title
        description
        contentOrder
        homework
      }
      nextToken
    }
    teachers
    imageUrl
  }
}
`;
export const listCourses = `query ListCourses(
  $filter: ModelCourseFilterInput
  $limit: Int
  $nextToken: String
) {
  listCourses(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      title
      description
      owner
      lessons {
        nextToken
      }
      teachers
      imageUrl
    }
    nextToken
  }
}
`;
export const getEmployeeInOrganization = `query GetEmployeeInOrganization($id: ID!) {
  getEmployeeInOrganization(id: $id) {
    id
    admin
    teacher
  }
}
`;
export const listEmployeesInOrganization = `query ListEmployeesInOrganization(
  $filter: ModelEmployeeInOrganizationFilterInput
  $limit: Int
  $nextToken: String
) {
  listEmployeesInOrganization(
    filter: $filter
    limit: $limit
    nextToken: $nextToken
  ) {
    items {
      id
      admin
      teacher
    }
    nextToken
  }
}
`;
export const getTeacherCourseAssignment = `query GetTeacherCourseAssignment($id: ID!) {
  getTeacherCourseAssignment(id: $id) {
    teacher {
      id
      admin
      teacher
    }
    course {
      id
      title
      description
      owner
      lessons {
        nextToken
      }
      teachers
      imageUrl
    }
    createdAt
    updatedAt
    finishedAt
  }
}
`;
export const listTeacherCourseAssignments = `query ListTeacherCourseAssignments(
  $filter: ModelTeacherCourseAssignmentFilterInput
  $limit: Int
  $nextToken: String
) {
  listTeacherCourseAssignments(
    filter: $filter
    limit: $limit
    nextToken: $nextToken
  ) {
    items {
      teacher {
        id
        admin
        teacher
      }
      course {
        id
        title
        description
        owner
        teachers
        imageUrl
      }
      createdAt
      updatedAt
      finishedAt
    }
    nextToken
  }
}
`;
export const getLesson = `query GetLesson($id: ID!) {
  getLesson(id: $id) {
    id
    courseLessonsId
    title
    description
    content {
      items {
        id
        widget
      }
      nextToken
    }
    contentOrder
    homework
  }
}
`;
export const listLessons = `query ListLessons(
  $filter: ModelLessonFilterInput
  $limit: Int
  $nextToken: String
) {
  listLessons(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      courseLessonsId
      title
      description
      content {
        nextToken
      }
      contentOrder
      homework
    }
    nextToken
  }
}
`;
export const getLessonDraft = `query GetLessonDraft($id: ID!) {
  getLessonDraft(id: $id) {
    id
    courseLessonsId
    contentOrder
    content {
      id
      widget
    }
  }
}
`;
export const listLessonDrafts = `query ListLessonDrafts(
  $filter: ModelLessonDraftFilterInput
  $limit: Int
  $nextToken: String
) {
  listLessonDrafts(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      courseLessonsId
      contentOrder
      content {
        id
        widget
      }
    }
    nextToken
  }
}
`;
export const getSection = `query GetSection($id: ID!) {
  getSection(id: $id) {
    id
    widget
  }
}
`;
export const listSections = `query ListSections(
  $filter: ModelSectionFilterInput
  $limit: Int
  $nextToken: String
) {
  listSections(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      widget
    }
    nextToken
  }
}
`;
export const getStudentInOrganization = `query GetStudentInOrganization($id: ID!) {
  getStudentInOrganization(id: $id) {
    id
    courseAssignments {
      items {
        id
        teacher
        createdAt
        updatedAt
        finishedAt
      }
      nextToken
    }
    lessonAssignments {
      items {
        id
        teacher
        createdAt
        updatedAt
        finishedAt
      }
      nextToken
    }
  }
}
`;
export const listStudentsInOrganization = `query ListStudentsInOrganization(
  $filter: ModelStudentInOrganizationFilterInput
  $limit: Int
  $nextToken: String
) {
  listStudentsInOrganization(
    filter: $filter
    limit: $limit
    nextToken: $nextToken
  ) {
    items {
      id
      courseAssignments {
        nextToken
      }
      lessonAssignments {
        nextToken
      }
    }
    nextToken
  }
}
`;
export const getStudentCourseAssignment = `query GetStudentCourseAssignment($id: ID!) {
  getStudentCourseAssignment(id: $id) {
    id
    student {
      id
      courseAssignments {
        nextToken
      }
      lessonAssignments {
        nextToken
      }
    }
    teacher
    course {
      id
      title
      description
      owner
      lessons {
        nextToken
      }
      teachers
      imageUrl
    }
    createdAt
    updatedAt
    finishedAt
  }
}
`;
export const listStudentCourseAssignments = `query ListStudentCourseAssignments(
  $filter: ModelStudentCourseAssignmentFilterInput
  $limit: Int
  $nextToken: String
) {
  listStudentCourseAssignments(
    filter: $filter
    limit: $limit
    nextToken: $nextToken
  ) {
    items {
      id
      student {
        id
      }
      teacher
      course {
        id
        title
        description
        owner
        teachers
        imageUrl
      }
      createdAt
      updatedAt
      finishedAt
    }
    nextToken
  }
}
`;
export const getStudentLessonAssignment = `query GetStudentLessonAssignment($id: ID!) {
  getStudentLessonAssignment(id: $id) {
    id
    student {
      id
      courseAssignments {
        nextToken
      }
      lessonAssignments {
        nextToken
      }
    }
    teacher
    lesson {
      id
      courseLessonsId
      title
      description
      content {
        nextToken
      }
      contentOrder
      homework
    }
    createdAt
    updatedAt
    finishedAt
  }
}
`;
export const listStudentLessonAssignments = `query ListStudentLessonAssignments(
  $filter: ModelStudentLessonAssignmentFilterInput
  $limit: Int
  $nextToken: String
) {
  listStudentLessonAssignments(
    filter: $filter
    limit: $limit
    nextToken: $nextToken
  ) {
    items {
      id
      student {
        id
      }
      teacher
      lesson {
        id
        courseLessonsId
        title
        description
        contentOrder
        homework
      }
      createdAt
      updatedAt
      finishedAt
    }
    nextToken
  }
}
`;
