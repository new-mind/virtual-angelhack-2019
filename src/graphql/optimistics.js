import Lesson from './Lesson/index'
import Course from './Course/index'

export { Lesson, Course }
export const Section = {
  __typename: 'Section',
  id: -1,
  widget: "{}"
}


export const Employee = {
  __typename: 'User',
  username: null,
  email: null,
  enabled: true,
  name: null,
  surname: null,
  phone: null,
  age: null,
  status: 'FORCE_CHANGE_PASSWORD'
}

export const Student = {
  __typename: 'User',
  username: null,
  email: null,
  enabled: true,
  name: null,
  surname: null,
  phone: null,
  age: null,
  status: 'FORCE_CHANGE_PASSWORD'
}

export const EmployeeFrom = (data) => {
  const optimistic = {};
  for (let k in Employee) {
    optimistic[k] = data[k] || optimistic[k];
  }

  return optimistic;
}

export const StudentCourseAssignment = {
  __typename: 'StudentCourseAssignment',
  id: -1,
  student: null,
  teacher: null,
  course: null,
  createdAt: null,
  updatedAt: null,
  finishedAt: null,
}
