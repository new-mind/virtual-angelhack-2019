import React from 'react';
import Amplify, { Storage, I18n } from 'aws-amplify';
import { withAuthenticator } from 'aws-amplify-react';
import {
  ConfirmSignIn, RequireNewPassword, VerifyContact, TOTPSetup, Loading,
} from 'aws-amplify-react';
import { createBrowserHistory } from 'history'
import { Query, ApolloProvider } from 'react-apollo'
import { YMInitializer } from 'react-yandex-metrika';
import GAInitializer from 'react-ga';
import { withCookies } from 'react-cookie';

import { getUserWithRoles } from '@lms/graphql'
import { PORTAL } from './constants';
import client from './graphqlEndpoint'
import awsconfig from './aws-exports';
import AWSS3Provider from './services/AWSS3Provider';
import SignIn from '@lms/components/forms/SignIn';
import SignUp from '@lms/components/forms/SignUp';
import Tawk from '@lms/components/Tawk/Tawk';
import ForgotPassword from '@lms/components/forms/ResetPassword';

const AUTH = {
  ru: {
    title: 'Создайте свою онлайн школу',
    school: 'Введите название школы',
    name: 'Введите Ваше имя',
    email: 'Введите Ваш email',
    password: 'Введите Ваш пароль',
    confirmPassword: 'Подтвердите Ваш пароль',
    loading: 'Загрузка...',
    signIn: 'Вход',
    signUp: 'Регистрация',
    forgotPassword: 'Забыли пароль?',
    terms: 'Услоия использования',
    resetPassword: 'Сброс пароля',
    resendCode: 'Выслать код заново',
    submit: 'Подтвердить',
    sendCode: 'Выслать код',
    code: 'Введите код',
    newPassword: 'Введите новый пароль',
  },
  en: {
    title: 'Create Your Own Online School',
    school: 'Enter Your School Name',
    name: 'Enter Your Name',
    email: 'Enter Your Email',
    password: 'Enter Password',
    confirmPassword: 'Confirm Your Password',
    loading: 'Loading...',
    signIn: 'Sign In',
    signUp: 'Sign Up',
    forgotPassword: 'Forgot Your Password?',
    terms: 'Terms of use',
    resetPassword: 'Reset Your Password',
    resendCode: 'Resend Code',
    submit: 'Submit',
    sendCode: 'Send Code',
    code: 'Enter Code',
    newPassword: 'Enter New Password',
  },
};

Storage.addPluggable(new AWSS3Provider());
Amplify.configure(awsconfig);
I18n.putVocabularies(AUTH);

const portals = {
  [PORTAL.ADMIN]: require('./portals/admin').default,
  [PORTAL.STUDENT]: require('./portals/student').default,
  [PORTAL.TEACHER]: require('./portals/teacher').default
};

class App extends React.Component {
  state = {
    history: createBrowserHistory()
  };

  requestPortal = (portal, history) => {
    const { cookies } = this.props;
    if (portal.role) {
      cookies.set("portal", portal.role, {path: '/'});
    }
    if (portal.organizationId) {
      if (portal.organizationId !== cookies.get("organizationId")) {
        cookies.set("organizationId", portal.organizationId, {path: '/'});
        history.push('/');
      }
    }
  };

  render() {
    const { authData } = this.props;
    const query = getUserWithRoles;

    return (
      <>
        <ApolloProvider client={client}>
          <Query query={query} variables={{id: authData.username}}>
          {({ loading, error, data }) => {
            if (loading) return null;
            if (error) return `${error}`;
            const { getRoles, getUser } = data;
            return this.renderPortal(getUser, getRoles);
          }}
          </Query>
        </ApolloProvider>
        <Tawk/>
        <YMInitializer accounts={[awsconfig.yandex_metric_counter]} options={{webvisor: true, defer: true}} version="2" />
        {GAInitializer.initialize(awsconfig.ga_track_id, { standardImplementation: true })}
      </>
    )
  }

  renderPortal(user, roles) {
    var { cookies } = this.props;

    let portal = cookies.get("portal");
    if (portal) {
      var maps = {
        [PORTAL.ADMIN]: {
          hasPermissions: () => { return roles.admin.length; },
          method: this.renderAdminPortal
        },
        [PORTAL.TEACHER]: {
          hasPermissions: () => {
            return roles.teacher.length || roles.admin.length;
          },
          method: this.renderTeacherPortal,
        },
        [PORTAL.STUDENT]: {
          hasPermissions: () => { return true; },
          method: this.renderStudentPortal
        }
      };

      var map = maps[portal];
      if (map && map.hasPermissions()) {
        return map.method.call(this, roles, user);
      }
    }

    if (roles.admin.length) return this.renderAdminPortal(roles, user);
    if (roles.teacher.length) return this.renderTeacherPortal(roles, user);

    return this.renderStudentPortal(roles, user);
  }

  renderAdminPortal(userRoles, user) {
    const { authData } = this.props;
    const session = authData.signInUserSession;
    const organizationId = this.getOrganization(userRoles.admin);
    if (!organizationId) {
      throw new Error('User is not assigned to any organization');
    }
    const portal = portals[PORTAL.ADMIN];

    return portal(client, this.state.history, {
      ...session.idToken,
      requestPortal: this.requestPortal,
      user,
      organizationId
    });
  }

  renderTeacherPortal(userRoles, user) {
    const { authData } = this.props;
    const session = authData.signInUserSession;
    let organizationId = this.getOrganization(userRoles.teacher);
    if (!organizationId) {
      organizationId = this.getOrganization(userRoles.admin);
    }
    if (!organizationId) {
      throw new Error('User is not assigned to any organization');
    }
    const portal = portals[PORTAL.TEACHER];

    return portal(client, this.state.history, {
      ...session.idToken,
      requestPortal: this.requestPortal,
      user,
      organizationId
    });
  }

  renderStudentPortal(userRoles, user) {
    const { authData } = this.props;
    const session = authData.signInUserSession;
    const organizationId = userRoles.student[0]; //TODO;
    const portal = portals[PORTAL.STUDENT];
    return portal(client, this.state.history, {
      ...session.idToken,
      requestPortal: this.requestPortal,
      userId: authData.username,
      user,
      organizationId
    });
  }

  getOrganization(orgs) {
    const { cookies } = this.props;
    if (!orgs) {
      return null;
    }

    let organizationId = cookies.get('organizationId', { path: '/' });
    if (organizationId && orgs.indexOf(organizationId) !== -1) return organizationId;
    organizationId = orgs[0];
    return organizationId;
  }
}

const authenticatorComponents = [
  <SignIn/>,
  <SignUp/>,
  <ConfirmSignIn/>,
  <RequireNewPassword/>,
  <VerifyContact/>,
  <ForgotPassword/>,
  <TOTPSetup/>,
  <Loading/>
];

export default withCookies(withAuthenticator(App, true, authenticatorComponents));
