import awsconfig from './aws-exports';
import { Auth } from 'aws-amplify';
import { setContext } from 'apollo-link-context';
import AWSAppSyncClient from 'aws-appsync';
import {
  InMemoryCache,
  IntrospectionFragmentMatcher,
  defaultDataIdFromObject
} from 'apollo-cache-inmemory';
import i18n from './i18n'

const fragmentMatcher = new IntrospectionFragmentMatcher({
  introspectionQueryResultData: {
    "__schema": {
      "types": [{
        "kind":"UNION",
        "name":"Section",
        "possibleTypes": [
          {"name":"AttachmentWidget"},
          {"name":"LinkWidget"},
          {"name":"MultiChoiceWidget"},
          {"name":"ImageWidget"},
          {"name":"ResponseFormWidget"},
          {"name":"TextWidget"},
          {"name":"VideoWidget"}
        ]
      }]
    }
  }
});

export const cache = window.cache = new InMemoryCache({
  dataIdFromObject: object => {
    switch (object.__typename) {
      // TODO
      case "LinkItem":
        return `{object.id}-${object.url}-${object.title}`;
      case "AttachementItem":
        return `{object.id}-${object.fileName}-${object.fileKey}-${object.title}`;
      default:
        return defaultDataIdFromObject(object);
    }
  },
  fragmentMatcher
});

const client = new AWSAppSyncClient({
  url: awsconfig.aws_appsync_graphqlEndpoint,
  region: awsconfig.aws_appsync_region,
  auth: {
    type: awsconfig.aws_appsync_authenticationType,
    jwtToken: async () => (await Auth.currentSession()).getIdToken().getJwtToken(),
  },
  disableOffline: true
}, {
  cache
});

const contextLink = setContext(() => ({
  headers: {
    'Accept-Language': i18n.language || "en" //TODO
  }
}));

client.link = contextLink.concat(client.link);

export default client
