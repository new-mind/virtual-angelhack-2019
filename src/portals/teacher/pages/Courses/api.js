import { graphql } from 'react-apollo';

import * as gql from '@lms/graphql';

const LIMIT = null;
export const listCourses = graphql(gql.listCourses, {
  options: props => ({
    variables: {
      limit: LIMIT,
      filter: {
        organizationId: props.organizationId
      }
    }
  })
});
