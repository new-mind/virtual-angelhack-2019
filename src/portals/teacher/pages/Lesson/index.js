import React from 'react';
import cn from 'classnames';
import { compose } from 'react-apollo';
import { withTranslation } from 'react-i18next';

import Title from '@lms/components/blocks/PageTitle';
import Error from '@lms/components/Error';
import Loader from '@lms/components/loader/Loader';
import Lesson from '@lms/components/Lesson';
import AssignHomeworkForm from '../Course/forms/AssignHomework';

import * as API from './api';
import './i18n';

import styles from './styles.module.scss';
import PageContent from '@lms/components/layouts/PageContent';
import * as _ from 'lodash';
import LessonNotFound from '@lms/components/LessonNotFound';

class LessonPage extends React.Component {
    state = {
      showAssignForm: false
    };

  render() {
    const { data, organizationId } = this.props;

    if (data.loading) return <Loader />;
    if (data.error) return <Error {...data.error} />;

    const { getLesson } = data;

    if (_.isNull(getLesson)) return (
      <PageContent>
        <LessonNotFound />
      </PageContent>
    );

    return (
      <PageContent>
        <div className={cn(styles.header, 'mb-m2')}>
          <h2><Title>{getLesson.title}</Title></h2>
        </div>
        {this.state.showAssignForm && (
          <AssignHomeworkForm
            organizationId={organizationId}
            students={getLesson.course.students}
            show={this.state.showAssignForm}
            lessonId={getLesson.id}
            onClose={() => this.setState({ showAssignForm: false }) }
          />
        )}
        <p className="mb-xl">{getLesson.description}</p>
        <Lesson sections={getLesson.content} mode="PREVIEW_WITHOUT_DATA"/>
      </PageContent>
    )
  }
}

export default compose(
  API.getLesson,
  withTranslation('teacher/pages/Lesson')
)(LessonPage);
