import { graphql } from 'react-apollo';
import { fragments } from '@lms/graphql';
import gql from 'graphql-tag';

const GET_LESSON = gql`
  query GetLesson($id: ID!) {
    getLesson(id: $id) {
      id
      courseId
      title
      description
      homework
      dirty
      rev
      course {
        id
        title
        students {
          id
          name
          surname
          email
        }
      }

      content {
        ...TextWidget
        ...VideoWidget
        ...LinkWidget
        ...MultiChoiceWidget
        ...AttachmentWidget
        ...ImageWidget
        ...ResponseFormWidget
      }
    }
  }
  ${fragments.TextWidget}
  ${fragments.VideoWidget}
  ${fragments.LinkWidget}
  ${fragments.MultiChoiceWidget}
  ${fragments.AttachmentWidget}
  ${fragments.ImageWidget}
  ${fragments.ResponseFormWidget}
`;

export const getLesson = graphql(GET_LESSON, {
  options: props => {
    return {
      variables: {
        id: props.match.params.id
      }
    };
  }
});
