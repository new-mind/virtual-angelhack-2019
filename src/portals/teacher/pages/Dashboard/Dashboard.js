import React from 'react';
import { Redirect, Switch } from 'react-router-dom';
import { withTranslation } from 'react-i18next';

import Dashboard from '@lms/components/Dashboard';
import Profile from '@lms/components/Profile';
import Route from '@lms/components/Route';

import Homeworks from '@lms/portals/teacher/subpages/Homeworks/Homeworks';
import Homework from '@lms/portals/teacher/subpages/Homework';
import './i18n';

class TeacherDashboard extends React.PureComponent {
  renderContent() {
    return (
      <Switch>
        <Route exact path="/profile" component={Profile} {...this.props} />
        <Route exact path="/homework/:id" component={Homework} {...this.props} />
        <Route exact path="/homework" component={Homeworks} {...this.props} />
        <Redirect to="/profile" />
      </Switch>
    );
  }

  render() {
    const { t } = this.props;
    const menu = [
      { url: "/profile", title: t("Profile") },
      { url: "/homework", title: t("Homework") }
    ];

    return (
      <Dashboard
        user={this.props.user}
        content={this.renderContent()}
        menu={menu}/>
    );
  }
}

export default withTranslation('teacher/pages/Dashboard')(TeacherDashboard);
