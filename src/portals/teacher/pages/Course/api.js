import * as _ from 'lodash';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';

const ASSIGN_STUDENTS = gql`
  mutation AddStudents($students: [ID!]!, $lessonId: ID!) {
    Lesson(id: $lessonId) {
      addStudents(students: $students)
    }
  }
`;

const UNASSIGN_STUDENTS = gql`
  mutation DeleteStudents($students: [ID!]!, $lessonId: ID!) {
    Lesson(id: $lessonId) {
      deleteStudents(students: $students)
    }
  }
`;

export const getCourse = graphql(gql`
  query GetCourse($id: ID!) {
    getCourse(id: $id) {
      id
      title
      description
      imageUrl
      lessons {
        items {
          id
          title
          description
          homework
        }
        nextToken
      }
      students {
        id
        email
        name
        surname
      }
    }
  }
`, {
  options: props => ({
    variables: {
      id: props.match.params.id,
    },
  }),
  props: props => {
    return {
      data: props.data
    }
  }
});

const getLessonGQL = gql`
  query GetLesson($id: ID!) {
    getLesson(id: $id) {
      id
      students {
        id
      }
    }
  }
`;
export const getLesson = graphql(getLessonGQL, {
  options: props => ({
    variables: {
      id: props.lessonId
    }
  })
});

export const assignStudents = graphql(ASSIGN_STUDENTS, {
  options: props => ({
    update: (store, {data: {Lesson}}) => {
      const query = getLessonGQL;
      const variables = { id: props.lessonId };
      const data = store.readQuery({ query, variables });

      data.getLesson.students = [
        ..._.map(Lesson.addStudents, (id) => ({
          __typename: "Student",
          id,
        })),
        ...data.getLesson.students,
      ];

      store.writeQuery({ query, variables, data });
    },
  }),
  props: ({mutate, ownProps}) => ({
    assignStudents: (students, lesson) => {
      return mutate({
        variables: {
          students,
          lessonId: ownProps.lessonId,
        },
        optimisticResponse: {
          __typename: 'Mutation',
          Lesson: {
            ...lesson,
            addStudents: students,
          },
        },
      });
    },
  }),
});

export const unassignStudents = graphql(UNASSIGN_STUDENTS, {
  options: props => ({
    update: (store, { data: { Lesson } }) => {
      const query = getLessonGQL;
      const variables = { id: props.lessonId };
      const data = store.readQuery({ query, variables });

      _.forEach(
        Lesson.deleteStudents,
        studentId => _.remove(data.getLesson.students, student => studentId === student.id),
      );

      store.writeQuery({ query, variables, data });
    },
  }),
  props: ({mutate, ownProps}) => ({
    unassignStudents: (students, lesson) => {
      return mutate({
        variables: {
          students,
          lessonId: ownProps.lessonId,
        },
        optimisticResponse: {
          __typename: 'Mutation',
          Lesson: {
            ...lesson,
            deleteStudents: students
          },
        },
      });
    },
  }),
});
