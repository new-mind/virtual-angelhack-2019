import React, { Component } from 'react';
import cn from 'classnames';
import { Alert, Col, Row } from 'react-bootstrap';
import { compose } from 'react-apollo';
import { withTranslation } from 'react-i18next';
import * as _ from 'lodash';
import { NavLink } from 'react-router-dom';

import Loader from '@lms/components/loader/Loader';
import Error from '@lms/components/Error';
import AssignHomeworkForm from './forms/AssignHomework';
import Title from '@lms/components/blocks/PageTitle';
import PageContent from '@lms/components/layouts/PageContent';
import Button from '@lms/components/blocks/Button';
import LessonCard from '@lms/components/LessonCard';

import './i18n';
import * as API from './api';

import styles from '@lms/portals/teacher/components/Lessons/styles.module.scss';

class Course extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showAssignForm: false,
      lessonId: null,
    };
  }

  handleAssign = (lessonId) => {
    this.setState({ showAssignForm: true, lessonId });
  };

  get lessons() {
    return _.get(this.props.data, 'getCourse.lessons.items', []);
  }

  render() {
    const { data, organizationId, t } = this.props;

    if (data.loading) return <Loader />;
    if (data.error) return <Error {...data.error} />;

    const { title, description, students } = data.getCourse;

    return (
      <PageContent>
        <Row className="mb-m2">
          <Col>
            <Title level={2}>{title}</Title>
          </Col>
        </Row>
        <Row className="mb-xl">
          <Col>{description}</Col>
        </Row>
        <Row>
          <Col>
            {this.lessons.length ? (
              <Row className="mx-ns2">
                {_.map(this.lessons, (lesson) => (
                  <Col
                    className="p-s2"
                    key={lesson.id}
                    xs={12} sm={12} md={6} lg={3} xl={3}
                  >
                    <LessonCard
                      title={lesson.title}
                      isHomework={lesson.homework}
                      actions={(
                        <>
                          <Button className={cn(styles.button, 'mr-s2')}>
                            <NavLink className="text-light" to={`/lessons/${lesson.id}`}>
                              {t('Open')}
                            </NavLink>
                          </Button>
                          <Button className={styles.button} onClick={() => this.handleAssign(lesson.id)}>
                            { t("Assign") }
                          </Button>
                        </>
                      )}
                    />
                  </Col>
                ))}
              </Row>
            ) : (
              <Alert variant="warning">
                <Alert.Heading>{t("Empty Course")}</Alert.Heading>
                <hr/>
                <p>{t("Ask your admin for creating content")}</p>
              </Alert>
            )}
          </Col>
        </Row>
        { this.state.showAssignForm &&
          <AssignHomeworkForm
              organizationId={organizationId}
              students={students}
              show={this.state.showAssignForm}
              lessonId={this.state.lessonId}
              onClose={() => this.setState({ showAssignForm: false }) }
            />
        }
      </PageContent>
    );
  }
}

export default compose(
  API.getCourse,
  withTranslation('teacher/pages/Course')
)(Course);
