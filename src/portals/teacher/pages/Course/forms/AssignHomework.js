import React, { Component } from 'react';
import { Form, Modal } from 'react-bootstrap';
import { Formik } from 'formik';
import * as yup from 'yup';
import { compose } from 'react-apollo';
import * as _ from 'lodash';
import { withTranslation } from 'react-i18next';

import Button from '@lms/components/blocks/Button';
import Lesson from '@lms/components/Lesson';
import Loader from '@lms/components/loader/Loader';
import Error from '@lms/components/Error';
import * as API from '../api';
import styles from '../styles.module.scss';

const schema = yup.object({});

class AssignHomework extends Component {
  constructor(props) {
    super(props);

    this._ref = React.createRef();
  }

  assign = () => {
    const assignedStudents = this._ref.current.getAssignedStudents();
    const unassignedStudents = this._ref.current.getUnassignedStudents();
    const { data } = this.props;

    _.size(assignedStudents) && this.props.assignStudents(assignedStudents, data.getLesson);
    _.size(unassignedStudents) && this.props.unassignStudents(unassignedStudents, data.getLesson);
    this.props.onClose();
  };

  handleClose = () => {
    this.props.onClose();
  };

  renderStudents() {
    const { data, students } = this.props;
    if (!data) return null;

    if (data.loading) return <Loader/>;
    if (data.error) return <Error {...data.error} />;

    const { getLesson } = data;

    return (
      <Lesson.AssignmentForm
        ref={this._ref}
        assignedStudents={_.keyBy(getLesson.students, (v) => v.id)}
        students={students}
        noHeader={true}
        noFooter={true}
      />
    );
  }

  render() {
    const { t } = this.props;
    return (
      <Modal show={this.props.show} onHide={this.handleClose}>
        <Formik
            validationSchema={schema}
            onSubmit={this.assign}>
          {({ handleSubmit }) => (
            <Form noValidate onSubmit={handleSubmit}>
              <Modal.Header closeButton className="border-0">
                <Modal.Title className={styles.modalTitle}>{t("assignmentForm.title")}</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                {this.renderStudents()}
              </Modal.Body>
              <Modal.Footer className="border-0">
                <Button className="my-2 w-100" type="submit">{t("Assign")}</Button>
              </Modal.Footer>
            </Form>
          )}
        </Formik>
      </Modal>
    );
  }
}

export default compose(
  API.getLesson,
  API.assignStudents,
  API.unassignStudents,
  withTranslation('teacher/pages/Course'),
)(AssignHomework);
