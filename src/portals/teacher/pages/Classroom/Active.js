import React from 'react';
import { Container, Image } from 'react-bootstrap';
import { compose } from 'react-apollo';

import Title from '@lms/components/blocks/PageTitle';
import Error from '@lms/components/Error';
import Loader from '@lms/components/loader/Loader';
import img2 from './assets/camera2.jpg';
import * as API from '../Lesson/api';
import Lesson from '@lms/components/Lesson';

import styles from './styles.module.scss';

class Classroom extends React.Component {
  render() {
    const { data } = this.props;

    if (data.loading) return <Loader />;
    if (data.error) return <Error {...data.error} />;

    const { getLesson } = data;

    return (
      <Container fluid>
        <div className={styles.classroom}>
          <div>
            <div className={styles.webcam}>
              <Image src={img2}/>
            </div>
          </div>
          <div className="my-5">
            <Container>
              <h3><Title>{getLesson.title}</Title></h3>
            </Container>
            <div className="my-5">
              <Lesson sections={getLesson.content}/>
            </div>
          </div>
        </div>
      </Container>
    );
  }
}

export default compose(
  API.getLesson
)(Classroom);
