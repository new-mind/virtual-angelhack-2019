import React from 'react';
import { Container, Image } from 'react-bootstrap';
import cn from 'classnames';

import Title from '@lms/components/blocks/PageTitle';
import Button from '@lms/components/blocks/Button';

import lesson from './assets/lesson.jpg';
import styles from './styles.module.scss';

import img2 from './assets/camera2.jpg';
import uploadImg from './assets/attachments/upload.jpg';
import aaImg from './assets/attachments/aa.jpg';
import a1Img from './assets/attachments/a1.jpg';
import a2Img from './assets/attachments/a2.jpg';
import a3Img from './assets/attachments/a3.jpg';
import a4Img from './assets/attachments/a4.jpg';
import a5Img from './assets/attachments/a5.jpg';

const [ LESSON, ATTACHMENTS, ATTACHMENT ] = ['LESSON', 'ATTACHMENTS', 'ATTACHMENT'];

class Classroom extends React.Component {
  state = {
    page: LESSON
  }

  renderLesson() {
    return (
      <Image src={lesson}/>
    );
  }

  showAttachment = () => {
    this.setState({page: ATTACHMENT})
  };

  renderAttachments() {
    return (
      <div className={styles.attachments}>
        <div>
          <Image src={uploadImg}/>
        </div>
        <div className={styles.list}>
          <Image src={a1Img} onClick={this.showAttachment}/>
          <Image src={a2Img} onClick={this.showAttachment}/>
          <Image src={a3Img} onClick={this.showAttachment}/>
          <Image src={a4Img} onClick={this.showAttachment}/>
          <Image src={a5Img} onClick={this.showAttachment}/>
          <Image src={a4Img} onClick={this.showAttachment}/>
          <Image src={a3Img} onClick={this.showAttachment}/>
          <Image src={a5Img} onClick={this.showAttachment}/>
          <Image src={a1Img} onClick={this.showAttachment}/>
          <Image src={a2Img} onClick={this.showAttachment}/>
        </div>
      </div>
    );
  }

  renderAttachment() {
    return (
      <div className="my-5">
        <Image src={aaImg}/>
      </div>
    )
  }

  renderContent() {
    switch (this.state.page) { 
      case ATTACHMENTS:
        return this.renderAttachments();
      case ATTACHMENT:
        return this.renderAttachment();
      case LESSON:
      default:
        return this.renderLesson();
    }
  }

  render() {
    return (
      <Container fluid className="my-5">
        <div className={styles.classroom}>
          <div>
            <div className={styles.webcam}>
              <Image src={img2}/>
            </div>
          </div>
          <div className="my-5">
            <Container className={styles.header}>
              <h3><Title>Perfect Subjunctive</Title></h3>
              <div className={styles.buttons}>
                <Button variant="light"
                  className={cn({transparent: this.state.page === LESSON})}
                  onClick={() => this.setState({page: LESSON})}>LESSON</Button>
                <Button variant="light"
                  className={cn({
                    transparent: this.state.page === ATTACHMENTS ||
                                 this.state.page === ATTACHMENT
                  })}
                  onClick={() => this.setState({page: ATTACHMENTS})}>ATTACHMENTS</Button>
              </div>
            </Container>
            <div className="my-5 mx-5">
              { this.renderContent() }
            </div>
          </div>
        </div>
      </Container>
    );
  }
}

export default Classroom;
