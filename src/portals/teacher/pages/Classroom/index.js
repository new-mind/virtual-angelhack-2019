import React from 'react';
import { Row, Col, Container, Image } from 'react-bootstrap';

import List from '@lms/components/blocks/List';
import Link from '@lms/components/blocks/Link';
import Title from '@lms/components/blocks/PageTitle';

import img1 from './assets/camera1.jpg';

class Classroom extends React.Component {
  render() {
    return (
      <Container fluid className="my-5">
        <Row>
          <Col md={6} style={{minWidth: '620px'}}>
            <Image src={img1}/>
          </Col>
          <Col md={6}>
            <h2><Title>Lesson list</Title></h2>
            <div className="my-5">
              <List>
                <List.Item>
                  <Link to="/classroom/fake/1">
                    LESSON TITLE
                  </Link>
                </List.Item>
                <List.Item>
                  <Link to="/classroom/fake/2">
                    LESSON TITLE
                  </Link>
                </List.Item>
                <List.Item>
                  <Link to="/classroom/fake/3">
                    LESSON TITLE
                  </Link>
                </List.Item>
                <List.Item>
                  <Link to="/classroom/fake/4">
                    LESSON TITLE
                  </Link>
                </List.Item>
                <List.Item>
                  <Link to="/classroom/fake/5">
                    LESSON TITLE
                  </Link>
                </List.Item>
              </List>
            </div>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default Classroom;
