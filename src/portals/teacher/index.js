import React from 'react';
import { ApolloProvider } from 'react-apollo';
import { Redirect, Router, Switch } from 'react-router-dom';
import { Container } from 'react-bootstrap';

import Route from '@lms/components/Route';
import Header from './components/Header';
import Courses from './pages/Courses/Courses';
import PageFooter from '@lms/components/layouts/PageFooter';
import Course from './pages/Course/Course';
import Dashboard from './pages/Dashboard/Dashboard';
import Lesson from './pages/Lesson';
import Classroom from './pages/Classroom';
import ActiveClassroom from './pages/Classroom/Active';
import FakeClassroom from './pages/Classroom/FakeActive';

export default function (client, history, props) {
  return (
    <ApolloProvider client={client}>
      <Container as="main" role="main" fluid="true" className="p-0 flex-column d-flex">
        <Router history={history}>
          <Header {...props} history={history}/>
          <Container className="flex-grow-1 flex-shrink-1 p-0" style={{ flex: '1 1 100%' }} fluid>
            <Switch>
              <Route exact path="/courses" component={Courses} {...props} />} />
              <Route exact path="/courses/:id" component={Course} {...props} />} />
              <Route exact path="/lessons/:id" component={Lesson} {...props} />
              <Route exect path="/classroom/fake/:id" component={FakeClassroom} {...props} />
              <Route exect path="/classroom/:id" component={ActiveClassroom} {...props} />
              <Route exect path="/classroom" component={Classroom} {...props} />

              <Route exect path="/profile" component={Dashboard} {...props} />
              <Route exect path="/students" component={Dashboard} {...props} />
              <Route exect path="/homework/:id" component={Dashboard} {...props} />
              <Route exect path="/homework" component={Dashboard} {...props} />
              <Redirect to="/courses" />
            </Switch>
          </Container>
          <PageFooter />
        </Router>
      </Container>
    </ApolloProvider>
  );
}
