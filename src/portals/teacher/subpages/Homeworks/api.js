import { graphql } from 'react-apollo';
import gql from 'graphql-tag';

const LIST_STUDENTS = gql`
  query ListStudents($filter: StudentFilterInput, $limit: Int, $nextToken: String, $organizationId: ID!) {
    listStudents(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        email
        name
        surname

        homeworksInOrg(organizationId: $organizationId) {
          id
          lessonId
          status
          title
        }
      }
      nextToken
    }
  }
`;
export const listStudents = graphql(LIST_STUDENTS, {
  options: props => {
    return {
      variables: {
        organizationId: props.organizationId,
        filter: {
          organizationId: props.organizationId,
        },
        limit: null
      }
    }
  }
});
