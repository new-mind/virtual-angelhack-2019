import React from 'react';
import cn from 'classnames';
import _ from 'lodash';

import { Dropdown, ListGroup } from 'react-bootstrap';
import Toggle from '@lms/components/blocks/Toggle/Toggle';
import { Link } from 'react-router-dom';

import styles from './styles.module.scss';

class StudentItem extends React.Component {
  state = {
    isOpenCurrent: false,
    isOpenCompleted: false,
  };

  splitHomeworks(homeworks) {
    const active = [];
    const completed = [];
    _.forEach(homeworks, homework => {
      if (homework.status === "COMPLETED") {
        completed.push(homework);
      } else {
        active.push(homework);
      }
    });

    return [active, completed];
  }
  render() {
    const { student, index: i, className: cell, isBlurred} = this.props;
    const [active, completed] = this.splitHomeworks(student.homeworksInOrg);

    return (
      <tr className={cn(isBlurred && styles.blurry)}>
        <td className={cn(cell)}>
          {i + 1}) {student.name || student.email}
        </td>
        <td className={cn(cell)}>
          { this.renderActiveList(active) }
        </td>
        <td className={cn(cell)}>
          { this.renderCompletedList(completed) }
        </td>
      </tr>
    );
  }

  renderActiveList(homeworks)  {
    if (!_.size(homeworks)) {
      return null;
    }

    if (_.size(homeworks) === 1) {
      const homework = homeworks[0];
      return (
        <p className={cn({
          [styles.submitted]: homework.status !== "OPEN" && homework.status !== "REOPEN"
        }, styles.item)}>
          { this.renderHomeworkLink(homework) }
        </p>
      )
    }

    const isSubmitted = !!_.find(homeworks, (h) => h.status === "SUBMITTED");
    return (
      <Dropdown onToggle={() => {
        this.props.onFocus(
          !this.state.isOpenCurrent || this.state.isOpenCompleted ?
            [this.props.index, true] :
            [this.props.index, false]);
        this.setState({ isOpenCurrent: !this.state.isOpenCurrent });
      }}>
        <Dropdown.Toggle
            bsPrefix={cn(styles.toggle, { submitted: isSubmitted })} //TODO: use props
            as={Toggle}
            isOpen={this.state.isOpenCurrent}>
          {homeworks[0].title}
        </Dropdown.Toggle>
        <Dropdown.Menu className="py-1">
          <ListGroup variant="flush" className="px-3">
            {_.map(homeworks, (homework, i) => (
              <ListGroup.Item
                  key={i}
                  className={cn({
                    [styles.submitted]: homework.status !== "OPEN" && homework.status !== "REOPEN"
                  }, styles.item, 'py-2 px-0 pr-2')}>
                { this.renderHomeworkLink(homework) }
              </ListGroup.Item>
            ))}
          </ListGroup>
        </Dropdown.Menu>
      </Dropdown>
    );
  }

  renderCompletedList(homeworks) {
    if (!_.size(homeworks)) return null;

    if (_.size(homeworks) === 1) {
      return this.renderHomeworkLink(homeworks[0]);
    }

    return (
      <Dropdown onToggle={() => {
        this.props.onFocus(
          !this.state.isOpenCompleted|| this.state.isOpenCurrent ?
            [this.props.index, true] :
            [this.props.index, false]);
        this.setState({ isOpenCompleted: !this.state.isOpenCompleted });
      }}>
        <Dropdown.Toggle bsPrefix={styles.toggle} as={Toggle} isOpen={this.state.isOpenCompleted}>{homeworks[0].title}</Dropdown.Toggle>
        <Dropdown.Menu className="py-1">
          <ListGroup variant="flush" className="px-3">
            {_.map(homeworks, (homework, i) => (
              <ListGroup.Item key={i} className={cn('py-2 px-0 pr-2 d-flex flex-row align-items-center')}>
                { this.renderHomeworkLink(homework) }
              </ListGroup.Item>
            ))}
          </ListGroup>
        </Dropdown.Menu>
      </Dropdown>
    );
  }

  renderHomeworkLink(homework) {
    return (
      <Link to={`/homework/${homework.id}`} className={styles.link}>
        {homework.overdue && <i className={cn(styles.overdue, 'd-flex flex-row align-items-center')} />}
        <span className="pr-2">{homework.title}</span>
      </Link>
    );
  }
}

export default StudentItem;
