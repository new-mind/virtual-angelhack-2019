import React from 'react';
import { Col, Row, Table } from 'react-bootstrap';
import _ from 'lodash';
import { compose } from 'react-apollo';
import { withTranslation } from 'react-i18next';

import Title from '@lms/components/blocks/PageTitle';
import Legend from '@lms/components/Legend/Legend';
import StudentItem from './StudentItem/StudentItem';

import * as API from './api';
import styles from './styles.module.scss';
import submittedIcon from './images/legend-submitted.svg';
import overdueIcon from './images/legend-overdue.svg';
import Error from '@lms/components/Error';
import Loader from '@lms/components/loader/Loader';
import './i18n';

class Homeworks extends React.Component {
  state = {
    focusedStudentItem: {},
  };

  render() {
    const { data, t } = this.props;
    const LEGEND = [{
      icon: submittedIcon,
      label: t('Submitted'),
    }, {
      icon: overdueIcon,
      label: t('Overdue'),
    }];

    if (data.loading) return <Loader />;
    if (data.error) return <Error {...data.error} />;

    const { listStudents } = data;

    return (
      <>
        <Row className="d-flex align-items-center">
          <Col>
            <Title>{t("Homework")}</Title>
          </Col>
        </Row>
        <Row>
          <Col>
            <Legend items={LEGEND} />
          </Col>
        </Row>
        <Row>
          <Col className="my-5">
            <Table borderless className={styles.students}>
              <thead>
                <tr className={styles.header}>
                  <th className={styles.cell} />
                  <th className={styles.cell}>
                    {t("Current homework")}
                  </th>
                  <th className={styles.cell}>
                    {t("Completed homework")}
                  </th>
                </tr>
              </thead>
              <tbody>
              {listStudents.items.map((student, i) => (
                <StudentItem
                  className={styles.cell}
                  key={i}
                  index={i}
                  student={student}
                  isBlurred={_.includes(this.state.focusedStudentItem, true) && !this.state.focusedStudentItem[i]}
                  onFocus={([index, status]) => {
                    this.setState({ focusedStudentItem: { ...this.state.focusedStudentItem, [index]: status } });
                  }}
                />
              ))}
              </tbody>
            </Table>
          </Col>
        </Row>
      </>
    );
  }
}

export default compose(
  API.listStudents,
  withTranslation('teacher/subpages/Homeworks')
)(Homeworks);
