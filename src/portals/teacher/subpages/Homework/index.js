import React from 'react';
import { Container, Col, Row  } from 'react-bootstrap';
import { compose } from 'react-apollo';
import cn from 'classnames';
import * as _ from 'lodash';
import { withTranslation } from 'react-i18next';
import { withCookies } from 'react-cookie';
import Separator from '@lms/components/blocks/Separator';

import * as API from './api';
import Loader from '@lms/components/loader/Loader';
import Error from '@lms/components/Error';
import Title from '@lms/components/blocks/PageTitle';
import Lesson from '@lms/components/Lesson';
import Button from '@lms/components/blocks/Button';
import Feedback from '@lms/components/Feedback';

import styles from './styles.module.scss';
import './i18n';

class Homework extends React.Component {
  constructor(props) {
    super(props);
    this._ref = React.createRef();
  }

  acceptLesson = () => {
    const { getStudentLesson } = this.props.data;
    this.props.acceptLesson(getStudentLesson.id, getStudentLesson);
  }

  reopenLesson = () => {
    const { getStudentLesson } = this.props.data;
    this.props.reopenLesson(getStudentLesson.id, getStudentLesson);
  }

  addComment = (comment) => {
    this.props.addComment(comment);
  }

  collapse = () => {
    const { cookies, data } = this.props;
    const { getStudentLesson } = data;
    const key = `collapsed-${getStudentLesson.id}`;
    const collapsed = cookies.get(key) === "true";
    cookies.set(key, !collapsed);
  }

  get comments() {
    const { getStudentLesson } = this.props.data;
    return _.chain(getStudentLesson.comments.items)
      .map(comment => {
        return {
          from: comment.userId === getStudentLesson.student.id ? 'STUDENT': 'TEACHER',
          createdAt: comment.createdAt,
          content: comment.content
        }
      })
      .value();
  }

  render() {
    const { data, t, cookies } = this.props;

    if (data.loading) return <Loader />;
    if (data.error) return <Error {...data.error} />;

    const { getStudentLesson } = data;
    const { student } = getStudentLesson;

    const key = `collapsed-${getStudentLesson.id}`;
    const collapsed = cookies.get(key) === "true";

    return (
      <Container className="my-5">
        <div className={styles.header}>
          <h2><Title>{getStudentLesson.title}</Title></h2>
          <div className={cn(styles.studentName, "d-flex")}>{this.getStudentName(student)}</div>
        </div>
        <p>{getStudentLesson.description}</p>
        { collapsed ? null :
          <Lesson sections={getStudentLesson.content}/>
        }
        <Separator onClick={this.collapse} collapsed={collapsed}/>

        <Feedback comments={this.comments}/>
        <Row>
          <Col xs={12} sm={12} md={6} lg={6} xl={6}>
            { getStudentLesson.status !== "COMPLETED" &&
              <Feedback.addForm onSubmit={this.addComment}/>
            }
          </Col>
          <Col xs={12} sm={12} md={6} lg={6} xl={6} className="d-flex flex-column justify-content-end text-right">
            <div className={styles.label}>{t("Student name")}</div>
            <div className={styles.studentName}>{this.getStudentName(student)}</div>
            { this.renderButtons() }
          </Col>
        </Row>
      </Container>
    );
  }

  renderButtons() {
    const { t } = this.props;
    const { getStudentLesson } = this.props.data;
    if (getStudentLesson.status === "SUBMITTED") {
      return (
        <div className="d-flex flex-row">
          <Button
              onClick={this.reopenLesson}
              className={cn("w-50 ml-auto mr-5")}>
            {t("Reopen")}
          </Button>
          <Button
              onClick={this.acceptLesson}
              variant="success"
              className={cn(styles.completed, "w-50 ml-auto")}>
            {t("Accept")}
          </Button>
        </div>
      );
    }

    if (getStudentLesson.status === "COMPLETED") {
      return (
        <div className="d-flex flex-row">
          <Button variant="success" className={cn(styles.completed, styles.button, "w-50 ml-auto")} disabled>{t("Completed")}</Button>;
        </div>
      )
    }

    return (
      <div className="d-flex flex-row">
        <Button variant="success" className={cn(styles.completed, styles.button, "w-50 ml-auto")} disabled>{t("Accept")}</Button>;
      </div>
    )
  }

  getStudentName(student) {
    if (!student) return null;

    if (student.name || student.surname) {
      return `${student.name} ${student.surname}`;
    }

    return student.email;
  }
}

export default compose(
  API.getLesson,
  API.acceptLesson,
  API.reopenLesson,
  API.addComment,
  withTranslation('teacher/subpages/Homework'),
  withCookies
)(Homework);
