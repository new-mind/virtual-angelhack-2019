import { graphql } from 'react-apollo';
import gql from 'graphql-tag';

import { fragments } from '@lms/graphql/Lesson';
import { optimistic } from '@lms/graphql';

const GET_LESSON = gql`
  query GetStudentLesson($id: ID!, $organizationId: ID!) {
    getStudentLesson(id: $id, organizationId: $organizationId) {
      id
      courseId
      title
      description
      homework
      status
      lessonId
      course {
        id
        title
      }
      comments {
        items {
          content
          createdAt
          userId
        }
      }

      content {
        ...TextWidget
        ...VideoWidget
        ...LinkWidget
        ...AttachmentWidget
        ...ImageWidget
        ...ResponseFormWidgetWithAnswers
      }

      student {
        id
        name
        surname
        email
      }
    }
  }
  ${fragments.TextWidget}
  ${fragments.VideoWidget}
  ${fragments.LinkWidget}
  ${fragments.AttachmentWidget}
  ${fragments.ImageWidget}
  ${fragments.ResponseFormWidgetWithAnswers}
`;

const ACCEPT_LESSON = gql`
  mutation AcceptStudentLesson($id: ID!, $organizationId: ID!) {
    acceptStudentLesson(id: $id, organizationId: $organizationId) {
      id
      lessonId
      status
    }
  }
`

const REOPEN_LESSON = gql`
  mutation ReopenStudentLesson($id: ID!, $organizationId: ID!) {
    reopenStudentLesson(id: $id, organizationId: $organizationId) {
      id
      lessonId
      status
    }
  }
`
const ADD_LESSON_COMMENT = gql`
  mutation AddLessonCommment($input: CreateStudentLessonCommentInput!) {
    addLessonComment(input: $input) {
      id
      content
      createdAt
      userId
    }
  }
`;

export const getLesson = graphql(GET_LESSON, {
  options: props => {
    return {
      variables: {
        organizationId: props.organizationId,
        id: props.match.params.id
      }
    }
  }
});

export const acceptLesson = graphql(ACCEPT_LESSON, {
  options: props => ({
    update: (store, { data: {acceptStudentLesson} }) => {
      const query = GET_LESSON;
      const variables = {
        organizationId: props.organizationId,
        id: props.match.params.id
      };
      const data = store.readQuery({ query, variables });
      data.getStudentLesson.status = 'COMPLETED';
      store.writeQuery({ query, variables, data });
    }
  }),
  props: ({mutate, ownProps}) => ({
    acceptLesson: (id, lesson) => {
      return mutate({
        variables: {
          id,
          organizationId: ownProps.organizationId
        },
        optimisticResponse: {
          __typename: "Mutation",
          acceptStudentLesson: lesson
        }
      });
    },
  })
});

export const reopenLesson = graphql(REOPEN_LESSON, {
  options: props => ({
    update: (store, { data: {acceptStudentLesson} }) => {
      const query = GET_LESSON;
      const variables = {
        organizationId: props.organizationId,
        id: props.match.params.id
      };
      const data = store.readQuery({ query, variables });
      data.getStudentLesson.status = 'OPEN';
      data.getStudentLesson.rev += 1;
      store.writeQuery({ query, variables, data });
    }
  }),
  props: ({mutate, ownProps}) => ({
    reopenLesson: (id, lesson) => {
      return mutate({
        variables: {
          id,
          organizationId: ownProps.organizationId
        },
        optimisticResponse: {
          __typename: "Mutation",
          reopenStudentLesson: lesson
        }
      });
    },
  })
});

export const addComment = graphql(ADD_LESSON_COMMENT, {
  options: props => ({
    update: (store, { data: {addLessonComment} }) => {
      const query = GET_LESSON;
      const variables = {
        organizationId: props.organizationId,
        id: props.match.params.id
      };
      const data = store.readQuery({ query, variables });
      data.getStudentLesson.comments.items = [
        ...data.getStudentLesson.comments.items,
        addLessonComment
      ]

      store.writeQuery({ query, variables, data });
    }
  }),
  props: ({mutate, ownProps}) => ({
    addComment: (content) => {
      return mutate({
        variables: {
          input: {
            organizationId: ownProps.organizationId,
            userId: ownProps.user.id,
            lessonId: ownProps.match.params.id,
            content
          }
        },
        optimisticResponse: {
          __typename: "Mutation",
          addLessonComment: {
            ...optimistic.StudentLessonComment,
            content,
            userId: ownProps.user.id
          }
        }
      });
    }
  })
})
