import React, { Component } from 'react';
import cn from 'classnames';
import { graphql, compose } from 'react-apollo';
import * as gql from '@lms/graphql';
import { withTranslation } from 'react-i18next';

import BaseHeader from '@lms/components/Header';
import UserMenu from '@lms/components/UserMenu';

import { PORTAL } from '@lms/constants';
import './i18n';

import styles from './styles.module.scss';
import Avatar from '@lms/components/blocks/Avatar';

class Header extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLesson: props.history.location.pathname.startsWith('/lessons/'),
      course: {},
    };


    this.unlisten = this.props.history.listen(this.handleListen);

    window.addEventListener('courseUploaded', this.handleCourseUploaded);
  }

  componentWillUnmount () {
    this.unlisten();
    window.removeEventListener('courseUploaded', this.handleCourseUploaded);
  }

  handleListen = (location) => {
    this.setState({
      isLesson: location.pathname.startsWith('/lessons/'),
      course: {},
    });
  };

  handleCourseUploaded = (e) => {
    this.setState({
      course: {
        id: e.detail.id,
        title: e.detail.title,
      }
    })
  };

  get items() {
    const { t } = this.props;
    const { isLesson } = this.state;

    return [{
      url: '/courses',
      title: !isLesson ? t('Courses') : t('Available Courses'),
    }, isLesson ? {
      url: `/courses/${this.state.course.id}`,
      title: this.state.course.title,
    } : {
      url: '/homework',
      title: t('homework'),
    }];
  }

  handleRoleChange = (role) => {
    this.props.requestPortal({role}, this.props.history);
  };

  handleOrganizationChange = (organizationId) => {
    this.props.requestPortal({organizationId}, this.props.history);
  };

  render() {
    const { data, user, t } = this.props;

    if (data.loading) return null;
    if (data.error) return null;

    const { isLesson } = this.state;
    const { getRoles } = data;

    return (
      <BaseHeader
        fluid={!isLesson} variant={isLesson ? 'light' : 'dark'} logo={!isLesson} items={this.items}
        avatar={
          <UserMenu
              items={this.items}
              currentRole={PORTAL.TEACHER}
              user={user}
              roles={getRoles}
              onOrganizationChange={this.handleOrganizationChange}
              onRoleChange={this.handleRoleChange}>
            {t('Teacher')} <Avatar className={cn(styles.avatar, 'p-0 ml-s3')} {...user} />
          </UserMenu>
        }
      />
    );
  }
}

export default compose(
  graphql(gql.getRoles, {
    options: props => ({
      variables: {
        id: props.user.id,
      }
    })
  }),
  withTranslation('header'),
)(Header);
