import React from 'react';
import { CardColumns, Card, Badge } from 'react-bootstrap';
import * as _ from 'lodash';
import cn from 'classnames';
import { withTranslation } from 'react-i18next';

import { NavLink } from 'react-router-dom';
import Button from '@lms/components/blocks/Button';

import styles from './styles.module.scss';
import './i18n';

class Lessons extends React.PureComponent {
  render() {
    const { lessons, t } = this.props;

    return (
      <CardColumns className="row mb-4">
        {lessons.map(lesson => (
          <div className={cn(styles.cell, "col-md-4 col-lg-3 col-sm-6 col-xs-12")}
            key={lesson.id}>

            <Card className={cn(styles.card, 'm-0')}>
              <Card.Body className={cn(styles.cardBody, "flex-column d-flex")}>
                {lesson.homework && <Badge className={styles.badge}>{t("Homework")}</Badge>}
                <Card.Title className={styles.cardTitle}>{_.truncate(lesson.title, {length: 30})}</Card.Title>
                <Card.Text className={styles.cardDescription}>{_.truncate(lesson.description, {length: 150})}</Card.Text>
                <div className={styles.buttons}>
                  <NavLink className="text-light mr-3" to={`/lessons/${lesson.id}`}>
                    <Button className={styles.button} variant="dark">{t("Go to " + (lesson.homework ? "Homework" : "Lesson"))}</Button>
                  </NavLink>
                  {this.props.onAssign && (
                    <Button onClick={() => this.props.onAssign(lesson.id)} className={cn(styles.button, "transparent")}>
                    { t("Assign") }
                    </Button>
                  )}
                </div>
              </Card.Body>
            </Card>
          </div>
        ))}
      </CardColumns>
    )
  }
}

export default withTranslation('teacher/components/Lessons')(Lessons);
