import { createBrowserHistory } from 'history'
import { applyMiddleware, createStore, combineReducers, compose } from 'redux'
import { connectRouter, routerMiddleware } from 'connected-react-router'

const history = createBrowserHistory();

const reducer = combineReducers({
  router: connectRouter(history)
});

const middlewares = compose(
  applyMiddleware(
    routerMiddleware(history)
  )
);
const store = createStore(reducer, {}, middlewares);
export default store;

export { history }
