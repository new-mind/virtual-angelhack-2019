import React from 'react';
import { ApolloProvider } from 'react-apollo';
import { Container } from 'react-bootstrap';
import { Redirect, Router, Switch } from 'react-router-dom';

import Route from '@lms/components/Route';
import Header from './components/Header';
import Dashboard from './pages/Dashboard';
import Course from './pages/Course';
import Courses from './pages/Courses/Courses';
import Lesson from './pages/Lesson';
import PageFooter from '@lms/components/layouts/PageFooter';

export default function (client, history, props) {
  return (
    <ApolloProvider client={client}>
      <Container as="main" role="main" fluid="true" className="p-0 flex-column d-flex">
        <Router history={history}>
          <Header {...props} history={history} />
          <Container className="flex-grow-1 flex-shrink-1 p-0" style={{ flex: '1 1 100%' }} fluid>
            <Switch>
              <Route exact path="/courses" component={Courses} {...props}/>
              <Route exact path="/courses/:id" component={Course} {...props}/>
              <Route exact path="/lessons/:id" component={Lesson} {...props}/>

              <Route exact path="/profile" component={Dashboard} {...props}/>
              <Route exact path="/homework" component={Dashboard} {...props}/>
              <Redirect to="/courses" />
            </Switch>
          </Container>
          <PageFooter />
        </Router>
      </Container>
    </ApolloProvider>
  );
}
