import React from 'react';
import { Card, Table } from 'react-bootstrap';

import Title from '@lms/components/blocks/PageTitle';

class SchedulePage extends React.Component {
  render() {
    return (
      <>
        <h2><Title>Schedule</Title></h2>
        <Card className="my-5">
          <Card.Header>Расписание</Card.Header>
          <Card.Body>
            <Table striped bordered hover variant="dark">
              <thead>
              <tr>
                <th width="30%">Время</th>
                <th width="10%">Понедельник</th>
                <th width="10%">Вторник</th>
                <th width="10%">Среда</th>
                <th width="10%">Четверг</th>
                <th width="10%">Пятница</th>
                <th width="10%">Суббота</th>
              </tr>
              </thead>
              <tbody>
              <tr>
                <td>8:00</td>
                <td />
                <td />
                <td />
                <td />
                <td />
                <td />
              </tr>
              <tr>
                <td>9:00</td>
                <td />
                <td />
                <td />
                <td />
                <td />
                <td />
              </tr>
              <tr>
                <td>10:00</td>
                <td />
                <td />
                <td />
                <td />
                <td />
                <td />
              </tr>
              <tr>
                <td>11:00</td>
                <td>Управление портфелем ценных бумаг</td>
                <td />
                <td />
                <td />
                <td />
                <td />
              </tr>
              </tbody>
            </Table>
          </Card.Body>
        </Card>
      </>
    );
  }
}

export default SchedulePage;
