import React from 'react';
import styled from 'styled-components';

import Title from '@lms/components/blocks/PageTitle';
import List from '@lms/components/blocks/List';
import Link from '@lms/components/blocks/Link';

const coursesMock = [{"id":"ecce3cc6-47c9-4f5e-bb8e-b3458a1530d1","title":"Ride like Rodney Mullen","description":"The Alabama State Defense Force (ASDF) is the state provided guard of the State of Alabama allowed by the Constitution of Alabama, The Code of Alabama and Executive Order. It has an authorized strength of 1,000 members and is organized on the United States Army structural pattern. The ASDF is under the control of the Governor of Alabama, as the state's Commander in Chief, and comes under the authority of The Adjutant General (TAG) of Alabama. The ASDF is an adjunct, volunteer, augmenting force to the Alabama National Guard, and it is not federally recognized. Currently, the ASDF is inactive awaiting reorganization by the Alabama National Guard.","imageUrl":"https://eduway-lms-storage-dev.s3.amazonaws.com/public/ecce3cc6-47c9-4f5e-bb8e-b3458a1530d1-preview.jpg","__typename":"Course"},{"id":"465f3ac7-41ef-4f1d-94ea-38aa41c403f6","title":"Business Spanish","description":"Object learn shareObject learn share Object learn share Object learn share Object learn share Object learn share Object learn share Object learn share Object learn share Object learn share Object learn share","imageUrl":"https://eduway-lms-storage-dev.s3.amazonaws.com/public/465f3ac7-41ef-4f1d-94ea-38aa41c403f6-preview.jpg","__typename":"Course"},{"id":"0adfce3a-2a96-438c-9cf7-15608aeb8641","title":"Course for test","description":"Description for test. Description for test Description for test","imageUrl":"https://eduway-lms-storage-dev.s3.amazonaws.com/public/0adfce3a-2a96-438c-9cf7-15608aeb8641-preview.jpg","__typename":"Course"}];
const StyledList= styled(List)`
  width: 26.250em;
`
class CompletedCoursesPage extends React.Component {
  render() {
    return (
      <>
        <h2><Title>Current courses</Title></h2>
        <StyledList>
        {coursesMock.map(({ title }, i) => (
          <List.Item key={i}>
              <Link to="#">{title}</Link>
          </List.Item>
        ))}
        </StyledList>
      </>
    );
  }
}

export default CompletedCoursesPage;
