import { graphql } from 'react-apollo';
import { gql } from 'apollo-boost';

import * as optimistics from '@lms/graphql/optimistics';
import { updateStudent } from '@lms/graphql/mutations'

export default graphql(gql(updateStudent), {
  options: () => ({
    update: (store, obj) => {

    }
  }),
  props: props => ({
    updateEmployee: variables => {
      props.mutate({
        variables,
        optimisticResponse: {
          __typename: 'Mutation',
          updateStudent: {
            ...optimistics.Student,
            ...variables.input
          }
        }
      });
    }
  })
});
