import getStudent from './getStudent';
import updateStudent from './updateStudent';

export { getStudent, updateStudent }
