import { graphql } from 'react-apollo';
import { gql } from 'apollo-boost';

import { getStudent } from '@lms/graphql/queries'

export default graphql(gql(getStudent), {
  options: props => ({
    variables: {
      username: props.username
    }
  })
});
