import React from 'react';
import { Col } from 'react-bootstrap';
import { compose } from 'react-apollo';

import Title from '@lms/components/blocks/PageTitle';
import ProfileForm from '@lms/containers/forms/Profile';
import * as API from './api';

import styles from './styles.module.scss';

class Profile extends React.Component {
  handleSubmit = (values) => {
    this.props.updateStudent({
      input: {
        username: this.props.username,
        ...values
      }
    });
  };

  render() {
    const { user } = this.props;

    return (
      <Col xs={12} md={6} lg={5} className={styles.profile}>
        <Title>Profile</Title>
        <ProfileForm onSubmit={this.handleSubmit} {...user}/>
      </Col>
    );
  }
}

export default compose(
  API.updateStudent
)(Profile);
