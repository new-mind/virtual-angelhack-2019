import React from 'react';
import { Row, Col, Table } from 'react-bootstrap';
import { compose } from 'react-apollo';
import cn from 'classnames';
import moment from 'moment';
import * as _ from 'lodash';
import { Link } from 'react-router-dom';
import { withTranslation } from 'react-i18next';

import Title from '@lms/components/blocks/PageTitle';
import Legend from '@lms/components/Legend/Legend';
import * as API from './api';
import Error from '@lms/components/Error';
import Loader from '@lms/components/loader/Loader';

import styles from './styles.module.scss';
import newResponseIcon from './images/legend-new_response.svg';
import submittedIcon from './images/legend-submitted.svg';
import './i18n';

class HomeworkPage extends React.Component {
  render() {
    const { data, t } = this.props;
    const LEGEND = [{
      icon: newResponseIcon,
      label: t('New response'),
    }, {
      icon: submittedIcon,
      label: t('Submitted'),
    }/*, {
      icon: overdueIcon,
      label: 'Overdue',
    }*/];


    if (data.loading) return <Loader />;
    if (data.error) return <Error {...data.error} />;

    const { homeworks } = data.Student;
    //const now = moment();

    return (
      <>
        <Row className="d-flex align-items-center" noGutters>
          <Col>
            <Title>{t("homework")}</Title>
          </Col>
        </Row>
        <Row>
          <Col>
            <Legend items={LEGEND} />
          </Col>
        </Row>
        <Row noGutters>
          <Col className="my-5">
            <Table borderless className={styles.homework}>
              <thead>
              <tr className={styles.header}>
                <th className={styles.cell}>
                  {t("homework")}
                </th>
                <th className={cn(styles.cell, 'text-center')}>
                  {t("Assigned At")}
                </th>
              </tr>
              </thead>
              <tbody>
              {_.map(homeworks, (homework, i) => (
                <tr key={i} className={cn({[styles.completed]: homework.status === "COMPLETED"})}>
                  <td className={styles.cell}>
                    <Link to={`/lessons/${homework.id}`} className={styles.link}>
                      { this.renderStatusIcon(homework) }
                      <span className="pr-2">{homework.title}</span>
                    </Link>
                  </td>
                  <td className={styles.cell}>
                    {/*now > moment(homework.deadline) && (
                      <img src={overdueIcon} alt="Overdue" className="mr-2" />
                    )*/}
                    <span>{moment(homework.createdAt).format('DD/MM/YYYY')}</span>
                  </td>
                </tr>
              ))}
              </tbody>
            </Table>
          </Col>
        </Row>
      </>
    );
  }

  renderStatusIcon(homework) {
    if (homework.status === 'REOPEN') {
      return <img alt="Reopen" src={newResponseIcon} className="mr-2"/>
    }
    if (homework.status === 'SUBMITTED') {
      return <img alt="Submitted" src={submittedIcon} className="mr-2"/>
    }
    return null;
  }
}

export default compose(
  API.getStudent,
  withTranslation('student/subpages/Homeworks')
)(HomeworkPage);
