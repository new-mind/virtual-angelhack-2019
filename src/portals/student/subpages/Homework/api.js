import { graphql } from 'react-apollo';
import gql from 'graphql-tag';

const GET_STUDENT = gql`
  query GetStudent($userId: ID!) {
    Student(id: $userId) {
      id
      homeworks {
        id
        lessonId
        status
        title
        createdAt
      }
    }
  }
`;

export const getStudent = graphql(GET_STUDENT, {
  options: props => {
    return {
      variables: {
        userId: props.user.id
      }
    }
  }
});
