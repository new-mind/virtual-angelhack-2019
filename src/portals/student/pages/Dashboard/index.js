import React from 'react';
import { Redirect, Switch } from 'react-router-dom';
import { withTranslation } from 'react-i18next';

import Dashboard from '@lms/components/Dashboard';
import Profile from '@lms/components/Profile';
import Route from '@lms/components/Route';
import Homework from '../../subpages/Homework';

import './i18n';

class StudentDashboard extends React.Component {
  renderContent() {
    return (
      <Switch>
        <Route exact path="/profile" component={Profile} {...this.props} />
        <Route exact path="/homework" component={Homework} {...this.props} />
        <Redirect to="/profile" />
      </Switch>
    );
  }

  render() {
    const { t } = this.props;
    const menu = [
      { url: "/profile", title: t("Profile") },
      { url: "/homework", title: t("homework") }
    ];

    return (
      <Dashboard
        user={this.props.user}
        content={this.renderContent()}
        menu={menu}/>
    );
  }
}

export default withTranslation('student/pages/Dashboard')(StudentDashboard);
