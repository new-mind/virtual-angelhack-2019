import i18n from '@lms/i18n';
import { basename, requireAll } from '@lms/services/utils';

const ctx = require.context('.', true, /\.json$/);
requireAll(ctx, (requireCtx, path) => {
  i18n.addResourceBundle(basename(path), 'student/pages/Dashboard', requireCtx(path));
});
