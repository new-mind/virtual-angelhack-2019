import React  from 'react';
import _ from 'lodash';
import { Alert, Col, Row } from 'react-bootstrap';
import { compose } from 'react-apollo';
import { withTranslation } from 'react-i18next';
import { NavLink } from 'react-router-dom';

import PageContent from '@lms/components/layouts/PageContent';
import Title from '@lms/components/blocks/PageTitle';
import LessonCard from '@lms/components/LessonCard';
import Button from '@lms/components/blocks/Button';
import Loader from '@lms/components/loader/Loader';
import Error from '@lms/components/Error';
import './i18n';

import styles from './CoursePage.module.scss';

import * as API from './api';

class CoursePage extends React.Component {
  render() {
    const { data, t } = this.props;

    if (data.loading) return <Loader />;
    if (data.error) return <Error {...data.error} />;

    const { title, lessons } = data.Student.getCourse;

    return (
      <PageContent>
        <Row className="mb-xl">
          <Col>
            <Title level={2}>{title}</Title>
          </Col>
        </Row>
        <Row>
          <Col>
            {lessons.items.length ? (
              <Row className="m-ns2">
                {_.map(lessons.items, (lesson) => (
                  <Col
                    className="p-s2"
                    key={lesson.id}
                    xs={12} sm={12} md={6} lg={3} xl={3}
                  >
                    <LessonCard
                      title={lesson.title}
                      isHomework={lesson.homework}
                      actions={(
                        <>
                          <Button className={styles.button}>
                            <NavLink className="text-light" to={`/lessons/${lesson.id}`}>
                              {t('Open')}
                            </NavLink>
                          </Button>
                        </>
                      )}
                    />
                  </Col>
                ))}
              </Row>
            ) : (
              <Alert variant="warning">
                <Alert.Heading>{t("You don't have assigned lessons")}</Alert.Heading>
                <hr/>
                <p>{t("Ask your teacher for an assignment")}</p>
              </Alert>
            )}
          </Col>
        </Row>
      </PageContent>
    );
  }
}

export default compose(
  API.getStudent,
  withTranslation('student/pages/Course')
)(CoursePage);
