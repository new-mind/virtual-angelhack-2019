import gql from 'graphql-tag';
import { graphql } from 'react-apollo';

const GET_COURSE = gql`
  query GetCourse($id: ID!, $courseId: ID!) {
    Student(id: $id) {
      id
      getCourse(id: $courseId) {
        id
        title
        description
        imageUrl
        lessons {
          items {
            id
            lessonId
            title
            description
            homework
          }
          nextToken
        }
      }
    }
  }
`;

export const getStudent = graphql(GET_COURSE, {
  options: props => ({
    variables: {
      id: props.user.id,
      courseId: props.match.params.id
    }
  })
});
