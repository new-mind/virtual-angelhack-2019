import React, { Component } from 'react';
import _ from 'lodash';
import { Col, Row } from 'react-bootstrap';
import { compose } from 'react-apollo';
import { withTranslation } from 'react-i18next';

import Button from '@lms/components/blocks/Button';
import Loader from '@lms/components/loader/Loader';
import Error from '@lms/components/Error';
import Title from '@lms/components/blocks/PageTitle';
import PageContent from '@lms/components/layouts/PageContent';
import CourseCard from '@lms/components/CourseCard';
import Courses from '@lms/components/Courses';

import * as API from './api';
import './i18n';

class CoursesPage extends Component {
  render() {
    const { data, history, t } = this.props;

    if (data.loading) return <Loader />;
    if (data.error) return <Error {...data.error} />;

    const { getStudent } = data;

    const courses = _.get(getStudent, ['courses', 'items'], []);

    return (
      <PageContent>
        <Row className="mb-xl">
          <Col>
            <Title>{t("Available Courses")}</Title>
          </Col>
        </Row>
        <Courses>
          {courses.map(course => (
            <CourseCard
              key={course.id}
              {...course}
              actions={(
                <Button onClick={() => history.push(`/courses/${course.id}`)} className="mr-3">
                  {t("Go to course")}
                </Button>
              )}
            />
          ))}
        </Courses>
      </PageContent>
    );
  }
}

export default compose(
  API.getStudent,
  withTranslation('student/pages/Courses')
)(CoursesPage);
