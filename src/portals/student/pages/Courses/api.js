import gql from 'graphql-tag';
import { graphql } from 'react-apollo';

const getStudentGQL = gql`
  query GetStudent($id: ID!) {
    getStudent(id: $id) {
      id
      courses {
        items {
          id
          title
        }
        nextToken
      }
    }
  }
`;

export const getStudent = graphql(getStudentGQL, {
  options: props => ({
    variables: {
      id: props.user.id
    }
  })
});
