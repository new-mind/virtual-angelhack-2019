import gql from 'graphql-tag';
import { graphql } from 'react-apollo';
import * as _ from 'lodash';

import { fragments } from '@lms/graphql/Lesson';
import { optimistic } from '@lms/graphql';

const GET_LESSON = gql`
  query GetLesson($userId: ID!, $lessonId: ID!, $organizationId: ID!) {
    Student(id: $userId) {
      id
      getLesson(id: $lessonId, organizationId: $organizationId) {
        id
        organizationId
        lessonId
        courseId
        title
        description
        homework
        status
        rev
        course {
          id
          title
        }

        content {
          ...TextWidget
          ...VideoWidget
          ...LinkWidget
          ...MultiChoiceWidget
          ...AttachmentWidget
          ...ImageWidget
          ...on ResponseFormWidget { #TODO
            id
            title
            formType
            answers(userId: $userId) {
              ...on StudentAnswer_TextWidget {
                id
                rev
                createdAt
                updatedAt
                data
              }
              ...on StudentAnswer_LinkWidget {
                id
                rev
                createdAt
                updatedAt
                items {
                  id
                  title
                  url
                }
              }
              ...on StudentAnswer_AttachmentWidget {
                id
                rev
                createdAt
                updatedAt
                items {
                  id
                  title
                  fileKey
                  fileName
                  identityId
                }
              }
            }
          }
        }
        comments {
          items {
            content
            createdAt
            userId
          }
        }
      }
    }
  }
  ${fragments.TextWidget}
  ${fragments.VideoWidget}
  ${fragments.LinkWidget}
  ${fragments.MultiChoiceWidget}
  ${fragments.AttachmentWidget}
  ${fragments.ImageWidget}
`;

const SUBMIT_LESSON = gql`
  mutation SubmitLession($id: ID!, $organizationId: ID!) {
    submitStudentLesson(id: $id, organizationId: $organizationId) {
      id
      lessonId
      status
    }
  }
`

const UPDATE_STUDENT_ANSWER = gql`
  mutation UpdateStudentAnswer($input: UpdateStudentAnswer) {
    updateStudentAnswer(input: $input) {
      ...on StudentAnswer_TextWidget {
        id
        sectionId
        lessonId
        organizationId
        updatedAt

        rev
        data
      }

      ...on StudentAnswer_LinkWidget {
        id
        sectionId
        lessonId
        organizationId
        updatedAt

        rev
        items {
          id
          url
          title
        }
      }

      ...on StudentAnswer_AttachmentWidget {
        id
        sectionId
        lessonId
        organizationId
        updatedAt

        rev
        items {
          id
          fileKey
          fileName
          identityId
          title
        }
      }
    }
  }
`;

const ADD_LESSON_COMMENT = gql`
  mutation AddLessonCommment($input: CreateStudentLessonCommentInput!) {
    addLessonComment(input: $input) {
      id
      content
      createdAt
      userId
    }
  }
`;

export const getLesson = graphql(GET_LESSON, {
  options: props => ({
    variables: {
      userId: props.user.id,
      lessonId: props.match.params.id,
      organizationId: props.organizationId
    }
  })
});

export const submitLesson = graphql(SUBMIT_LESSON, {
  options: props => ({
    update: (store, { data: {submitStudentLesson} }) => {
      const query = GET_LESSON;
      const variables = {
        userId: props.user.id,
        lessonId: props.match.params.id,
        organizationId: props.organizationId
      };
      const data = store.readQuery({ query, variables });
      data.Student.getLesson.status = 'SUBMITTED';
      store.writeQuery({ query, variables, data });
    }
  }),
  props: ({mutate, ownProps}) => ({
    submitLesson: (id, lesson) => {
      return mutate({
        variables: {
          id,
          organizationId: lesson.organizationId
        },
        optimisticResponse: {
          __typename: "Mutation",
          submitStudentLesson: lesson
        }
      });
    },
  })
});

export const updateStudentAnswer = graphql(UPDATE_STUDENT_ANSWER, {
  options: props => ({
    update: (store, res) => {
      const { data: { updateStudentAnswer } } = res;
      const query = GET_LESSON;
      const variables = {
        userId: props.user.id,
        lessonId: props.match.params.id,
        organizationId: updateStudentAnswer.organizationId
      }
      const data = store.readQuery({ query, variables });
      const section = _.find(data.Student.getLesson.content,
        section => section.id === updateStudentAnswer.sectionId);

      section.answers[0] = updateStudentAnswer;
      store.writeQuery({ query, variables, data });
    }
  }),
  props: ({mutate, ownProps}) => ({
    updateStudentAnswer: (input) => {
      input = {
        userId: ownProps.user.id,
        ...input
      };
      return mutate({
        variables: {
          input
        },
        optimisticResponse: {
          __typename: "Mutation",
          updateStudentAnswer: optimistic.StudentAnswer(input)
        }
      })
    }
  })
});

export const addComment = graphql(ADD_LESSON_COMMENT, {
  options: props => ({
    update: (store, { data: {addLessonComment} }) => {
      const query = GET_LESSON;
      const variables = {
        userId: props.user.id,
        lessonId: props.match.params.id,
        organizationId: props.organizationId
      };
      const data = store.readQuery({ query, variables });
      data.Student.getLesson.comments.items = [
        ...data.Student.getLesson.comments.items,
        addLessonComment
      ]

      store.writeQuery({ query, variables, data });
    }
  }),
  props: ({mutate, ownProps}) => ({
    addComment: (lesson, content) => {
      return mutate({
        variables: {
          input: {
            organizationId: lesson.organizationId,
            userId: ownProps.user.id,
            lessonId: lesson.id,
            content
          }
        },
        optimisticResponse: {
          __typename: "Mutation",
          addLessonComment: {
            ...optimistic.StudentLessonComment,
            content,
            userId: ownProps.user.id
          }
        }
      });
    }
  })
})
