import React, { Component } from 'react';
import { Col } from 'react-bootstrap';
import { compose } from 'react-apollo';
import * as _ from 'lodash';
import cn from 'classnames';
import { withTranslation } from 'react-i18next';
import { withCookies } from 'react-cookie';

import Title from '@lms/components/blocks/PageTitle';
import Error from '@lms/components/Error';
import Loader from '@lms/components/loader/Loader';
import Button from '@lms/components/blocks/Button';
import Separator from '@lms/components/blocks/Separator';
import Lesson from '../../components/Lesson';
import LessonNotFound from '@lms/components/LessonNotFound';

import * as API from './api';

import styles from './styles.module.scss';
import './i18n';
import Feedback from '@lms/components/Feedback';
import PageContent from '@lms/components/layouts/PageContent';

class LessonPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      widget: null,
    };
  }

  handleChangeSection = (data) => {
    const { data: {Student: {getLesson}} } = this.props;
    const input = {
      type: data.type,
      lessonId: getLesson.lessonId,
      sectionId: data.id,
      organizationId: getLesson.organizationId,
      rev: getLesson.rev,
    };
    switch (input.type) {
      case 'TextWidget':
        input.data = data.data;
        break;
      case 'AttachmentWidget':
      case 'LinkWidget':
        input.items = data.items;
        break;
      default:
        throw new Error(`Unknown answer typename ${input.type}`);
    }
    this.props.updateStudentAnswer(input);
  };

  submitLesson = () => {
    const { data: {Student: {getLesson}} } = this.props;
    this.props.submitLesson(getLesson.id, getLesson);
  };

  get comments() {
    const { user, data } = this.props;
    const { Student: {getLesson} } = data;
    return _.chain(getLesson.comments.items)
      .sortBy('createdAt')
      .map(comment => {
        return {
          from: comment.userId === user.id ? 'STUDENT' : 'TEACHER',
          createdAt: comment.createdAt,
          content: comment.content
        }
      })
      .value();
  }

  addComment = (comment) => {
    const { Student: {getLesson} } = this.props.data;
    this.props.addComment(getLesson, comment);
  };

  collapse = () => {
    const { cookies, data } = this.props;
    const { Student: {getLesson} } = data;
    const key = `collapsed-${getLesson.id}`;
    const collapsed = cookies.get(key) === "true";
    cookies.set(key, !collapsed);
  };

  render() {
    const { data, t, cookies } = this.props;

    if (data.loading) return <Loader />;
    if (data.error) return <Error {...data.error} />;

    const { Student: { getLesson } } = data;

    if (_.isNull(getLesson)) return (
      <PageContent>
        <LessonNotFound />
      </PageContent>
    );

    const key = `collapsed-${getLesson.id}`;
    const collapsed = cookies.get(key) === "true";

    return (
      <PageContent>
        {getLesson.homework && <div className={cn(styles.homeworkMark)}>{t("Homework")}</div>}
        <Title className={styles.lessonTitle} level={2}>{getLesson.title}</Title>
        <p className={cn(styles.description, 'mb-xl')}>{getLesson.description}</p>
        { collapsed ? null :
          <Lesson
            lessonId={getLesson.lessonId}
            onChangeSection={this.handleChangeSection}
            status={getLesson.status}
            sections={getLesson.content}/>
        }
        <Separator onClick={this.collapse} collapsed={collapsed}/>

        <Feedback comments={this.comments} separatorAtRight={true}/>
        <Col
            xs={12}
            sm={12}
            md={{span: 6, offset: 6}}
            lg={{span: 6, offset: 6}}
            xl={{span: 6, offset: 6}}>
          { this.renderFeedbackForm() }
        </Col>
        { this.renderBottom() }
      </PageContent>
    )
  }

  renderFeedbackForm() {
    const { data: {Student: {getLesson}}, t } = this.props;
    if (getLesson.status !== "OPEN" &&
        getLesson.status !== "REOPEN") return null;

    return (
      <Feedback.addForm
        onSubmit={this.addComment}
        buttomAtRight={true}
        buttomText={t("Comment")}
      />
    );
  }

  renderBottom() {
    const { data: {Student: {getLesson}}, t } = this.props;
    if (!getLesson.homework) return null;

    switch (getLesson.status) {
      case 'SUBMITTED':
        return (
          <div className={styles.check}>
            <div className={styles.text}>
              {t("Your homework was sent to the teacher")}
            </div>
          </div>
        );
      case 'COMPLETED':
        return (
          <div className={styles.check}>
            <div className={styles.text}>
              {t("acceptMessage")}
            </div>
          </div>
        );
      case 'OPEN':
      case 'REOPEN':
      default:
        return (
          <Button onClick={this.submitLesson} className={cn(styles.submit, "mx-auto px-5 d-block")}>{t("Submit")}</Button>
        );
    }
  }
}

export default compose(
  API.getLesson,
  API.submitLesson,
  API.updateStudentAnswer,
  API.addComment,
  withTranslation('student/pages/Lesson'),
  withCookies
)(LessonPage);
