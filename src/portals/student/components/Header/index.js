import React, { Component } from 'react';
import cn from 'classnames';
import { withTranslation } from 'react-i18next';
import { graphql, compose } from 'react-apollo';

import * as gql from '@lms/graphql';
import BaseHeader from '@lms/components/Header';
import UserMenu from '@lms/components/UserMenu';

import styles from './styles.module.scss';
import { PORTAL } from '@lms/constants';
import Avatar from '@lms/components/blocks/Avatar';
import './i18n';

class Header extends Component {
  get items() {
    const { t } = this.props;

    return [{
      url: '/courses',
      title: t('Courses'),
    }, {
      url: '/homework',
      title: t('homework')
    }];
  }

  handleRoleChange = (role) => {
    this.props.requestPortal({role}, this.props.history);
  };

  render() {
    const { data, user, t } = this.props;

    if (data.loading) return null;
    if (data.error) return null;

    const { getRoles } = data;

    return (
      <BaseHeader
        items={this.items}
        avatar={
          <UserMenu
              items={this.items}
              currentRole={PORTAL.STUDENT}
              roles={getRoles}
              user={user}
              onRoleChange={this.handleRoleChange}>
            {t('role')} <Avatar className={cn(styles.avatar, 'p-0 ml-s3')} {...user} />
          </UserMenu>
        }
      />
    );
  }
}

export default compose(
  graphql(gql.getRoles, {
    options: props => ({
      variables: {
        id: props.user.id,
      }
    })
  }),
  withTranslation('student/header')
)(Header);
