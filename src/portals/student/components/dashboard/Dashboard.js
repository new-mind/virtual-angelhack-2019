import React, { Component } from 'react';
import {
  Badge,
  Button,
  Card,
  CardColumns,
  Col,
  Container,
  Form,
  Image,
  ListGroup,
  ProgressBar,
  Table,
} from 'react-bootstrap';
import Avatar from '@assets/teachers/2.jpg'
import { NavLink } from 'react-router-dom';

class Dashboard extends Component {
  constructor(props) {
    super(props);

    this.state = {};

  }

  render() {
    return (
      <Container className="mt-3" fluid>
        <CardColumns>
          <Card>
            <Card.Header>Профиль <Badge pill variant="dark">Матёрый</Badge></Card.Header>
            <Card.Body>
              <Form>
                <Form.Row>
                  <Form.Group as={Col} controlId="avatar">
                    <Image src={Avatar} style={{ width: '100%' }}/>
                  </Form.Group>

                  <Col>
                    <Form.Group controlId="firstName">
                      <Form.Label>Имя</Form.Label>
                      <Form.Control placeholder="Ваше имя" value="Егор" />
                    </Form.Group>
                    <Form.Group controlId="secondName">
                      <Form.Label>Фамилия</Form.Label>
                      <Form.Control placeholder="Ваша фамилия" value="Литвяков" />
                    </Form.Group>
                  </Col>
                </Form.Row>
                <Form.Group controlId="email">
                  <Form.Label>Учебный адрес электронной почты</Form.Label>
                  <Form.Control type="email" placeholder="evlitvyakov@edu.hse.ru" value="ylitvyakov@gmail.com" />
                  <Form.Text className="text-muted">
                    По этому адресу мы либо ваш учитель смогут с вами связаться для решения внештатных вопросов.
                  </Form.Text>
                </Form.Group>
                <Form.Group controlId="password">
                  <Form.Label>Password</Form.Label>
                  <Form.Control type="password" placeholder="evlitvyakov@edu.hse.ru" value="ylitvyakov@gmail.com" />
                  <Form.Text className="text-muted">
                    Here should be your password
                  </Form.Text>
                </Form.Group>
                <Form.Group controlId="phone">
                  <Form.Label>Phone Number</Form.Label>
                  <Form.Control type="tel" placeholder="+7 977 424 38 18" value="+7 977 424 38 19" />
                  <Form.Text className="text-muted">
                    Here should be your phone number
                  </Form.Text>
                </Form.Group>
                <Form.Group controlId="age">
                  <Form.Label>Возраст</Form.Label>
                  <Form.Control placeholder="Ваш возраст" value="28" />
                </Form.Group>
                <Form.Group controlId="sex">
                  <Form.Label>Пол</Form.Label>
                  <Form.Check type="radio" label="мужской" name="sex" id="m" checked />
                  <Form.Check type="radio" label="женский" name="sex" id="f" />
                  <Form.Text className="text-muted">
                    Выбрав однажды советуем не менять
                  </Form.Text>
                </Form.Group>
                <Button variant="dark" type="submit">
                  Сохранить изменения
                </Button>
              </Form>
            </Card.Body>
          </Card>
          <Card>
            <Card.Header>Текущие курсы</Card.Header>
            <Card.Body>
              <ListGroup>
                <ListGroup.Item>
                  Управление портфелем ценных бумаг
                  <ProgressBar now={60} />
                </ListGroup.Item>
                <ListGroup.Item>
                  Производные финансовые инструменты
                  <ProgressBar now={30} />
                </ListGroup.Item>
                <ListGroup.Item>
                  Конструирование структурных финансовых продуктов
                  <ProgressBar now={95} />
                </ListGroup.Item>
                <ListGroup.Item>
                  Управление финансовыми рисками
                  <ProgressBar now={15} />
                </ListGroup.Item>
              </ListGroup>
            </Card.Body>
          </Card>
          <Card>
            <Card.Header>Ближайшие события</Card.Header>
            <Card.Body>
              <ListGroup>
                <ListGroup.Item variant="info" style={{ fontSize: "8px" }}>Ближайшее занятие. Управление финансовыми рисками</ListGroup.Item>
                <ListGroup.Item>Урок 12. Расчёт вероятности дефолта компании с помощью Гауссовой копулы <Badge variant="secondary">15.06.19 18:00</Badge></ListGroup.Item>
                <ListGroup.Item variant="dark" style={{ fontSize: "8px" }}>Домашнее задание. Конструирование структурных финансовых продуктов</ListGroup.Item>
                <ListGroup.Item>Урок 10. Анализ любого струкрутрного продукта компании BCS <Badge variant="secondary">22.06.19 18:00</Badge></ListGroup.Item>
              </ListGroup>
            </Card.Body>
          </Card>
          <Card>
            <Card.Header>Пройденные курсы</Card.Header>
            <Card.Body>
              <ListGroup>
                <ListGroup.Item>Инвестиционные стратегии (Технический анализ) <Badge variant="success">95</Badge></ListGroup.Item>
                <ListGroup.Item>Инвестиционные стратегии (Фундаментальный анализ) <Badge variant="success">92</Badge></ListGroup.Item>
                <ListGroup.Item>Теория финансов <Badge variant="success">97</Badge></ListGroup.Item>
                <ListGroup.Item>Сделки по слияниям и поглащениям <Badge variant="success">100</Badge></ListGroup.Item>
              </ListGroup>
            </Card.Body>
          </Card>
          <Card>
            <Card.Header>Homework</Card.Header>
            <Card.Body>
              <ListGroup>
                <ListGroup.Item>
                  <NavLink to="/lesson/1">
                    Homework 1
                  </NavLink>
                </ListGroup.Item>
                <ListGroup.Item>
                  <NavLink to="/lesson/2">
                    Homework 2
                  </NavLink>
                </ListGroup.Item>
              </ListGroup>
            </Card.Body>
          </Card>
          <Card>
            <Card.Header>Feedback</Card.Header>
            <Card.Body>
              Учиться, учиться и ещё раз учиться
            </Card.Body>
          </Card>
        </CardColumns>
        <Card>
          <Card.Header>Расписание</Card.Header>
          <Card.Body>
            {/*@todo render schedule from data*/}
            <Table striped bordered hover variant="dark">
              <thead>
              <tr>
                <th width="30%">Время</th>
                <th width="10%">Понедельник</th>
                <th width="10%">Вторник</th>
                <th width="10%">Среда</th>
                <th width="10%">Четверг</th>
                <th width="10%">Пятница</th>
                <th width="10%">Суббота</th>
              </tr>
              </thead>
              <tbody>
              <tr>
                <td>8:00</td>
                <td />
                <td />
                <td />
                <td />
                <td />
                <td />
              </tr>
              <tr>
                <td>9:00</td>
                <td />
                <td />
                <td />
                <td />
                <td />
                <td />
              </tr>
              <tr>
                <td>10:00</td>
                <td />
                <td />
                <td />
                <td />
                <td />
                <td />
              </tr>
              <tr>
                <td>11:00</td>
                <td>Управление портфелем ценных бумаг</td>
                <td />
                <td />
                <td />
                <td />
                <td />
              </tr>
              </tbody>
            </Table>
          </Card.Body>
        </Card>
      </Container>
    );
  }
}

Dashboard.propTypes = {};

export default Dashboard;
