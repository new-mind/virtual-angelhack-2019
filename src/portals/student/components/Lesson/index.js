import React from 'react';
import { Container } from 'react-bootstrap';

import Widget from '@lms/components/widgets';

class Lesson extends React.Component {
  handleChangeSection = (data, section) => {
    const { onChangeSection } = this.props;

    onChangeSection && onChangeSection(data, section);
  }

  render() {
    const { sections, status, lessonId } = this.props;
    return (
      <Container>
        { sections.map(section => (
          <React.Fragment key={section.id}>
            <Widget
              {...section}
              status={status}
              lessonId={lessonId}
              onChange={this.handleChangeSection}/>
          </React.Fragment>
          ))
        }
      </Container>
    )
  }
}

export default Lesson;
