import React from 'react';
import { storiesOf } from '@storybook/react';
import { MemoryRouter } from 'react-router-dom';

import LessonListItem from '@lms/portals/student/components/LessonListItem';
import LessonList from '@lms/portals/student/components/LessonList';

const title = 'Lorem ipsum dolor';
const description = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam ante mi, suscipit non nibh vehicula, volutpat accumsan nunc. Vestibulum iaculis dolor arcu, varius sodales ante consequat et. Mauris convallis turpis ac libero dictum, vitae viverra neque auctor. Vivamus eget ante ac elit euismod viverra in nec mauris. Integer id leo porta, volutpat justo eu, rhoncus tellus. Suspendisse eu mattis velit. Curabitur et augue elit. Pellentesque malesuada, nulla non tempor bibendum, metus libero rhoncus nisl, ut fringilla arcu erat ut justo.';

storiesOf('Lesson:List & Lesson:Item', module)
  .addDecorator(story => (
    <MemoryRouter initialEntries={['/']}>{story()}</MemoryRouter>
  ))
  .addDecorator(story => <div className="p-xxl">{story()}</div>)
  .add('By default', () => (
    <LessonList className="mb-4">
      <LessonListItem title={title} description={description} id={1} homework />
      <LessonListItem title={title} description={description} id={2} />
      <LessonListItem title={title} description={description} id={3} homework />
      <LessonListItem title={title} description={description} id={4} />
      <LessonListItem title={title} description={description} id={5} />
      <LessonListItem title={title} description={description} id={6} />
    </LessonList>
  ));
