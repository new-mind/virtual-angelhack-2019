import React from 'react';
import cn from 'classnames';
import { CardColumns, Row } from 'react-bootstrap';

// @todo implement DnD items
// @todo implement adding&removing items

class LessonList extends React.Component {
  render() {
    const { className, children, ...props } = this.props;

    return <CardColumns as={Row} noGutters className={cn(className)} {...props}>{children}</CardColumns>;
  }
}

export default LessonList;
