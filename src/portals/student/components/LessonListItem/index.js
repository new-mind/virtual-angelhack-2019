import React from 'react';
import cn from 'classnames';
import _ from 'lodash';
import { Badge, Card, Col } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import { withTranslation } from 'react-i18next';

import Button from '@lms/components/blocks/Button';

import styles from './styles.module.scss';
import './i18n';

class LessonListItem extends React.Component {
  get title() {
    return _.truncate(this.props.title, { length: 30 });
  }

  get description() {
    return _.truncate(this.props.description, { length: 150 });
  }

  get url() {
    return `/lessons/${this.props.id}`;
  }

  get urlTitle() {
    const { t } = this.props;
    return t(`Go to ${this.props.homework ? 'Homework' : 'Lesson'}`);
  }

  render() {
    return (
      <Col xs={12} sm={6} md={4} lg={3}>
        <Card className={cn(styles.item, 'd-flex flex-column w-auto m-s2 pt-s3 px-m2 pb-m2')}>
          <Card.Body className= "p-0 mb-s3">
            {this._renderBadge()}
            <Card.Title className={styles.title}>{this.title}</Card.Title>
            <Card.Text className={styles.description}>{this.description}</Card.Text>
          </Card.Body>
          <Card.Footer className={cn(styles.actions, "p-0")}>
            <NavLink className="text-light" to={this.url}>
              <Button variant="dark" className={cn(styles.action, 'py-s2 px-s3')}>
                {this.urlTitle}
              </Button>
            </NavLink>
          </Card.Footer>
        </Card>
      </Col>
    );
  }

  _renderBadge() {
    const { t } = this.props;

    return (
      <div className={cn(styles.status, 'mb-m1')}>
        {this.props.homework && (
          <Badge className={cn(styles.badge, styles.homework, 'ml-auto px-s1')}>
            {t("Homework")}
          </Badge>
        )}
      </div>
    );
  }
}

export default withTranslation('student/components/LessonListItem')(LessonListItem);
