import React from 'react';
import { ApolloProvider } from 'react-apollo';
import { Container } from 'react-bootstrap';
import { Redirect, Router, Switch } from 'react-router-dom';

import Route from '@lms/components/Route';
import Header from './components/Header';
import Courses from './pages/Courses';
import Lesson from './pages/Lesson';
import LessonPreview from './pages/LessonPreview';
import Users from './pages/Users';
import Course from './pages/Course';
import Student from './pages/Student';
import Dashboard from './pages/Dashboard';
import PageFooter from '@lms/components/layouts/PageFooter';

export default function App(client, history, props) {
  return (
    <ApolloProvider client={client}>
      <Container as="main" role="main" fluid="true" className="p-0 flex-column d-flex">
        <Router history={history}>
          <Header className="" {...props} history={history} />
          <Container className="flex-grow-1 flex-shrink-1 p-0" style={{ flex: '1 1 100%' }} fluid>
            <Switch>
              <Route exact path="/courses" component={Courses} {...props} />
              <Route exact path="/courses/:id?" component={Course} {...props} />
              <Route exact path="/users/:type?" component={Users} {...props} />
              <Route exact path="/students/:username" component={Student} {...props} />
              <Route exact path="/lessons/:id" component={Lesson} {...props} />
              <Route exact path="/lessons/:id/preview" component={LessonPreview} {...props} />

              <Route exact path="/profile" component={Dashboard} {...props} />
              <Redirect to="/courses" />
            </Switch>
          </Container>
          <PageFooter />
        </Router>
      </Container>
    </ApolloProvider>
  );
}
