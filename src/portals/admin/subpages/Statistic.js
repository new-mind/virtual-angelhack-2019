import React from 'react';
import { Card, CardColumns } from 'react-bootstrap';

import Title from '@lms/components/blocks/PageTitle';

class StatisticPage extends React.Component {
  render() {
    return (
      <React.Fragment>
        <h2><Title>Statistic</Title></h2>
        <CardColumns className="my-5">
          <Card>
            <Card.Header>Teacher statistic</Card.Header>
            <Card.Body>
              Here should be data
            </Card.Body>
          </Card>
          <Card>
            <Card.Header>Student statstic</Card.Header>
            <Card.Body>
              Here should be data
            </Card.Body>
          </Card>
        </CardColumns>
      </React.Fragment>
    );
  }
}

export default StatisticPage;
