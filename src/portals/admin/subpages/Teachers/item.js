import React from 'react';
import styles from './styles.module.scss';
import PropTypes from 'prop-types';
import cn from 'classnames';
import { FaEllipsisH as Menu } from 'react-icons/fa';
import { ListGroup, OverlayTrigger, Popover, Dropdown } from 'react-bootstrap';
import * as _ from 'lodash';
import { withTranslation } from 'react-i18next';

import Confirm from '@lms/components/blocks/Confirm';
import List from '@lms/components/blocks/List';

class ContextMenu extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(e) {
    e.preventDefault();

    this.props.onClick(e);
  }

  render() {
    return (
      <Menu onClick={this.handleClick} />
    );
  }
}

const popover = (assignedCourses = []) => (
  <Popover>
    <ListGroup variant="flush">
      {assignedCourses.map((item, index) => (
        <ListGroup.Item key={index} className={cn(styles.item, styles['item_marked'])}>
          {_.get(item, 'course.title', '')}
        </ListGroup.Item>
      ))}
    </ListGroup>
  </Popover>
);

class Item extends React.Component {
  static propTypes = {
    index: PropTypes.number,
    name: PropTypes.string,
    surname: PropTypes.string,
    email: PropTypes.string,
    assignedCourses: PropTypes.arrayOf(PropTypes.object),
    onAssign: PropTypes.func,
    onRemove: PropTypes.func,
    onComment: PropTypes.func,
  };

  static defaultProps = {
    index: null,
    name: 'Not Specified',
    surname: 'Not Specified',
    email: 'Not Specified',
    assignedCourses: [],
    onAssign: () => {},
    onRemove: null,
    onComment: () => {},
  };

  render() {
    const { index, name, surname, email, onAssign, onRemove, assignedCourses, t } = this.props;

    return (
      <div className={styles.item}>
        {index && (<div className={cn(styles['item__cell-number'], styles['item__cell'])}>{index})</div>)}
        <div className={cn(styles['item__cell-name'], styles['item__cell'])}>{name}</div>
        <div className={cn(styles['item__cell-surname'], styles['item__cell'])}>{surname}</div>
        <div className={cn(styles['item__cell-email'], styles['item__cell'])}>{email}</div>
        <div
          className={cn(styles['item__cell-assigning'], styles['item__cell'])}
          onClick={onAssign}
        >
          {assignedCourses.length ? (
            <OverlayTrigger trigger="hover" placement="bottom" overlay={popover(assignedCourses)}>
              <p className={'m-0'}>
                Assign Course
                <svg width="6" height="16" viewBox="0 0 6 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M4.66048 8C4.66048 12.4183 7.07871 16 2.66043 16C-1.75785 16 0.660475 12.4183 0.660475 8C0.660475 3.58172 -1.75785 0 2.66043 0C7.07871 0 4.66048 3.58172 4.66048 8Z" fill="#E85668"/>
                </svg>
              </p>
            </OverlayTrigger>
          ) : ''}
        </div>
        <div className={cn(styles['item__cell-comments'], styles['item__cell'], 'text-md-right')}>
        </div>
        <div className={cn(styles['item__cell-menu'], styles['item__cell'], 'text-md-right')}>
          <Dropdown>
            <Dropdown.Toggle as={ContextMenu} />
            {onRemove && (
              <Dropdown.Menu>
                <List.Item size="sm" className={styles['item__menu-item']}>
                  <Confirm
                    onClick={onRemove}
                    className="transparent"
                    confirm={t("Confirm")}
                  >
                    {t("Remove")}
                  </Confirm>
                </List.Item>
              </Dropdown.Menu>
            )}
          </Dropdown>
        </div>
      </div>
    );
  }
}

export default withTranslation('admin/subpages/Teachers')(Item);
