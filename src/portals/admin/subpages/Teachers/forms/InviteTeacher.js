import React from 'react';
import { Modal, Form } from 'react-bootstrap';
import * as yup from 'yup';
import { Formik } from 'formik';
import { withTranslation } from 'react-i18next'

import Button from '@lms/components/blocks/Button';
import * as API from '../api';

const schema = yup.object({
  email: yup.string().email().required()
});

class InviteTeacher extends React.Component {
  state = {
    email: null,
    password: null
  };

  invite = (data) => {
    this.props.inviteTeacher({
      input: {
        organizationId: this.props.organizationId,
        email: data.email,
      },
    });
    this.props.onClose();
  };

  render() {
    const { t } = this.props;

    return (
      <Modal show={this.props.show} onHide={this.props.onClose}>
        <Formik
          validationSchema={schema}
          onSubmit={this.invite}
        >
        {({
          touched,
          errors,
          handleChange,
          handleSubmit
        }) => (
          <Form noValidate onSubmit={handleSubmit}>
            <Modal.Header closeButton>
              <Modal.Title>{t("Invite teacher")}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form.Group>
                <Form.Control
                  name="email"
                  type="text"
                  placeholder={t("Enter email")}
                  isInvalid={touched.email && errors.email}
                  isValid={touched.email && !errors.email}
                  onChange={handleChange}
                />
                <Form.Control.Feedback type="invalid">
                  {errors.email}
                </Form.Control.Feedback>
              </Form.Group>
              <Form.Text className="text-muted">
                  {t("inviteDescription")}
              </Form.Text>
            </Modal.Body>
            <Modal.Footer>
              <Button className="transparent" onClick={this.props.onClose}>{t("Close")}</Button>
              <Button type="submit">{t("Invite")}</Button>
            </Modal.Footer>
          </Form>
        )}
        </Formik>
      </Modal>
    )
  }
}

export default API.inviteTeacher(
  withTranslation('admin/subpages/Teachers')(InviteTeacher)
);
