import React, { Fragment } from 'react';
import { Row, Col } from 'react-bootstrap';
import { withTranslation } from 'react-i18next';
import * as _ from 'lodash';

import './i18n';
import * as API from './api';
import Loader from '@lms/components/loader/Loader';
import Error from '@lms/components/Error';
import Button from '@lms/components/blocks/Button';
import InviteTeacherForm from './forms/InviteTeacher';
import Title from '@lms/components/blocks/PageTitle';
import Item from './item';
import { compose } from 'react-apollo';

class Teachers extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showInviteForm: false
    };
  }

  handleRemove = (id, organizationId) => {
    this.props.deleteTeacher({
      input: {
        id,
        organizationId,
      },
    });
  };

  render() {
    const { data, organizationId, t } = this.props;
    const { showInviteForm } = this.state;
    if (data.loading ) return <Loader />;
    if (data.error) return <Error {...data.error} />;

    let teachers = data.listTeachers.items;

    return (
      <Fragment>
        <Row className="my-xl">
          <Col>
            <Title>{t("Teachers")}</Title>
          </Col>
          <Col className="d-flex align-items-center justify-content-end">
            <Button
              style={{ textTransform: 'uppercase' }}
              variant="dark"
              onClick={() => this.setState({ showInviteForm: !showInviteForm })}
            >
              {t("Invite teacher")}
            </Button>
            <InviteTeacherForm
              show={showInviteForm}
              organizationId={organizationId}
              onClose={() => { this.setState({ showInviteForm: false })} }
            />
          </Col>
        </Row>
        <div>
          {_.map(teachers, (teacher, index) => {
            return (
              <Item
                key={index}
                index={index + 1}
                onRemove={() => this.handleRemove(teacher.id, organizationId)}
                {...teacher}
              />
            );
          })}
        </div>
      </Fragment>
    )
  }
}

export default compose(
  API.listTeachers,
  API.deleteTeacher,
)(withTranslation('admin/subpages/Teachers')(Teachers));
