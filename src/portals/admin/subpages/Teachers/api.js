import { graphql } from 'react-apollo';

import * as gql from '@lms/graphql';
import { optimistic } from '@lms/graphql';

const LIMIT = null;
export const listTeachers = graphql(gql.listTeachers, {
  options: props => ({
    variables: {
      filter: {
        organizationId: props.organizationId
      },
      limit: LIMIT,
    }
  })
});

export const inviteTeacher = graphql(gql.inviteTeacher, {
  options: props => ({
    update: (store, {data: {inviteTeacher}}) => {
      const query = gql.listTeachers;
      const filter = { organizationId: props.organizationId };
      const variables = { filter, limit: LIMIT };
      const data = store.readQuery({ query, variables });

      data.listTeachers.items = [
        inviteTeacher,
        ...data.listTeachers.items.filter(item => item.id !== inviteTeacher.id)
      ];
      store.writeQuery({query, variables, data});
    }
  }),
  props: ({mutate}) => ({
    inviteTeacher: variables => {
      return mutate({
        variables,
        optimisticResponse: {
          __typename: "Mutation",
          inviteTeacher: {
            ...optimistic.Teacher,
            ...variables.input
          }
        }
      })
    }
  })
});

export const deleteTeacher = graphql(gql.deleteTeacher, {
  options: props => ({
    update: (dataProxy, response) => {
      const query = gql.listTeachers;
      const filter = { organizationId: props.organizationId };
      const variables = { filter, limit: LIMIT, organizationId: props.organizationId };
      const data = dataProxy.readQuery({ query, variables });

      data.listTeachers.items = [
        ...data.listTeachers.items.filter(item => item.id !== response.data.deleteTeacher.id),
      ];

      dataProxy.writeQuery({ query, variables, data });
    },
  }),
  props: props => ({
    deleteTeacher: (variables) => {
      props.mutate({
        variables,
        optimisticResponse: {
          __typename: 'Mutation',
          deleteTeacher: {
            ...optimistic.Teacher,
            ...variables.input,
          },
        },
      });
    },
  }),
});
