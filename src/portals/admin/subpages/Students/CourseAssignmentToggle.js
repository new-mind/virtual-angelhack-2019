import React from 'react';
import uid from 'uuid/v1';
import { withTranslation } from 'react-i18next';

import './i18n';
import styles from './styles.module.scss';

class CourseAssignmentToggle extends React.Component {
  show = React.createRef();
  hide = React.createRef();

  state = {
    id: uid(),
  };

  componentDidUpdate(prev) {
    if (this.props.isOpen !== prev.isOpen) {
      if (!this.props.isOpen) {
        const node = this.hide.current;

        node.beginElement();
      } else {
        const node = this.show.current;

        node.beginElement();
      }
    }
  }

  render() {
    const { t } = this.props;
    return (
      <div onClick={this.props.onClick} className={styles.toggle}>
        <span style={{ userSelect: 'none' }}>{t("Assign Course")}</span>
        <svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="8" height="16" viewBox="0 0 8 16">
          <path
            id={this.state.id}
            d="M8 8C8 10.20915 6.20914 12 4 12C1.79086 12 0 10.20915 0 8C0 5.79086 1.79086 4 4 4C6.20914 4 8 5.79086 8 8Z"
            style={{ userSelect: 'none' }}
          />
          <animate
            ref={this.show}
            href={`#${this.state.id}`}
            begin="indefinite"
            attributeName="d"
            attributeType="XML"
            from="M8 8C8 10.20915 6.20914 12 4 12C1.79086 12 0 10.20915 0 8C0 5.79086 1.79086 4 4 4C6.20914 4 8 5.79086 8 8Z"
            to="M5.91048 8C5.91048 12.4183 8.32871 16 3.91043 16C -0.50785 16 1.910475 12.4183 1.910475 8C1.910475 3.58172 -0.50785 0 3.91043 0C8.32871 0 5.91048 3.58172 5.91048 8Z"
            dur="75ms"
            fill="freeze"
          />
          <animate
            ref={this.hide}
            href={`#${this.state.id}`}
            begin="indefinite"
            attributeName="d"
            attributeType="XML"
            from="M5.91048 8C5.91048 12.4183 8.32871 16 3.91043 16C -0.50785 16 1.910475 12.4183 1.910475 8C1.910475 3.58172 -0.50785 0 3.91043 0C8.32871 0 5.91048 3.58172 5.91048 8Z"
            to="M8 8C8 10.20915 6.20914 12 4 12C1.79086 12 0 10.20915 0 8C0 5.79086 1.79086 4 4 4C6.20914 4 8 5.79086 8 8Z"
            dur="75ms"
            fill="freeze"
          />
        </svg>
      </div>
    );
  }
}

export default withTranslation('admin/subpages/Students', {withRef: true})(CourseAssignmentToggle);
