import React from 'react';
import { Col, Row, Table } from 'react-bootstrap';
import { compose } from 'react-apollo';
import * as _ from 'lodash';
import { withTranslation } from 'react-i18next';

import Loader from '@lms/components/loader/Loader';
import Error from '@lms/components/Error';
import InviteStudentForm from './forms/InviteStudent';
import Button from '@lms/components/blocks/Button';
import * as API from './api';
import Title from '@lms/components/blocks/PageTitle';
import Item from './Item';

import './i18n';

class Students extends React.Component {
  state = {
    showInviteForm: false,
    showCommentForm: false,
    loading: false,
    selectedStudent: null,
    focusedItemIndex: -1,
  };

  loadMore(nextToken) {
    this.setState({loading: true});
    this.props.loadMore(nextToken, () => {
      this.setState({loading: false});
    })
  }

  deleteStudent(userId) {
    this.props.deleteStudent({
      input: {
        id: userId,
        organizationId: this.props.organizationId,
      }
    });
  }

  render() {
    const { data, students, nextToken, organizationId, t } = this.props;
    //TODO move out to HOC
    if (data.loading) return <Loader />;
    if (data.error) return <Error {...data.error} />;

    const { showInviteForm, showCommentForm } = this.state;

    return (
      <>
        <Row className="my-xl">
          <Col>
            <Title>{t("Students")}</Title>
          </Col>
          <Col className="d-flex align-items-center justify-content-end">
            <Button
              variant="dark"
              onClick={() => this.setState({ showInviteForm: !showInviteForm })}
            >
              { _.toUpper(t("Invite student")) }
            </Button>
            <InviteStudentForm
              courses={data.listCourses.items}
              organizationId={organizationId}
              show={showInviteForm}
              onClose={() => { this.setState({ showInviteForm: false }) } }
            />
          </Col>
        </Row>
        <Table borderless>
          <tbody>
            {students.map((student, index) => (
              <Item
                isBlurred={this.state.focusedItemIndex >= 0 && this.state.focusedItemIndex !== index}
                key={index}
                index={index}
                organizationId={organizationId}
                onFocus={(i) => this.setState({ focusedItemIndex: i })}
                onRemove={() => this.deleteStudent(student.id)}
                onComment={() => this.setState({ showCommentForm: !showCommentForm, selectedStudent: student })}
                courses={data.listCourses.items}
                student={student}
              />
            ))}
          </tbody>
          {/* https://redmine.eduway.today/issues/360
          <CommentForm
            show={showCommentForm}
            onClose={() => { this.setState({ showCommentForm: false }) }}
          />
          */}
        </Table>
        {this.state.loading ? <Loader/> : null}
        {nextToken && <Button
          variant="dark"
          className="mb-3"
          onClick={() => this.loadMore(nextToken)}
        >
          {t("Load More")}
        </Button>
        }
      </>
    )
  }
}

export default compose(
  API.listStudents,
  API.listCourses,
  API.deleteStudent
)(withTranslation('admin/subpages/Students')(Students));
