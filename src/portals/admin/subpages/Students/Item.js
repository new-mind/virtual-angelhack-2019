import React from 'react';
import cn from 'classnames';
import { ListGroup, Dropdown } from 'react-bootstrap';
import * as _ from 'lodash';
import { compose } from 'react-apollo';
import { withTranslation } from 'react-i18next';

import Confirm from '@lms/components/blocks/Confirm';
import List from '@lms/components/blocks/List';
import * as API from './api';

import * as props from './props';
import './i18n';

import MenuToggle from './MenuToggle';
import CourseAssignmentToggle from './CourseAssignmentToggle';

import styles from './styles.module.scss';

class Item extends React.Component {
  state = {
    isOpen: false,
  };

  static propTypes = props.propTypes;

  static defaultProps = props.defaultProps;

  handleClick = (course) => {
    let assignment = this.assignments[course.id];

    if (!assignment) {
      this.props.addCourse(course.id);
    } else {
      this.props.deleteCourse(course.id);
    }
  };

  get assignments() {
    const { student } = this.props;
    return _.keyBy(student.courseIds, (id) => id);
  }

  handleCourseAssignmentToggle = () => {
    this.props.onFocus(!this.state.isOpen ? this.props.index : -1);
    this.setState({ isOpen: !this.state.isOpen });
  };

  render() {
    const { index, student, onRemove, courses, isBlurred, t } = this.props;

    return (
      <tr className={cn(styles.item, isBlurred && styles.blurry)}>
        <td className="text-left">{index + 1})</td>
        <td>{student.name}</td>
        <td>{student.surname}</td>
        <td>{student.email}</td>
        <td className="text-center">
          <Dropdown onToggle={this.handleCourseAssignmentToggle}>
            <Dropdown.Toggle as={CourseAssignmentToggle} isOpen={this.state.isOpen} />
            <Dropdown.Menu>
              <ListGroup>
                {courses.map(course => (
                  <ListGroup.Item
                    key={course.id}
                    className={cn(styles.item, styles.small, this.assignments[course.id] && styles.selected)}
                    onClick={() => this.handleClick(course)}
                  >
                    {_.get(course, 'title', '')}
                  </ListGroup.Item>
                ))}
              </ListGroup>
            </Dropdown.Menu>
          </Dropdown>
        </td>
        <td className="text-right">
          <Dropdown>
            <Dropdown.Toggle as={MenuToggle} />
            <Dropdown.Menu>
              <List.Item size="sm" className={styles.menuItem}>
                <Confirm
                  onClick={onRemove}
                  className="transparent"
                  confirm={t("Confirm")}
                >
                  {t("Remove")}
                </Confirm>
              </List.Item>
            </Dropdown.Menu>
          </Dropdown>
        </td>
      </tr>
    );
  }
}

export default compose(
  API.addCourses,
  API.deleteCourses
)(withTranslation('admin/subpages/Students')(Item));
