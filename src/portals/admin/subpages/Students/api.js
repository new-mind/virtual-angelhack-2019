import * as _ from 'lodash';
import { graphql } from 'react-apollo';
import { defaultDataIdFromObject } from 'apollo-cache-inmemory';
import gqlTag from 'graphql-tag';

import * as gql from '@lms/graphql';
import { optimistic } from '@lms/graphql';

const INVITE_STUDENTS = gqlTag`
  mutation InviteStudents($input: InviteStudentsInput) {
    inviteStudents(input: $input) {
      id
      email
      name
      surname
      phone
      age
      courseIds
    }
  }
`;

const LIMIT = null;
export const listStudents = graphql(gql.listStudents, {
  options: props => ({
    variables: {
      filter: {
        organizationId: props.organizationId,
      },
      limit: LIMIT,
      organizationId: props.organizationId //TODO
    }
  }),
  props: ({data, ownProps}) => ({
    students: data.listStudents ? data.listStudents.items : [],
    data,
    nextToken: data.listStudents ? data.listStudents.nextToken : null,
    loadMore: (nextToken, cb) => {
      return data.fetchMore({
        query: gql.listStudents,
        variables: {
          nextToken,
          limit: LIMIT,
          filter: { organizationId: ownProps.organizationId },
        },
        updateQuery: (previousResult, { fetchMoreResult: {listStudents} }) => {
          cb && cb();
          return {
            listStudents: {
              nextToken: listStudents.nextToken,
              items: [
                ...previousResult.listStudents.items,
                ...listStudents.items
              ],
              __typename: previousResult.listStudents.__typename
            },
          }
        }
      });
    }
  })
});

export const deleteStudent = graphql(gql.deleteStudent, {
  options: props => ({
    update: (store, {data: {deleteStudent}}) => {
      const query = gql.listStudents;
      const filter = { organizationId: props.organizationId };
      const variables = { filter, limit: LIMIT, organizationId: props.organizationId }; //TODO
      const data = store.readQuery({ query, variables });

      data.listStudents.items = [
        ...data.listStudents.items.filter(item => item.id !== deleteStudent.id)
      ];

      store.writeQuery({ query, variables, data });
    }
  }),
  props: props => ({
    deleteStudent: (variables) => {
      props.mutate({
        variables,
        optimisticResponse: {
          __typename: 'Mutation',
          deleteStudent: {
            ...optimistic.Student,
            ...variables.input
          }
        }
      })
    }
  })
});

export const inviteStudent = graphql(gql.inviteStudent, {
  options: props => ({
    update: (store, {data: {inviteStudent}}) => {
      const query = gql.listStudents;
      const filter = { organizationId: props.organizationId };
      const variables = { filter, limit: LIMIT, organizationId: props.organizationId }; //TODO
      const data = store.readQuery({ query, variables }); //TODO

      data.listStudents.items = [
        inviteStudent,
        ...data.listStudents.items.filter(item => item.id !== inviteStudent.id)
      ]
      store.writeQuery({query, variables, data});
    }
  }),
  props: props => ({
    inviteStudent: variables => {
      props.mutate({
        variables,
        optimisticResponse: {
          __typename: "Mutation",
          inviteStudent: {
            ...optimistic.Student,
            ...variables.input
          }
        }
      })
    }
  })
});

export const inviteStudents = graphql(INVITE_STUDENTS, {
  options: props => ({
    update: (store, {data: {inviteStudents}}) => {
      const query = gql.listStudents;
      const filter = { organizationId: props.organizationId };
      const variables = { filter, limit: LIMIT, organizationId: props.organizationId };
      const data = store.readQuery({ query, variables });

      const ids = {};
      _.forEach(inviteStudents, student => {
        ids[student.id] = true;
      });

      data.listStudents.items = [
        ...inviteStudents,
        ...data.listStudents.items.filter(item => !ids[item.id])
      ]
      store.writeQuery({query, variables, data});
    }
  }),
  props: props => ({
    inviteStudents: variables => {
      props.mutate({
        variables,
        optimisticResponse: {
          __typename: "Mutation",
          inviteStudents: _.map(variables.input.emails, (email, i) => ({
            ...optimistic.Student,
            email,
            id: -1 * (i + 1),
            courseIds: variables.input.courses
          }))
        }
      })
    }
  })
});

export const listCourses = graphql(gql.listCourses, {
  skip: props => !(props.data && props.data.listStudents), // TODO: make parallel 
  options: props => ({
    variables: {
      limit: null,
      filter: {
        organizationId: props.organizationId
      }
    }
  }),
  props: props => ({
    data: {
      ...props.ownProps.data,
      ...props.data,
    },
  }),
});

export const addCourses = graphql(gql.addCourses, {
  options: props => ({
    update: (store, {data: {Student}}) => {
      const id = defaultDataIdFromObject(Student);
      const variables = { organizationId: props.organizationId }; //TODO
      const fragment = store.readFragment({
        id,
        variables,
        fragment: gql.fragment
      });

      fragment.courseIds = [
        Student.addCourses[0],
        ...fragment.courseIds
      ]

      store.writeFragment({
        id,
        variables,
        fragment: gql.fragment,
        data: fragment
      });
    }
  }),
  props: ({mutate, ownProps}) => ({
    addCourse: (id) => {
      return mutate({
        variables: {
          id: ownProps.student.id,
          input: [id]
        },
        optimisticResponse: {
          __typename: "Mutation",
          Student: {
            ...optimistic.Student,
            id: ownProps.student.id,
            addCourses: [id]
          }
        },
      });
    }
  })
});

export const deleteCourses = graphql(gql.deleteCourses, {
  options: props => ({
    update: (store, {data: {Student}}) => {
      const id = defaultDataIdFromObject(Student);
      const variables = { organizationId: props.organizationId }; //TODO
      const fragment = store.readFragment({
        id,
        variables,
        fragment: gql.fragment
      });

      fragment.courseIds = _.filter(fragment.courseIds, (id) => id !== Student.deleteCourses[0]);

      store.writeFragment({
        id,
        variables,
        fragment: gql.fragment,
        data: fragment
      });
    }
  }),
  props: ({mutate, ownProps}) => ({
    deleteCourse: (id) => {
      return mutate({
        variables: {
          id: ownProps.student.id,
          input: [id]
        },
        optimisticResponse: {
          __typename: "Mutation",
          Student: {
            ...optimistic.Student,
            id: ownProps.student.id,
            deleteCourses: [id]
          }
        }
      })
    }
  })
});
