import React from 'react';
import { Modal, Form } from 'react-bootstrap';
import { Formik } from 'formik';
import { withTranslation } from 'react-i18next';
import { ListGroup } from 'react-bootstrap';
import _ from 'lodash';
import cn from 'classnames';

import '../../i18n'
import styles from './styles.module.scss';
import * as API from '../../api';
import Button from '@lms/components/blocks/Button';
import Input from '@lms/components/blocks/Input';

class InviteStudent extends React.Component {
  state = {
    emails: [],
    courses: {}
  }

  invite = (data, form) => {
    const { emails, courses } = this.state;
    if (!emails.length) return;

    const courseIds = [];
    for (let k in courses) {
      if (courses[k]) courseIds.push(k);
    }

    this.props.inviteStudents({
      input: {
        organizationId: this.props.organizationId,
        emails: emails,
        courses: courseIds
      }
    })
    this.setState({ emails: [], courses: {} });
    this.props.onClose();
  };

  toggleCourse = (id) => {
    const courses = this.state.courses;

    courses[id] = !courses[id];
    this.setState({courses});
  }

  render() {
    const { t, courses } = this.props;
    return (
      <Modal show={this.props.show} onHide={this.props.onClose}>
        <Formik onSubmit={this.invite}>
        {({
          touched,
          errors,
          handleChange,
          handleSubmit
        }) => (
          <Form noValidate onSubmit={handleSubmit}>
          <Modal.Header closeButton>
            <Modal.Title>{t("inviteForm.title")}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Group>
              <Input.Emails t={t}
                emails={this.state.emails}
                onChange={(emails) => {
                  this.setState({ emails });
                }}/>
                <Form.Control.Feedback type="invalid">
                  {errors.emails}
                </Form.Control.Feedback>
            </Form.Group>
            {/*
            <Form.Text className="text-muted">
              {t("inviteForm.description")}
            </Form.Text>
            */}
            <Form.Group className="mt-3">
              <Form.Label> </Form.Label>
              <ListGroup className={styles.list}>
                {_.map(courses, (course, i) => (
                  <ListGroup.Item
                      key={i}
                      className={cn(styles.item, {enabled: this.state.courses[course.id]})}
                      onClick={() => this.toggleCourse(course.id)}>

                    <div className={styles.title}>{course.title}</div>
                    <span className={styles.bullet} />
                  </ListGroup.Item>
                ))}
              </ListGroup>
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button className="transparent" onClick={this.props.onClose}>{t("Close")}</Button>
            <Button type="submit">{t("Invite")}</Button>
          </Modal.Footer>
          </Form>
        )}
        </Formik>
      </Modal>
    )
  }
}

export default API.inviteStudents(
  withTranslation('admin/subpages/Students')(InviteStudent)
);
