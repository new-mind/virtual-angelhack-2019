import React, { Component } from 'react';
import { Form, Modal } from 'react-bootstrap';
import { Formik } from 'formik';
import Button from '@lms/components/blocks/Button';
import * as yup from 'yup';

const schema = yup.object({});

class Comment extends Component {
  render() {
    return (
      <Modal show={this.props.show} onHide={this.props.onClose}>
        <Formik
          validationSchema={schema}
          onSubmit={this.invite}
        >
          {({
              touched,
              errors,
              handleChange,
              handleSubmit
            }) => (
            <Form noValidate onSubmit={handleSubmit}>
              <Modal.Header closeButton>
                <Modal.Title>Comments</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <Form.Text>
                  Цепочка обратной связи по студенту
                </Form.Text>
              </Modal.Body>
              <Modal.Footer>
                <Button variant="light" onClick={this.props.onClose}>
                  Close
                </Button>
                <Button variant="dark" type="submit">
                  Save
                </Button>
              </Modal.Footer>
            </Form>
          )}
        </Formik>
      </Modal>
    );
  }
}

export default Comment;
