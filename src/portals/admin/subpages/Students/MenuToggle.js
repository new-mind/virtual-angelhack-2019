import React from 'react';
import { FaEllipsisH as Menu } from 'react-icons/fa';

class MenuToggle extends React.Component {
  handleClick = (e) => {
    e.preventDefault();

    this.props.onClick(e);
  };

  render() {
    return (
      <Menu onClick={this.handleClick} />
    );
  }
}

export default MenuToggle;
