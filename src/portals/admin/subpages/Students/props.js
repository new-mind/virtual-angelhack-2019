import PropTypes from 'prop-types';

export const propTypes = {
  index: PropTypes.number.isRequired,
  name: PropTypes.string,
  surname: PropTypes.string,
  email: PropTypes.string,
  onRemove: PropTypes.func,
  onComment: PropTypes.func,
};

export const defaultProps = {
  name: 'Not Specified',
  surname: 'Not Specified',
  email: 'Not Specified',
  onRemove: () => {},
  onComment: () => {},
};
