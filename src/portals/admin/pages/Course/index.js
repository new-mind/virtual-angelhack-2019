import React, { Component } from 'react'
import { Card, CardColumns, Col, Row } from 'react-bootstrap';
import { compose, withApollo  } from 'react-apollo';
import * as _ from 'lodash';
import cn from 'classnames';
import { DndProvider } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import { withTranslation } from 'react-i18next';

import * as gql from '@lms/graphql';
import Loader from '@lms/components/loader/Loader';
import Error from '@lms/components/Error';
import Title from '@lms/components/blocks/PageTitle';
import Text from '@lms/components/blocks/Text';
import CreateLessonForm from './forms/CreateLesson';
import LessonItem from './Item';
import * as API from './api';

import './i18n';
import styles from './styles.module.scss';
import PageContent from '@lms/components/layouts/PageContent';

class Course extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showCreateForm: false
    };
  }

  deleteLesson = ({ id }) => {
    this.props.deleteLesson({ id });
  };

  handleClose = () => { this.setState( { showCreateForm: false }) };

  handleChange = (params) => {
    this.props.updateCourse({ input: {
        organizationId: this.props.organizationId,
        id: this.props.match.params.id,
        ...params,
      }
    }, this.props.data.getCourse);
  };

  handleMove = (id, from, to) => {
    const { client, match } = this.props;
    const store = client.store.cache;
    const query = gql.getCourse;
    const variables = { id: match.params.id };
    const data = store.readQuery({ query, variables });

    const lessons = data.getCourse.lessons.items;
    const lesson = lessons.splice(from, 1)[0];
    lessons.splice(to, 0, lesson);
    store.writeQuery({query, data});
    this.forceUpdate();
  };

  handleDrop = (id, from, to) => {
    this.props.moveLesson({
      id,
      position: to
    });
  };

  render() {
    const { data, t } = this.props;

    if (data.loading) return <Loader />;
    if (data.error) return <Error {...data.error} />;

    const { title, description, id, lessons } = data.getCourse;

     return (
       <PageContent>
         <CreateLessonForm
           courseId={id}
           show={this.state.showCreateForm}
           onClose={this.handleClose}
         />
         <Row className="mb-m2">
           <Col>
             <Title level={2}>
               <Text.Editable onChange={value => this.handleChange({ title: value })}>{title}</Text.Editable>
             </Title>
           </Col>
         </Row>
         <Row className="mb-xl">
           <Col className={styles.description}>
             <Text.Collapsable
               enable={description && description.length > 700}
               collapsed={_.truncate(description, {length: 700})}
             >
               <Text.Editable onChange={value => this.handleChange({ description: value })}>
                 {description}
               </Text.Editable>
             </Text.Collapsable>
           </Col>
         </Row>
         <DndProvider backend={HTML5Backend}>
           <CardColumns className="row m-ns2">
             {_.map(lessons.items, (lesson, index )=> (
               <LessonItem
                 key={lesson.id}
                 t={t}
                 lesson={lesson}
                 index={index}
                 onMove={this.handleMove}
                 onDrop={this.handleDrop}
                 onDelete={this.deleteLesson}
               />
             ))}
             <div className='col-md-4 col-lg-3 col-sm-6 col-xs-12 p-s2'>
               <Card
                 className={cn(styles.card, styles.new, 'm-0')}
                 onClick={() => this.setState({ showCreateForm: true })}
               >
                 <Card.Body
                   className={cn(styles.cardBody, 'flex-column d-flex justify-content-center align-items-center')}
                 >
                   <i className={cn(styles.icon, 'fas fa-plus-circle')} />
                 </Card.Body>
               </Card>
             </div>
           </CardColumns>
         </DndProvider>
       </PageContent>
     );
  }
}

export default compose(
  API.getCourse,
  API.updateCourse,
  API.deleteLesson,
  API.moveLesson,
)(
  withTranslation('admin/pages/Course', { useRef: true })(
    withApollo(Course)
  ),
);
