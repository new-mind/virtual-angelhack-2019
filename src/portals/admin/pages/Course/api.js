import { graphql } from 'react-apollo';
import gql from 'graphql-tag';
import * as _ from 'lodash';

import * as g from '@lms/graphql';
import { optimistic } from '@lms/graphql';

const MOVE_LESSON = gql`
  mutation MoveLesson($courseId: ID!, $lessonId: ID!, $position: Int!) {
    Course(id: $courseId) {
      moveLesson(id: $lessonId, position: $position) {
        position
        lesson {
          id
        }
      }
    }
  }
`
export const getCourse = graphql(g.getCourse, {
  options: props => ({
    variables: {
      id: props.match.params.id,
    },
  }),
  props: props => {
    return {
      data: props.data
    }
  }
});

export const updateCourse = graphql(g.updateCourse, {
  props: props => ({
    updateCourse: (variables, course) => {
      return props.mutate({
        variables: variables,
        optimisticResponse: {
          __typename: 'Mutation',
          updateCourse: {
            ...optimistic.Course,
            ...course,
            ...variables.input
          }
        }
      });
    }
  })
});

export const createLesson = graphql(g.createLesson, {
  options: props => ({
    update: (store, { data: {createLesson} }) => {
      const query = g.getCourse;
      const variables = { id: props.courseId };
      const data = store.readQuery({ query, variables });

      data.getCourse.lessons.items = [
        ...data.getCourse.lessons.items.filter(item => item.id !== createLesson.id),
        createLesson
      ];
      store.writeQuery({query, variables, data});
    }
  }),
  props: props => ({
    createLesson: variables => {
      props.mutate({
        variables: variables,
        optimisticResponse: {
          __typename: "Mutation",
          createLesson: {
            ...optimistic.Lesson,
            ...variables.input,
          }
        }
      })
    }
  })
});

export const deleteLesson = graphql(g.deleteLesson, {
  options: props => ({
    update: (store, {data: {deleteLesson}}) => {
      const query = g.getCourse;
      const variables = { id: props.match.params.id };
      const data = store.readQuery({ query, variables });

      data.getCourse.lessons.items = [
        ...data.getCourse.lessons.items.filter(item => item.id !== deleteLesson.id)
      ];
      store.writeQuery({query, data});
    },
  }),
  props: props => ({
    deleteLesson: ({id}) => {
      return props.mutate({
        variables: {id},
        optimisticResponse: {
          __typename: "Mutation",
          deleteLesson: {
            ...optimistic.Lesson,
            id
          }
        }
      });
    }
  })
});

export const moveLesson = graphql(MOVE_LESSON, {
  options: props => ({
    update: (store, {data: {Course: {moveLesson}}}) => {
      const query = g.getCourse;
      const variables = { id: props.match.params.id };
      const data = store.readQuery({ query, variables });

      const { position, lesson } = moveLesson;
      const { items } = data.getCourse.lessons;

      const index = _.findIndex(items, (item) => item.id === lesson.id);
      const currentLesson = items.splice(index, 1)[0];
      items.splice(position, 0, currentLesson);
      store.writeQuery({query, data});
    }
  }),
  props: ({mutate, ownProps}) => ({
    moveLesson: ({id, position}) => {
      return mutate({
        variables: {
          courseId: ownProps.match.params.id,
          lessonId: id,
          position
        },
        optimisticResponse: {
          __typename: "Mutation",
          Course: {
            __typename: "Course",
            moveLesson: {
              __typename: "LessonWithPos",
              position,
              lesson: {
                ...optimistic.Lesson,
                id
              }
            }
          }
        }
      })
    }
  })
});
