import React from 'react';
import { Form } from 'react-bootstrap';
import { Formik } from 'formik';
import * as yup from 'yup';
import { v4 as uuid } from 'uuid';
import { withTranslation } from 'react-i18next';

import '../i18n';
import * as API from '../api';
import Button from '@lms/components/blocks/Button';
import Modal from '@lms/components/blocks/Modal';

const schema = yup.object({
  title: yup.string().min(5).required()
})

class CreateLesson extends React.Component {
  createLesson = (data) => {
    const isHomework = this.props.mode === 'homework';
    this.props.createLesson({
      input: {
        id: uuid(),
        title: data.title,
        description: data.description || null,
        courseId: this.props.courseId,
        homework: isHomework
      }
    });
    this.props.onClose();
  };


  render() {
    const { t } = this.props;
    return (
      <Modal show={this.props.show} size={'lg'} onHide={this.props.onClose}>
        <Formik
          validationSchema={schema}
          onSubmit={this.createLesson}>
          {({
            errors,
            touched,
            handleChange,
            handleSubmit
          }) => (
            <Form noValidate onSubmit={handleSubmit}>
              <Modal.Header closeButton>
                <Modal.Title className="mt-2">{t("createForm.title")}</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <Form.Group>
                  <Form.Control
                    type="text"
                    className="mb-4"
                    style={{ border: '1px solid #231F20', opacity: '0.5', borderRadius: '2px' }}
                    placeholder={t("createForm.titlePlaceholder")}
                    isInvalid={touched.title && errors.title}
                    isValid={touched.title && !errors.title}
                    name="title"
                    onChange={handleChange}
                  />
                  <Form.Control.Feedback type="invalid">
                    {errors.title}
                  </Form.Control.Feedback>
                </Form.Group>
                <Form.Group>
                  <Form.Control
                    as="textarea" rows="6"
                    style={{ border: '1px solid #231F20', opacity: '0.5', borderRadius: '2px' }}
                    placeholder={t("createForm.descriptionPlaceholder")}
                    name="description"
                    onChange={handleChange}
                  />
                </Form.Group>
              </Modal.Body>
              <Modal.Footer>
                <Button style={{width: '100%'}} type="submit">
                  {t("createForm.submitButton")}
                </Button>
              </Modal.Footer>
            </Form>
          )}
        </Formik>
      </Modal>
    )
  }
}

export default API.createLesson(
  withTranslation('admin/pages/Course')(CreateLesson)
);
