import React, { useRef } from 'react';
import { NavLink } from 'react-router-dom';
import { useDrag, useDrop } from 'react-dnd';
import cn from 'classnames';

import LessonCard from '@lms/components/LessonCard';
import Button from '@lms/components/blocks/Button';

import styles from './styles.module.scss';

const Item = ({lesson, onDelete, index, onMove, onDrop, t}) => {
  const ref = useRef(null);
  const [, drop] = useDrop({
    accept: 'LessonCard',
    hover(item, monitor) {
      if (!ref.current)
        return;
      if (item.index === index)
        return;

      onMove(item.id, item.index, index);
      item.index = index;
    },
    drop(item, monitor) {
      if (!ref.current)
        return;

      onDrop(item.id, item.index, index);
    }
  });

  const [{ isDragging }, drag] = useDrag({
    item: { type: 'LessonCard', index, id: lesson.id  },
    collect: monitor => ({
      isDragging: !!monitor.isDragging()
    })
  });

  const opacity = isDragging ? 0 : 1;
  drag(drop(ref));

  return (
    <div ref={ref}
        className='col-md-4 col-lg-3 col-sm-6 col-xs-12 p-s2'
        style={{opacity}}
        key={lesson.id}>
      <LessonCard
        title={lesson.title}
        isHomework={lesson.homework}
        isDraft={lesson.dirty || lesson.rev === -1}
        actions={(
          <>
            <Button className={styles.button} variant="dark">
              <NavLink className="text-light" to={`/lessons/${lesson.id}`}>
                {t('Edit')}
              </NavLink>
            </Button>
            <Button.Confirm
              className={cn(styles.button, 'transparent')}
              confirm={t('Confirm')}
              onClick={() => onDelete(lesson)}
            >
              {t('Delete')}
            </Button.Confirm>
          </>
        )}
      />
    </div>
  );
};

export default Item;
