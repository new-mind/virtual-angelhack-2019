import React, { Component } from 'react';
import { Button, Col, Container, Image, Row, Card } from 'react-bootstrap';
import { NavLink, Route, BrowserRouter as Router } from 'react-router-dom';

import BasicImg from '@assets/beards/3.png'
import AdvancedImg from '@assets/beards/2.png'
import EnterpriseImg from '@assets/beards/1.png'

export default class Plans extends Component {
  render() {
    const { match } = this.props;

    return (
      <Container className="mt-3" fluid>
        <Router>
          <Row>
            <Col md={4}>
              <Card style={{width: '30em'}} className="mt-3 mx-auto">
                <Card.Body>
                  <Card.Title>Basic</Card.Title>
                  <Card.Img src={BasicImg}/>
                  <Button variant="link" className="mt-5">Downgrade</Button>
                </Card.Body>
              </Card>
            </Col>
            <Col md={4}>
              <Card style={{width: '30em'}} className="mt-3 mx-auto">
                <Card.Body>
                  <Card.Title>Advanced</Card.Title>
                  <Card.Img src={AdvancedImg}/>
                  <Button variant="link" className="mt-5">Your plan</Button>
                </Card.Body>
              </Card>
            </Col>
            <Col md={4}>
              <Card style={{width: '30em'}} className="mt-3 mx-auto">
                <Card.Body>
                  <Card.Title>Enterprise</Card.Title>
                  <Card.Img src={EnterpriseImg}/>
                  <Button variant="warning" className="mt-5">Upgrade</Button>
                </Card.Body>
              </Card>
            </Col>
          </Row>
        </Router>
      </Container>
    );
  }
}
