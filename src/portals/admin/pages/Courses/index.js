import React, { Component } from 'react';
import { Col, Row } from 'react-bootstrap';
import { compose } from 'react-apollo';
import { withTranslation } from 'react-i18next';

import * as API from './api';
import Button from '@lms/components/blocks/Button';
import Loader from '@lms/components/loader/Loader';
import Title from '@lms/components/blocks/PageTitle';
import Error from '@lms/components/Error';
import CreateCourseForm from './forms/CreateCourse';

import './i18n';
import PageContent from '@lms/components/layouts/PageContent';
import CourseCard from '@lms/components/CourseCard';
import Courses from '@lms/components/Courses';

class CoursesPage extends Component {
  state = {
    showCreateForm: false
  };

  deleteCourse = (id) => {
    const { deleteCourse } = this.props;

    deleteCourse({id});
  };


  render() {
    const { data, history, t } = this.props;

    if (data.loading) return <Loader />;
    if (data.error) return <Error {...data.error} />;

    let courses = data.listCourses.items;

    return (
      <PageContent>
        <CreateCourseForm
          organizationId={this.props.organizationId}
          show={this.state.showCreateForm}
          onClose={() => this.setState({showCreateForm: false}) }
        />
        <Row className="mb-xl">
          <Col className="d-flex align-items-center">
            <Title>{t("My Courses")}</Title>
            <Button
              className="ml-auto"
              onClick={() => this.setState({showCreateForm: true})}>
              {t("ADD COURSE")}
            </Button>
          </Col>
        </Row>
        <Courses>
          {courses.map(course => (
            <CourseCard
              key={course.id}
              {...course}
              actions={(
                <>
                  <Button onClick={() => history.push(`/courses/${course.id}`)} className="mr-3">
                    {t("Edit")}
                  </Button>
                  <Button.Confirm
                    className="transparent"
                    confirm={t("Confirm")}
                    onClick={() => this.deleteCourse(course.id)}>
                    {t("Delete")}
                  </Button.Confirm>
                </>
              )}
            />
          ))}
        </Courses>
      </PageContent>
    );
  }
}

export default compose(
  API.listCourses,
  API.deleteCourse,
  withTranslation('admin/pages/Courses')
)(CoursesPage);
