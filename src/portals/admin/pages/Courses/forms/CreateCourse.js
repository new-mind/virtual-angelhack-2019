import  React from 'react';
import { Form } from 'react-bootstrap';
import * as yup from 'yup';
import { Formik } from 'formik';
import { compose } from 'react-apollo';
import { Storage } from 'aws-amplify';
import { v4 as uuid } from 'uuid';
import { withTranslation } from 'react-i18next';

import * as API from '../api';
import ImageUploader from '@lms/components/blocks/ImageUploader';
import Modal from '@lms/components/blocks/Modal';
import Button from '@lms/components/blocks/Button';

const schema = yup.object({
  title: yup.string().min(5).required()
})

class CreateCourse extends React.Component {
  state = {
    image: null,
    imagePreview: null
  }

  create = async (data) => {
    this.props.onClose();

    const {data: {createCourse}} = await this.props.createCourse({
      input: {
        id: uuid(),
        organizationId: this.props.organizationId,
        title: data.title,
        description: data.description || null
      }
    }, this.state.imagePreview)

    if (!this.state.image) return;

    const file = this.state.image;
    let match = /(\.[\w\d]*?)$/.exec(file.name);
    let ext = '';
    if (match) {
      ext = match[1];
    }
    const {key}= await Storage.put(`${createCourse.id}-preview${ext}`, file, {
      contentType: file.type,
      ACL: 'public-read',
      level: 'public'
    })

    this.props.setImageUrl({
      input: {
        id: createCourse.id,
        title: createCourse.title,
        description: createCourse.description,
        imageUrl: ImageUploader.getUrl(key)
      }
    });
  };

  onImagePick = (file, base64) => {
    this.setState({
      image: file,
      imagePreview: base64
    })
  };

  render() {
    const { t } = this.props;
    return (
      <Modal show={this.props.show} size={'lg'} onHide={this.props.onClose}>
        <Formik
          validationSchema={schema}
          onSubmit={this.create}
        >
        {({
          errors,
          touched,
          handleChange,
          handleSubmit
        }) => (
          <Form onSubmit={handleSubmit}>
            <Modal.Header closeButton>
              <Modal.Title>{t("addForm.title")}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Form.Group>
                <Form.Control
                  name="title"
                  type="text"
                  style={{ border: '1px solid #231F20', opacity: '0.5', borderRadius: '2px' }}
                  placeholder={t("addForm.titlePlaceholder")}
                  isInvalid={touched.title && errors.title}
                  isValid={touched.title && !errors.title}
                  onChange={handleChange}
                />
                <Form.Control.Feedback type="invalid">
                  {errors.title}
                </Form.Control.Feedback>
              </Form.Group>
              <Form.Group>
                <Form.Control
                  name="description"
                  as="textarea"
                  style={{ border: '1px solid #231F20', opacity: '0.5', borderRadius: '2px' }}
                  placeholder={t("addForm.descriptionPlaceholder")}
                  rows="5"
                  isInvalid={touched.description && errors.description}
                  isValid={touched.description && !errors.description}
                  onChange={handleChange}
                />
              </Form.Group>
              {/* https://redmine.eduway.today/issues/142
              <Form.Group>
                <ImageUploader
                  imageUrl={this.props.imageUrl}
                  onPick={this.onImagePick}
                />
              </Form.Group>
              */}
            </Modal.Body>
            <Modal.Footer>
              <Button style={{ width: '100%' }} variant="dark" type="submit">
                {t("addForm.submitButton")}
              </Button>
            </Modal.Footer>
          </Form>
        )}
        </Formik>
      </Modal>
    );
  }
}

export default compose(
  API.createCourse,
  API.updateCourse
)(withTranslation('admin/pages/Courses')(CreateCourse));
