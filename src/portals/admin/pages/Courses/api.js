import { graphql } from 'react-apollo';

import * as gql from '@lms/graphql';
import { optimistic } from '@lms/graphql';

const LIMIT = null;
export const listCourses = graphql(gql.listCourses, {
  options: props => ({
    variables: {
      limit: LIMIT,
      filter: {
        organizationId: props.organizationId
      }
    }
  })
});

export const createCourse = graphql(gql.createCourse, {
  options: props => ({
    update: (store, {data: {createCourse}}) => {
      const query = gql.listCourses;
      const filter = { organizationId: props.organizationId };
      const variables = { limit: LIMIT, filter };
      const data = store.readQuery({ query, variables });

      //TODO: hardcode for handling base64 optimistic image
      const optimistic = store.optimistic && store.optimistic[0];
      if (optimistic) {
        try {
          createCourse.imageUrl = optimistic.data["Course:-1"].imageUrl.json[0]; //TODO: hardcode
        } catch(e) {}
      }

      data.listCourses.items = [
        createCourse,
        ...data.listCourses.items.filter(item => item.id !== createCourse.id)
      ]
      store.writeQuery({query, variables, data});
    }
  }),
  props: props => ({
    createCourse: (variables, imageUrl = null) => {
      return props.mutate({
        variables,
        optimisticResponse: {
          __typename: 'Mutation',
          createCourse: {
            ...optimistic.Course,
            ...variables.input,
            imageUrl,
          }
        }
      })
    }
  })
});

export const deleteCourse = graphql(gql.deleteCourse, {
  options: (props) => ({
    update: (store, {data: {deleteCourse}}) => {
      const query = gql.listCourses;
      const filter = { organizationId: props.organizationId };
      const variables = { limit: LIMIT, filter };
      const data = store.readQuery({ query, variables });

      data.listCourses.items = [
        ...data.listCourses.items.filter(item => item.id !== deleteCourse.id)
      ];
      store.writeQuery({query, variables, data});
    }
  }),
  props: props => ({
    deleteCourse: variables => {
      return props.mutate({
        variables,
        optimisticResponse: {
          __typename: 'Mutation',
          deleteCourse: {
            ...optimistic.Course,
            ...variables
          }
        }
      });
    }
  })
});

export const updateCourse = graphql(gql.updateCourse, {
  props: props => ({
    setImageUrl: (variables) => {
      return props.mutate({
        variables: variables,
        optimisticResponse: {
          __typename: 'Mutation',
          updateCourse: {
            ...optimistic.Course,
            ...variables.input
          }
        }
      })
    }
  })
});
