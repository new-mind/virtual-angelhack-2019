import React from 'react';
import { Container } from 'react-bootstrap';
import { compose } from 'react-apollo'
import { withTranslation } from 'react-i18next';

import Button from '@lms/components/blocks/Button';
import Loader from '@lms/components/loader/Loader';
import Error from '@lms/components/Error';
import * as API from '../Lesson/api';
import Lesson from '@lms/components/Lesson';
import './i18n';

class LessonPreview extends React.Component {
  render() {
    const { data, history, t } = this.props;

    if (data.loading) return <Loader />;
    if (data.error) return <Error {...data.error} />;

    const { getLesson } = data;

    /* @todo remove */
    const event = new CustomEvent('courseUploaded', { 'detail': getLesson.course });
    window.dispatchEvent(event);
    /* @todo remove */

    return (
      <Container fluid>
        <Button
          className="my-5 mx-auto d-block"
          onClick={() => history.push(`/lessons/${getLesson.id}`)}>
          {t("Constructor")}
        </Button>
        <Lesson
          mode="PREVIEW_WITHOUT_DATA"
          sections={getLesson.content}/>
      </Container>
    )
  }
}

export default compose(
  API.getLesson
)(withTranslation('admin/pages/LessonPreview')(LessonPreview));
