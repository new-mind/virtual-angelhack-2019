import React, { Component } from 'react';
import { Col, Row } from 'react-bootstrap';
import { withTranslation } from 'react-i18next';

import './i18n';
import Teachers from '../../subpages/Teachers'
import Students from '../../subpages/Students'
import List from '@lms/components/blocks/List';
import { Link } from '@lms/components/blocks/Link';
import PageContent from '@lms/components/layouts/PageContent';

class Users extends Component {
  render() {
    const { match, t } = this.props;

    return (
      <PageContent>
        <Row className="h-100">
          <Col
            md={{ span: 9, order: 1 }}
            sm={{ order: 12 }}
            xs={{ order: 12 }}>
            { match.params.type === 'teachers' ?
              <Teachers organizationId={this.props.organizationId}/>
              :
              <Students organizationId={this.props.organizationId}/>
            }
          </Col>
          <Col
            md={{ span: 3, order: 12 }}
            sm={{ span: 12, order: 1 }}
            xs={{ span: 12, order: 1 }}>
            <List>
              <List.Item>
                <Link to="/users/students">
                  {t("Students")}
                </Link>
              </List.Item>
              <List.Item>
                <Link to="/users/teachers">
                  {t("Teachers")}
                </Link>
              </List.Item>
            </List>
          </Col>
        </Row>
      </PageContent>
    );
  }
}

export default withTranslation('admin/pages/Users')(Users);
