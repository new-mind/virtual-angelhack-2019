import React from 'react';

class Student extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <div>
        Student {this.props.match.params.username} page
      </div>
    );
  }
}

Student.propTypes = {};

export default Student;
