import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { withTranslation } from 'react-i18next';

import './i18n';
import Dashboard from '@lms/components/Dashboard';
import ProfilePage from '@lms/components/Profile';

class AdminDashboard extends React.PureComponent {
  renderContent() {
    const { user } = this.props;
    return (
      <Switch>
        <Route exact path="/profile" render={() => (<ProfilePage {...this.props} user={user}/>)}/>
        <Redirect to="/profile" />
      </Switch>
    );
  }

  render() {
    const { t } = this.props;
    const menu = [
      { url: "/profile", title: t("Profile") }
    ];

    return (
      <Dashboard
        user={this.props.user}
        content={this.renderContent()}
        menu={menu}/>
    );
  }
}

export default withTranslation('admin/pages/Dashboard')(AdminDashboard);
