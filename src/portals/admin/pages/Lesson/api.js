import * as _ from 'lodash';
import { graphql } from 'react-apollo';
import _gql_ from 'graphql-tag';
import * as gql from '@lms/graphql';

import { optimistic, fragments } from '@lms/graphql';

const MOVE_SECTION = _gql_`
  mutation MoveSection($lessonId: ID! $id: ID!, $position: Int!) {
    Lesson(id: $lessonId) {
      moveSection(id: $id, position: $position) {
        section {
          ...TextWidget
          ...VideoWidget
          ...LinkWidget
          ...MultiChoiceWidget
          ...AttachmentWidget
          ...ImageWidget
          ...ResponseFormWidget
        }
        position
      }
    }
  }
  ${fragments.TextWidget}
  ${fragments.VideoWidget}
  ${fragments.MultiChoiceWidget}
  ${fragments.LinkWidget}
  ${fragments.AttachmentWidget}
  ${fragments.ImageWidget}
  ${fragments.ResponseFormWidget}
`;

export const getLesson = graphql(gql.getLesson, {
  options: props => {
    return {
      variables: {
        id: props.match.params.id
      }
    };
  }
});

export const updateLesson = graphql(gql.updateLesson, {
  props: ({mutate, ownProps}) => ({
    updateLesson: (variables, lesson) => {
      return mutate({
        variables: {
          input: {
            organizationId: ownProps.organizationId,
            ...variables.input,
          }
        },
        optimisticResponse: {
          __typename: 'Mutation',
          updateLesson: {
            ...optimistic.Lesson,
            ...lesson,
            ...variables.input
          }
        }
      })
    }
  })
});

export const publishLesson = graphql(gql.publishLesson, {
  props: ({mutate, ownProps}) => ({
    publishLesson: (lessonId, lesson) => {
      return mutate({
        variables: {
          id: lessonId
        },
        optimisticResponse: {
          __typename: 'Mutation',
          publishLesson: {
            ...optimistic.Lesson,
            ...lesson,
            dirty: false,
            rev: lesson.rev + 1
          }
        }
      });
    }
  })
});

export const addSection = graphql(gql.addSection, {
  options: props => ({
    update: (store, {data: {Lesson}}) => {
      const query = gql.getLesson;
      const variables = {
        id: props.match.params.id
      };
      const { position, section } = Lesson.addSection;
      const data = store.readQuery({ query, variables });
      data.getLesson.content = [
        ..._.slice(data.getLesson.content, 0, position),
        section,
        ..._.slice(data.getLesson.content, position)
      ]
      data.getLesson.dirty = true;

      store.writeQuery({ query, variables, data });
    }
  }),
  props: ({mutate, ownProps}) => ({
    addSection: (variables) => {
      const id = ownProps.match.params.id;
      const section = variables.input.section;

      return mutate({
        variables: {
          id,
          input: variables.input
        },
        //TODO
        optimisticResponse: {
          __typename: "Mutation",
          Lesson: {
            ...optimistic.Lesson,
            id,
            addSection: {
              __typename: "SectionWithPos",
              position: variables.input.position,
              section: optimistic.Section(section)
            }
          }
        }
      });
    }
  })
});

export const deleteSection = graphql(gql.deleteSection, {
  options: props => ({
    update: (store, {data: {Lesson}}) => {
      const query = gql.getLesson;
      const variables = {
        id: props.match.params.id
      };
      const { section } = Lesson.deleteSection;

      const data = store.readQuery({ query, variables });
      data.getLesson.content = _.filter(
        data.getLesson.content, ({id}) => id !== section.id);
      data.getLesson.dirty = true;
      store.writeQuery({ query, variables, data });
    }
  }),
  props: ({mutate, ownProps}) => ({
    deleteSection: (section) => {
      const id = ownProps.match.params.id;

      return mutate({
        variables: {
          id,
          sectionId: section.id
        },
        optimisticResponse: {
          __typename: "Mutation",
          Lesson: {
            ...optimistic.Lesson,
            id,
            deleteSection: {
              __typename: "SectionWithPos",
              position: -1,
              section
            }
          }
        }
      })
    }
  })
});

export const updateSection = graphql(gql.updateSection, {
  props: ({mutate, ownProps}) => ({
    updateSection: (variables, section) => {
      const id = ownProps.match.params.id;
      if (!section) {
        throw new Error('Section is not provided');
      }

      return mutate({
        variables: {
          input: {
            organizationId: ownProps.organizationId,
            lessonId: id,
            ...variables.input
          }
        },
        optimisticResponse: {
          __typename: "Mutation",
          updateSection: optimistic.Section({
            ...section,
            ...variables.input
          })
        }
      });
    }
  })
});

export const moveSection = graphql(MOVE_SECTION, {
  options: props => ({
    update: (store, {data: {Lesson}, isOptimistic}) => {
      if (isOptimistic) return;
      const query = gql.getLesson;
      const variables = {
        id: props.match.params.id
      };
      const { section, position } = Lesson.moveSection;

      const data = store.readQuery({ query, variables });
      data.getLesson.content = _.filter(
        data.getLesson.content, ({id}) => id !== section.id);
      data.getLesson.content.splice(position, 0, section);
      data.getLesson.dirty = true;
      store.writeQuery({ query, variables, data });
    }
  }),
  props: ({mutate, ownProps}) => ({
    moveSection: (section, lessonId, position) => {
      return mutate({
        variables: {
          id: section.id,
          lessonId,
          position,
        },
        optimisticResponse: {
          __typename: "Mutation",
          isOptimistic: true,
          Lesson: {
            ...optimistic.Lesson,
            id: lessonId,
            moveSection: {
              __typename: "SectionWithPos",
              section,
              position
            }
          }
        }
      });
    }
  })
})
