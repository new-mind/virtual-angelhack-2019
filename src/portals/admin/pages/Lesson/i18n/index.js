import i18n from '@lms/i18n';
import { requireAll, basename } from '@lms/services/utils';

const ctx = require.context('.', true, /\.json$/);
requireAll(ctx, (requireCtx, path) => {
  i18n.addResourceBundle(basename(path), 'admin/pages/Lesson', requireCtx(path));
});
