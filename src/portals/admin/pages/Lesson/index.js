import React, { Component } from 'react';
import cn from 'classnames';
import { Row, Col } from 'react-bootstrap';
import { compose } from 'react-apollo'
import * as _ from 'lodash';
import { withTranslation } from 'react-i18next';
import { NavLink } from 'react-router-dom';

import Loader from '@lms/components/loader/Loader';
import Error from '@lms/components/Error';
import * as API from './api';

import LessonConstructor from '../../components/Lesson';
import Title from '@lms/components/blocks/PageTitle';
import Text from '@lms/components/blocks/Text';
import TextEditable from '@lms/components/blocks/TextEditable';
import Checkbox from '@lms/components/blocks/Checkbox';
import Button from '@lms/components/blocks/Button';
import PageContent from '@lms/components/layouts/PageContent';
import Tutorial from '@lms/components/Tutorial';

import './i18n';
import styles from './Lesson.module.scss';
import LessonNotFound from '@lms/components/LessonNotFound';

class Lesson extends Component {
  constructor(props) {
    super(props);

    this.state = {
      widget: null
    };
  }

  handleAddSection = (position, section) => {
    this.props.addSection({
      input: {
        position,
        section
      }
    });
  };

  handleDeleteSection = (section) => {
    this.props.deleteSection(section);
  };

  handlePublish = () => {
    const { data: { getLesson } } = this.props;
    this.props.publishLesson(getLesson.id, getLesson);
  };

  handleChangeSection = (data, section) => {
    this.props.updateSection({ input: data }, section);
  };

  handleChangeSectionOrder = (fromIndex, toIndex) => {
    /* TODO
    if (fromIndex === toIndex) return; // before widget
    if (fromIndex === toIndex - 1) return; // after widget

    const { getLesson } = this.props.data;
    let contentOrder = getLesson.contentOrder || [];
    if (fromIndex >= contentOrder.length || fromIndex < 0) return;
    if (toIndex > contentOrder.length || toIndex < 0) return;

    if (contentOrder.length === toIndex) toIndex--;
    contentOrder = arrayMove(contentOrder, fromIndex, toIndex);

    // presaving content order
    const { store } = this.props.client;
    const data = store.cache.readQuery(API.getLesson.store);
    data.getLesson.contentOrder = contentOrder;
    store.cache.writeQuery({
      ...API.getLesson.store,
      data
    });

    this.props.updateContentOrder({
      input: {
        id: getLesson.id,
        contentOrder: contentOrder
      }
    });
    */
  };

  handleMoveSection = (section, position) => {
    const { getLesson } = this.props.data;
    if (position < 0 || position >= getLesson.content) return;
    this.props.moveSection(section, getLesson.id, position);
  };

  handleChange = (params) => {
    this.props.updateLesson({
      input: {
        id: this.props.match.params.id,
        ...params,
      }
    }, this.props.data.getLesson);
  };

  makeHomeWork = () => {
    const { getLesson } = this.props.data;
    if (getLesson.homework) return;

    this.props.updateLesson({
      input: {
        id: getLesson.id,
        homework: true
      }
    }, getLesson);
  };

  makeClassRoom = () => {
    const { getLesson } = this.props.data;
    if (!getLesson.homework) return;

    this.props.updateLesson({
      input: {
        id: getLesson.id,
        homework: false
      }
    }, getLesson);
  };

  render() {
    const { data, history, t } = this.props;

    if (data.loading) return <Loader />;
    if (data.error) return <Error {...data.error} />;

    const { getLesson } = data;

    if (_.isNull(getLesson)) return (
      <PageContent>
        <LessonNotFound />
      </PageContent>
    );

    const { title, description, homework } = getLesson;

    return (
      <PageContent>
        <Tutorial type="adminLesson"/>
        <Row className="mb-xl">
          <Col className="d-flex">
            <NavLink className={styles.back} to={`/courses/${getLesson.course.id}`}>Back</NavLink>
          </Col>
        </Row>
        <Row noGutters className="mb-m2">
          <Col className="d-flex align-items-center">
            <Title level={2} className="mr-l2">
              <TextEditable onChange={value => this.handleChange({ title: value })}>
                {title}
              </TextEditable>
            </Title>
            <Checkbox
              className={cn(styles.homework, 'ml-auto')}
              checked={Boolean(homework)}
              onChange={() => homework ? this.makeClassRoom() : this.makeHomeWork()}
              label={t("Homework")}
            />
          </Col>
        </Row>
        <Row className="mb-xl">
          <Col>
            <Text.Collapsable enable={description && description.length > 700} collapsed={_.truncate(description, {length: 700})}>
              <TextEditable onChange={value => this.handleChange({ description: value })}>{description}</TextEditable>
            </Text.Collapsable>
          </Col>
        </Row>
        {getLesson.dirty && (
          <Row className="mb-m2">
            <Col className="d-flex justify-content-end">
              <Button className={styles.publish} transparent onClick={this.handlePublish}>
                {this.props.t("Publish Changes")}
              </Button>
            </Col>
          </Row>
        )}
        <LessonConstructor
          history={history}
          sections={getLesson.content}
          isDirty={getLesson.dirty}
          lessonId={getLesson.id}
          onAdd={this.handleAddSection}
          onPublish={this.handlePublish}
          onDelete={this.handleDeleteSection}
          onChange={this.handleChangeSection}
          onMoveSection={this.handleMoveSection}
        />
      </PageContent>
    )
  }
}

export default compose(
  API.getLesson,
  API.updateLesson,
  API.addSection,
  API.moveSection,
  API.deleteSection,
  API.updateSection,
  API.publishLesson
)(withTranslation('admin/pages/Lesson')(Lesson));
