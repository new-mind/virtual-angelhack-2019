import React from 'react';
import cn from 'classnames';

import styles from './styles.module.scss';

class ControlButtons extends React.PureComponent {
  render() {
    const { className } = this.props;
    return (
      <div className={cn(styles.controlButtons, className)}>
        { this.props.collapsed ?
          <span onClick={this.props.onClickExpand}><i className="fas fa-expand"></i></span>
          :
          <span onClick={this.props.onClickCollapse}><i className="fas fa-minus"></i></span>
        }
        { this.props.showDown && <span onClick={this.props.onClickDown}><i className="fas fa-chevron-down"></i></span> }
        { this.props.showUp && <span onClick={this.props.onClickUp}><i className="fas fa-chevron-up"></i></span> }
      </div>
    );
  }
}

export default ControlButtons;
