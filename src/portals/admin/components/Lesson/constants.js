export const WIDGETS = [{
  name: 'text',
  title: 'Text',
}, {
  name: 'image',
  title: 'Image',
}, {
  name: 'link',
  title: 'Link',
}, {
  name: 'video',
  title: 'Video',
}, {
  name: 'multiChoice',
  title: 'Multiple Choice',
},/* {
  name: 'singleChoice',
  title: 'Single Choice',
},*/
{
  name: 'attachment',
  title: 'Attachment'
},/* {
  name: 'h5p',
  title: 'H5P.org',
},*/ {
  name: 'responseForm',
  title: 'Response Form'
}, {
  name: 'audio',
  title: 'Audio',
  disabled: true,
}, {
  name: 'fillBlank',
  title: 'Fill the Blank',
  disabled: true,
}, {
  name: 'flashCards',
  title: 'Flash Cards',
  disabled: true,
}];
