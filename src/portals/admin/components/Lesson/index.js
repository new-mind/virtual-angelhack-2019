import React from 'react';
import { Card, Col, Row } from 'react-bootstrap';
import { v4 as uuid } from 'uuid';
import cn from 'classnames';
import * as _ from 'lodash';
import { withTranslation } from 'react-i18next';
import { withCookies } from 'react-cookie';
import { compose } from 'react-apollo'

import Button from '@lms/components/blocks/Button';
import Error from '@lms/components/Error';
// @TODO: group up
import EditAttachmentWidget from '@lms/components/widgets/Attachment/Editor';
import EditLinkWidget from '@lms/components/widgets/Link/Editor';
import EditResponseFormWidget from '@lms/components/widgets/EditResponseForm';
import EditTextWidget from '@lms/components/widgets/EditText';
import EditVideoWidget from '@lms/components/widgets/Video/Editor';
import EditImageWidget from '@lms/components/widgets/EditImage';
import MultiChoice from '@lms/components/widgets/MultiChoice';
import ControlButtons from './ControlButtons';
import EditAudio from '@lms/components/widgets/EditAudio';

import Widget from '../widgets';
import { WIDGETS } from './constants';
import Slot from './Slot';

import './i18n';
import styles from './styles.module.scss';

class Lesson extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      clipboard: null,
    };

    this.current = 0;
    this.widgets = _.map(props.sections, () => {
      return React.createRef();
    });
  }

  handleDragStart = (name, e) => {
    e.dataTransfer.setData('text', '1');
    this.setState({ clipboard: { name }});
  };

  handleDragEnd = () => {
    this.setState({ clipboard: null });
  };

  handleDrop = (index) => {
    let section = {
      id: uuid(),
      ...this._getWidgetData(),
    };

    this.props.onAdd(index, section);
  };

  handleRemove = (section) => {
    this.props.onDelete(section);
  };

  handleChange = (data, section) => {
    this.props.onChange(data, section);
  };

  handleCollapseSection = (section) => {
    const { cookies } = this.props;
    cookies.set(section.id, 'collapsed');
    this.forceUpdate();
  };

  handleExpandSection = (section) => {
    const { cookies } = this.props;
    cookies.remove(section.id);
    this.forceUpdate();
  };

  render() {
    const { history, lessonId, sections, t, cookies } = this.props;
    const { clipboard } = this.state;

    return (
      <Row>
        <Col xs={12} sm={12} md={5} lg={4} xl={3} className="py-1">
          <div className={styles.widgets}>
            <h1 className={styles.widgets__title}>{t("Add widget")}</h1>
            <div>
              {WIDGETS.map(widget => (
                <Card
                  key={widget.name}
                  draggable
                  onDragStart={this.handleDragStart.bind(this, widget.name)}
                  onDragEnd={this.handleDragEnd.bind(this, widget.name)}
                  className={cn(styles.item, widget.disabled && styles.disabled, 'w-100')}
                >
                  <Card.Text style={{ textAlign: 'center' }}>
                    {t(`widget.${widget.name}`)}
                  </Card.Text>
                </Card>
              ))}
            </div>
            <Button
              className="mt-2 d-block w-100"
              style={{ width: '15.000em' }}
              onClick={() => history.push(`/lessons/${lessonId}/preview`)}>
              {t("Preview")}
            </Button>
          </div>
        </Col>
        <Col xs={12} sm={12} md={7} lg={8} xl={9}>
          {sections.map((section, i) => {
            const collapsed = cookies.get(section.id);
            return (
              <div key={section.id} ref={this.widgets[i]}>
                <Slot
                  event={clipboard ? 'hover': 'no'}
                  index={i}
                  onDrop={this.handleDrop}
                />
                <div className={cn("my-5", styles.widget)}>
                  <div className={cn("p-3", styles.section)}>
                    <ControlButtons
                      className={styles.controlButtons}
                      collapsed={!!collapsed}
                      showUp={ i !== 0 }
                      showDown={ i !== (sections.length - 1) }
                      onClickUp={ () => { this.props.onMoveSection(section, i - 1); } }
                      onClickDown={ () => { this.props.onMoveSection(section, i + 1); } }
                      onClickCollapse={ () => { this.handleCollapseSection(section); } }
                      onClickExpand={ () => { this.handleExpandSection(section); } }
                    />
                    <Widget widget={section} onChange={this.handleChange} lessonId={lessonId} collapsed={!!collapsed}/>
                    {!collapsed &&
                    <Button.Confirm
                      transparent
                      confirm={t("Confirm")}
                      onClick={() => this.handleRemove(section)}>{t("Remove")}</Button.Confirm>
                    }
                  </div>
                </div>
              </div>
            )
          })}
          <Slot event={clipboard ? 'hover': 'no'} index={sections.length} onDrop={this.handleDrop} />
        </Col>
      </Row>
    );
  }

  _getWidgetData() {
    const { t } = this.props;
    const { name } = this.state.clipboard;
    let decl;
    switch (name) {
      case 'attachment':
        decl = EditAttachmentWidget.data;
        break;
      case 'text':
        decl = EditTextWidget.data;
        break;
      case 'link':
        decl = EditLinkWidget.data;
        break;
      case 'multiChoice':
        decl = MultiChoice.Editor.data;
        break;
      case 'image':
        decl = EditImageWidget.data;
        break;
      case 'video':
        decl = EditVideoWidget.data;
        break;
      case 'responseForm':
        decl = EditResponseFormWidget.data;
        // @todo invent mechanism for setting locales for default initial value of title
        decl.title = t('widget.responseForm');
        break;
      case 'audio':
        decl = EditAudio.data;
        break;
      default:
        break;
    }

    if (!decl) {
      throw new Error(`admin/components/Lesson: There is no such widget as ${name}`);
    }

    return decl;
  }
}

export default compose(
  withTranslation('admin/components/Lesson'),
  withCookies
)(Lesson);
