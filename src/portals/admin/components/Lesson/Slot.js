import React from 'react'
import { Alert, Row } from 'react-bootstrap';
import { withTranslation } from 'react-i18next';

import styles from './styles.module.scss';

class Slot extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      color: null,
    };
  }

  handleDragEnter = (e) => {
    e.preventDefault();
  };

  handleDragOver = (e) => {
    e.preventDefault();
    this.setState({ color: 'success' });
  };

  handleDragLeave = () => {
    this.setState({ color: null });
  };

  handleDrop = (e) => {
    e.preventDefault();
    const { onDrop, index } = this.props;

    this.setState({ color: null });
    onDrop(index)
  };

  render() {
    const { event, t } = this.props;
    let variant = 'info';

    switch (event) {
      case 'hover':
        variant = 'danger';
        break;
      default:
        break;
    }

    return (
      <Row style={{ margin: '5px 0' }} noGutters="false">
        <Alert
          variant={this.state.color || variant}
          className={styles.alert}
          onDragOver={this.handleDragOver}
          onDragLeave={this.handleDragLeave}
          onDragEnter={this.handleDragEnter}
          onDrop={this.handleDrop}
        >
          {t("Drag and Drop Widget Here")}
        </Alert>
      </Row>
    );
  }
}
export default withTranslation('admin/components/Lesson')(Slot);
