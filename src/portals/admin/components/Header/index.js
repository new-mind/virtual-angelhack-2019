import React, { Component } from 'react';
import cn from 'classnames';
import { graphql, compose } from 'react-apollo';
import * as gql from '@lms/graphql';
import { withTranslation } from 'react-i18next';

import BaseHeader from '@lms/components/Header';
import UserMenu from '@lms/components/UserMenu';
import Avatar from '@lms/components/blocks/Avatar';
import './i18n';

import { PORTAL } from '@lms/constants';

import styles from './styles.module.scss'

class Header extends Component {
  get items() {
    const { t } = this.props;

    return [{
      url: '/courses',
      title: t('Courses'),
    }, {
      url: '/users',
      title: t('Users'),
    }];
  }

  handleRoleChange = (role) => {
    this.props.requestPortal({role}, this.props.history);
  };

  handleOrganizationChange = (organizationId) => {
    this.props.requestPortal({organizationId}, this.props.history);
  };

  render() {
    const { data, user, t } = this.props;

    if (data.loading) return null;
    if (data.error) return null;

    const { getRoles } = data;

    return (
      <BaseHeader className={styles.header} items={this.items}
        avatar={
          <UserMenu
              items={this.items}
              currentRole={PORTAL.ADMIN}
              roles={getRoles}
              user={user}
              onOrganizationChange={this.handleOrganizationChange}
              onRoleChange={this.handleRoleChange}>
            {t('Admin')} <Avatar className={cn(styles.avatar, 'p-0 ml-s3')} {...user} />
          </UserMenu>
        }
      />
    );
  }
}

export default compose(
  graphql(gql.getRoles, {
    options: props => ({
      variables: {
        id: props.user.id,
      }
    })
  }),
  withTranslation('header'),
)(Header);
