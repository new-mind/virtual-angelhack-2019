import React from 'react';
import { Alert } from 'react-bootstrap';
import * as _ from 'lodash';

import AttachmentWidget from '@lms/components/widgets/Attachment/Editor';
import VideoWidget from '@lms/components/widgets/Video/Editor';
import TextWidget from '@lms/components/widgets/EditText';
import LinkWidget from '@lms/components/widgets/Link/Editor';
import ImageWidget from '@lms/components/widgets/EditImage';
import ResponseFormWidget from '@lms/components/widgets/EditResponseForm';
import MultiChoice from '@lms/components/widgets/MultiChoice';

class Widget extends React.PureComponent {
  render() {
    const { __typename } = this.props.widget;

    switch (__typename) {
      case 'TextWidget':
        return <TextWidget {...this.props} />;
      case 'VideoWidget':
        return <VideoWidget {...this.props} />;
      case 'ImageWidget':
        return <ImageWidget {...this.props} />;
      case 'LinkWidget':
        return <LinkWidget {...this.props} />;
      case 'AttachmentWidget':
        return <AttachmentWidget {...this.props} />;
      case 'ResponseFormWidget':
        return <ResponseFormWidget {...this.props} />;
      case 'MultiChoiceWidget':
        return <MultiChoice.Editor {...this.props} />;
      default:
        return (
          <Alert variant="warning">
            <Alert.Heading>{_.upperCase(__typename)} is not implemented yet</Alert.Heading>
          </Alert>
        );
    }
  }
}

export default Widget;
