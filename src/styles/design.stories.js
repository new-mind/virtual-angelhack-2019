import React from 'react';
import { storiesOf } from '@storybook/react';

import { Table } from 'react-bootstrap';

storiesOf('Design Variables', module)
  .addDecorator(story => <div className="p-xxl">{story()}</div>)
  .add('Grid Spacers', () => (
    <>
      <h1>Grid Spacers</h1>
      <Table bordered hover className="w-50">
        <thead>
        <tr>
          <th className="w-25">Variable</th>
          <th className="w-25">Value</th>
        </tr>
        </thead>
        <tbody>
        <tr>
          <td><b>0</b>:</td>
          <td><i>0px</i></td>
        </tr>
        <tr>
          <td><b>s1</b>:</td>
          <td><i>4px</i></td>
        </tr>
        <tr>
          <td><b>s2</b>:</td>
          <td><i>8px</i></td>
        </tr>
        <tr>
          <td><b>s3</b>:</td>
          <td><i>12px</i></td>
        </tr>
        <tr>
          <td><b>m1</b>:</td>
          <td><i>16px</i></td>
        </tr>
        <tr>
          <td><b>m2</b>:</td>
          <td><i>20px</i></td>
        </tr>
        <tr>
          <td><b>m3</b>:</td>
          <td><i>24px</i></td>
        </tr>
        <tr>
          <td><b>l1</b>:</td>
          <td><i>28px</i></td>
        </tr>
        <tr>
          <td><b>l2</b>:</td>
          <td><i>32px</i></td>
        </tr>
        <tr>
          <td><b>l3</b>:</td>
          <td><i>36px</i></td>
        </tr>
        <tr>
          <td><b>xl</b>:</td>
          <td><i>40px</i></td>
        </tr>
        <tr>
          <td><b>xxl</b>:</td>
          <td><i>60px</i></td>
        </tr>
        </tbody>
      </Table>
    </>
  ))
  .add('Color Schema', () => (
    <>
      <h1>Color Schema</h1>
      <Table bordered hover className="w-50">
        <thead>
        <tr>
          <th className="w-25">Variable</th>
          <th className="w-25">Value</th>
          <th className="w-25">Example</th>
        </tr>
        </thead>
        <tbody>
        <tr>
          <td><b>$MAIN1</b>:</td>
          <td><i>#231f20ff</i></td>
          <td style={{ backgroundColor: '#231f20ff' }} />
        </tr>
        <tr>
          <td><b>$MAIN1_O20</b>:</td>
          <td><i>#231f20cc</i></td>
          <td style={{ backgroundColor: '#231f20cc' }} />
        </tr>
        <tr>
          <td><b>$MAIN1_O40</b>:</td>
          <td><i>#231f2099</i></td>
          <td style={{ backgroundColor: '#231f2099' }} />
        </tr>
        <tr>
          <td><b>$MAIN1_O60</b>:</td>
          <td><i>#231f2066</i></td>
          <td style={{ backgroundColor: '#231f2066' }} />
        </tr>
        <tr>
          <td><b>$MAIN1_O80</b>:</td>
          <td><i>#231f2033</i></td>
          <td style={{ backgroundColor: '#231f2033' }} />
        </tr>
        <tr>
          <td><b>$MAIN2</b>:</td>
          <td><i>#f2f2f2ff</i></td>
          <td style={{ backgroundColor: '#f2f2f2ff' }} />
        </tr>
        <tr>
          <td><b>$MAIN2_O20</b>:</td>
          <td><i>#f2f2f2cc</i></td>
          <td style={{ backgroundColor: '#f2f2f2cc' }} />
        </tr>
        <tr>
          <td><b>$MAIN2_O40</b>:</td>
          <td><i>#f2f2f299</i></td>
          <td style={{ backgroundColor: '#f2f2f299' }} />
        </tr>
        <tr>
          <td><b>$MAIN2_O60</b>:</td>
          <td><i>#f2f2f266</i></td>
          <td style={{ backgroundColor: '#f2f2f266' }} />
        </tr>
        <tr>
          <td><b>$MAIN2_O80</b>:</td>
          <td><i>#f2f2f233</i></td>
          <td style={{ backgroundColor: '#f2f2f233' }} />
        </tr>
        <tr>
          <td><b>$MAIN3</b>:</td>
          <td><i>#1ec9e8ff</i></td>
          <td style={{ backgroundColor: '#1ec9e8ff' }} />
        </tr>
        <tr>
          <td><b>$MAIN3_O20</b>:</td>
          <td><i>#1ec9e8cc</i></td>
          <td style={{ backgroundColor: '#1ec9e8cc' }} />
        </tr>
        <tr>
          <td><b>$MAIN3_O40</b>:</td>
          <td><i>#1ec9e899</i></td>
          <td style={{ backgroundColor: '#1ec9e899' }} />
        </tr>
        <tr>
          <td><b>$MAIN3_O60</b>:</td>
          <td><i>#1ec9e866</i></td>
          <td style={{ backgroundColor: '#1ec9e866' }} />
        </tr>
        <tr>
          <td><b>$MAIN3_O80</b>:</td>
          <td><i>#1ec9e833</i></td>
          <td style={{ backgroundColor: '#1ec9e833' }} />
        </tr>
        <tr>
          <td><b>$MAIN4</b>:</td>
          <td><i>#e85668ff</i></td>
          <td style={{ backgroundColor: '#e85668ff' }} />
        </tr>
        <tr>
          <td><b>$MAIN4_O20</b>:</td>
          <td><i>#e85668cc</i></td>
          <td style={{ backgroundColor: '#e85668cc' }} />
        </tr>
        <tr>
          <td><b>$MAIN4_O40</b>:</td>
          <td><i>#e8566899</i></td>
          <td style={{ backgroundColor: '#e8566899' }} />
        </tr>
        <tr>
          <td><b>$MAIN4_O60</b>:</td>
          <td><i>#e8566866</i></td>
          <td style={{ backgroundColor: '#e8566866' }} />
        </tr>
        <tr>
          <td><b>$MAIN4_O80</b>:</td>
          <td><i>#e8566833</i></td>
          <td style={{ backgroundColor: '#e8566833' }} />
        </tr>
        <tr>
          <td><b>$MAIN5</b>:</td>
          <td><i>#7acc5eff</i></td>
          <td style={{ backgroundColor: '#7acc5eff' }} />
        </tr>
        <tr>
          <td><b>$MAIN5_O20</b>:</td>
          <td><i>#7acc5ecc</i></td>
          <td style={{ backgroundColor: '#7acc5ecc' }} />
        </tr>
        <tr>
          <td><b>$MAIN5_O40</b>:</td>
          <td><i>#7acc5e99</i></td>
          <td style={{ backgroundColor: '#7acc5e99' }} />
        </tr>
        <tr>
          <td><b>$MAIN5_O60</b>:</td>
          <td><i>#7acc5e66</i></td>
          <td style={{ backgroundColor: '#7acc5e66' }} />
        </tr>
        <tr>
          <td><b>$MAIN5_O80</b>:</td>
          <td><i>#7acc5e33</i></td>
          <td style={{ backgroundColor: '#7acc5e33' }} />
        </tr>
        </tbody>
      </Table>
    </>
  ));
