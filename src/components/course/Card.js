import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import Card from '@lms/components/blocks/Card';
import Button from '@lms/components/blocks/Button';

import { NavLink } from 'react-router-dom';
import img from '@lms/assets/bg.png';

import styles from './styles.module.scss'

class Image extends Component {
  render() {
    const { imageUrl } = this.props;

    return (
      <div className={styles.cardImg}>
        <Card.Img
          className={styles.cardImg}
          src={imageUrl || img} alt="Card image" />
      </div>
    );
  }
}

class CourseCard extends Component {
  static propTypes = {
    title: PropTypes.string,
    description: PropTypes.string,
    url: PropTypes.string,
    imageUrl: PropTypes.string,
    onEdit: PropTypes.func,
    onDelete: PropTypes.func,
    onAssign: PropTypes.func,
  };

  static defaultProps = {
    title: '',
    description: '',
    url: '',
    imageUrl: '',
    onEdit: null,
    onDelete: null,
    onAssign: null,
  };

  render() {
    const { title, description, url, imageUrl, onEdit, onDelete, onAssign } = this.props;

    return (
      <Card className={cn(styles.card, 'mb-4')}>
        <Image imageUrl={imageUrl} />
        <Card.ImgOverlay>
          <Card.Title><NavLink to={url}>{title}</NavLink></Card.Title>
          <Card.Text className={styles.text}>{description}</Card.Text>
          <div className={styles.buttons}>
            {onEdit && (
              <Button onClick={onEdit} className="mr-3">
                Edit
              </Button>
            )}
            {onDelete && (
              <Button className="transparent" onClick={onDelete}>
                Delete
              </Button>
            )}
            {onAssign && (
              <Button className="transparent" onClick={onAssign}>
                Assign
              </Button>
            )}
          </div>
        </Card.ImgOverlay>
      </Card>
    );
  }
}

export default CourseCard;
