import React, { Component } from 'react';
import cn from 'classnames';

import styles from './styles.module.scss'

class Grid extends Component {
  render() {
    const { className } = this.props;

    return (
      <div className={cn(styles.container, className)}>
        {this.props.children}
      </div>
    );
  }
}

Grid.propTypes = {};

export default Grid;
