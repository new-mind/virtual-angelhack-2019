import React from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import cn from 'classnames';

import styles from './CourseCard.module.scss';
import { NavLink } from 'react-router-dom';

class CourseCard extends React.Component {
  style = {
    backgroundImage: `url(${require(`./images/${_.random(1, 4)}.svg`)})`,
  };

  static propTypes = {
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    description: PropTypes.string,
    actions: PropTypes.node,
  };

  render() {
    const { id, title, description, actions } = this.props;

    return (
      <div className={cn(styles.card, 'p-m2')} style={this.style}>
        <h1 className={cn(styles.title, 'mb-m1')}>
          <NavLink to={`/courses/${id}`}>
            {title}
          </NavLink>
        </h1>
        {description && <p className={cn(styles.description, actions ? 'mb-m1' : 'm-0')}>{description}</p>}
        {actions && <footer className={styles.actions}>{actions}</footer>}
      </div>
    );
  }
}

export default CourseCard;
