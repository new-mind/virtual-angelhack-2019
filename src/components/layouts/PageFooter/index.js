import React from 'react';
import cn from 'classnames';
import { Nav, Container } from 'react-bootstrap';
import { withTranslation } from 'react-i18next';

import './i18n';
import Language from '@lms/components/Language';

import styles from './PageFooter.module.scss';

const REFS = {
  I: 'https://www.instagram.com/eduway.today/',
  T: 'https://t.me/eduwaytoday',
  E: 'mailto:support@eduway.today',
  A: 'https://eduway.today/',
};

function Footer(props) {
  const { t } = props;

  return(
    <Container fluid className={cn(styles.layer, 'p-0')}>
      <Container className="p-0 h-100">
        <footer className={cn(styles.footer, 'h-100')}>
          <div className={styles.socials}>
            <Nav className="flex-row align-items-center h-100">
              <Nav.Link target="_blank" href={REFS.I} className={cn(styles.social, styles.instagram)} />
              <Nav.Link target="_blank" href={REFS.T} className={cn(styles.social, styles.telegram)} />
              <Nav.Link target="_blank" href={REFS.E} className={cn(styles.social, styles.email)} />
            </Nav>
          </div>
          <div className={styles.info}>
            <Nav className="h-100">
              <Nav.Link target="_blank" href={REFS.A}>{t("About Us")}</Nav.Link>
              <Nav.Link target="_blank" href={t('privacyPolicyLink')}>{t("Privacy policy")}</Nav.Link>
              <Nav.Link target="_blank" href={t('termsOfUseLink')}>{t("Terms of use")}</Nav.Link>
            </Nav>
          </div>
          <div className={styles.language}>
            <Language className="h-100 justify-content-end" />
          </div>
        </footer>
      </Container>
    </Container>
  );
}

export default withTranslation('components/Footer')(Footer);
