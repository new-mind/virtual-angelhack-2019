import React from 'react';
import { storiesOf } from '@storybook/react';
import StoryRouter from 'storybook-react-router';

import Footer from './index';

storiesOf('Layout|Footer', module)
  .addDecorator(StoryRouter())
  .addDecorator(storyFn => <div className="h-100 d-flex align-items-end">{storyFn()}</div>)
  .add('Default', () => (
    <Footer />
  ));
