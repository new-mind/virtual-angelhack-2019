import React from 'react';
import cn from 'classnames';
import { Container } from 'react-bootstrap';

import styles from './PageContent.module.scss';

class PageContent extends React.Component {
  render() {
    return (
      <Container className={cn(styles.pageContent, this.props.className)}>
        {this.props.children}
      </Container>
    );
  }
}

export default PageContent;
