import PropTypes from 'prop-types';

export const propTypes = {
  comments: PropTypes.arrayOf(
    PropTypes.shape({
      from: PropTypes.string,
      createdAt: PropTypes.string,
      text: PropTypes.string,
      content: PropTypes.string
    }),
  ),
};
