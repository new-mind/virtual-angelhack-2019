import React from 'react';
import { convertToRaw } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import Button from '@lms/components/blocks/Button';
import { withTranslation } from 'react-i18next';
import cn from 'classnames';

import styles from '../styles.module.scss'
import '../i18n';

const toolbar = {
  options: ['inline'],
  inline: {
    options: ['bold', 'italic', 'underline', 'strikethrough' ],
  }
}

class AddFeedbackForm extends React.Component {
  constructor(props) {
    super(props);

    this._ref = React.createRef();
  }

  onSubmit = () => {
    const { onSubmit } = this.props;
    const editorState = this._ref.current.getEditorState();
    const content = editorState.getCurrentContent();
    if (!content.hasText()) return;

    onSubmit && onSubmit(JSON.stringify(convertToRaw(content)));
    this._ref.current.setState({
      editorState: this._ref.current.createEditorState(null)
    });
  }

  render () {
    const { t } = this.props;
    return (
      <>
        <hr className={cn(styles.separator, {separatorAtRight: this.props.separatorAtRight})} />
        <h2 className={styles.title}>{t("Your feedback and comments")}</h2>
        <Editor
          ref={this._ref}
          placeholder={t("Enter feedback")}
          wrapperClassName={styles.feedbackForm}
          editorClassName={styles.editor}
          toolbar={toolbar}/>
        <p className={cn({buttomAtRight: this.props.buttomAtRight})}>
          <Button type="submit" className="w-50" onClick={this.onSubmit}>{this.props.buttomText || t("Submit")}</Button>
        </p>
      </>
    )
  }
}

export default withTranslation('components/Feedback')(AddFeedbackForm);
