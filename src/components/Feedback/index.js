import React from 'react';
import cn from 'classnames';
import moment from 'moment';
import * as _ from 'lodash';
import convertToHtml from 'draftjs-to-html';
import { withTranslation } from 'react-i18next';
import hoistStatics from 'hoist-non-react-statics';

import * as props from './props';

import './i18n';
import styles from './styles.module.scss';
import AddFeedbackForm from './forms/addFeedback';

const FROM = {
  TEACHER: 'TEACHER',
  STUDENT: 'STUDENT',
};

class Feedback extends React.Component {
  static addForm = AddFeedbackForm;
  static propTypes = props.propTypes;

  render() {
    const { comments, t } = this.props;
    return (
      <section className={styles.feedback}>
        {_.map(comments, (message, i) => (
          <article className={cn(styles.message, message.from === FROM.TEACHER ? styles.teacher : styles.student)} key={i}>
            <h1 className={styles.title}>
              {message.from === FROM.TEACHER ? t("Teacher's feedback") : t("Student's comment")} ({moment(message.createdAt).format('DD/MM/YYYY')})
            </h1>
            <p className={styles.text}>{this.renderText(message)}</p>
          </article>
        ))}
      </section>
    );
  }

  renderText(message) {
    if (message.text) {
      return message.text;
    }

    const { content } = message;
    if (!content) return null;

    return (
      <span dangerouslySetInnerHTML={{__html: convertToHtml(JSON.parse(content))}} />
    );
  }
}

export default hoistStatics(
  withTranslation('components/Feedback')(Feedback),
  Feedback
);
