import React from 'react';
import { Editor } from '@tinymce/tinymce-react';

class TinyEditor extends React.Component {
  render() {
    return (
      <Editor
        apiKey="c7cgjub47x9inuboe4t0eay9frjls6u5s8y389nch5li8ctq"
        initialValue={this.props.value}
        init={{
          height: 500,
          menubar: false,
          plugins: [
            'advlist autolink lists link image charmap print preview anchor table',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table paste code help wordcount'
          ],
          toolbar:
            `undo redo | formatselect | bold italic backcolor |
            alignleft aligncenter alignright alignjustify | table |
            ullist numlist outdent indent | removeformat | help`
        }}
        onChange={this.props.onChange}
      />
    );
  }
}

export default TinyEditor;
