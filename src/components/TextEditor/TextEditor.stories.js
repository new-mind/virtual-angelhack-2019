import React from 'react';

import { storiesOf } from '@storybook/react';

import Editor from './TinyEditor';

const StoryEditor = function () {
  const [value, setValue] = React.useState('');

  return <Editor value={value} onChange={setValue} />
};

storiesOf('Editor:Text', module)
  .add('By default', () => (<div className="p-xxl"><StoryEditor /></div>));
