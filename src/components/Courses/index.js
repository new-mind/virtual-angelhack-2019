import React from 'react';
import { Col, Row } from 'react-bootstrap';

class Courses extends React.Component {
  static layout = index => ({
    xs: 12,
    lg: Math.ceil(index / 2) % 2 !== 1 ? 7 : 5
  });

  render() {
    return (
      <Row className="mx-ns3 my-ns3">
        {this.props.children.map((course, index) => (
          <Col key={course.key} {...Courses.layout(index)} className="p-s3">
            {course}
          </Col>
        ))}
      </Row>
    );
  }
}

export default Courses;
