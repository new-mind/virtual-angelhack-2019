import React from 'react';
import uuid from 'uuid/v4';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import StoryRouter from 'storybook-react-router';

import Courses from './index';
import CourseCard from '@lms/components/CourseCard';
import Button from '@lms/components/blocks/Button';

const title = 'Lorem ipsum';
const description = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi maximus posuere aliquet. In pretium ante a ex congue gravida. Duis non aliquet mi. Nulla lobortis ut nibh eu tincidunt. Aliquam molestie sagittis tempor. Curabitur laoreet pellentesque tellus, quis feugiat tortor tempus id. Duis elementum eros velit, vel sodales justo pulvinar sit amet. Cras blandit justo sit amet consectetur venenatis. Integer tempus, ex at fermentum ullamcorper, lorem eros tincidunt dui, ac molestie mauris arcu vel nunc. Aliquam sed facilisis libero, id viverra massa.';
const actions = (id) => (
  <>
    <Button onClick={action(`Edit ${id}`)} className="mr-3">Edit</Button>
    <Button.Confirm
      className="transparent"
      confirm="Confirm"
      onClick={action(`Delete ${id}`)}
    >
      Delete
    </Button.Confirm>
  </>
);
let courses = new Array(10);
courses.fill({ title, description });
courses = courses.map(() => ({ title, description, id: uuid() }));

storiesOf('Components|Courses/Grid', module)
  .addDecorator(StoryRouter())
  .addDecorator(storyFn => (<div className="p-xxl">{storyFn()}</div>))
  .add('Admin', () => (
    <Courses>
      {courses.map((course) => (
        <CourseCard
          id={course.id}
          key={course.id}
          title={course.title}
          description={course.description}
          actions={actions(course.id)}
        />
      ))}
    </Courses>
  ));
