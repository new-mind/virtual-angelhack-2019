import React from 'react';

import styles from './styles.module.scss';
import PageContent from '@lms/components/layouts/PageContent';

class Dashboard extends React.Component {
  render() {
    const { content } = this.props;
    return (
      <PageContent className={styles.dashboard}>
        { content }
      </PageContent>
    );
  }
}

export default Dashboard;
