import React from 'react';

import styles from './LessonNotFound.module.scss';

function LessonNotFound() {
  return (
    <div className={styles.lessonNotFound}>
      Lesson Not Found
    </div>
  );
}

export default LessonNotFound;
