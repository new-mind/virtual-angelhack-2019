import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { Responsive, WidthProvider } from 'react-grid-layout';

import LessonCard from './index';
import Button from '@lms/components/blocks/Button';

const GridLayout = WidthProvider(Responsive);

const longTitle = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ac tincidunt mi. Pellentesque volutpat vel nulla gravida pretium. Suspendisse quis lorem turpis. Vivamus vel augue ut ex eleifend euismod sagittis a magna. Proin vel ligula vel sapien aliquam viverra vitae quis neque. Nunc consequat orci quis dui venenatis fringilla. Nunc.';
const actions = (
  <>
    <Button onClick={action('Edit')} className="mr-3">Edit Lesson</Button>
    <Button.Confirm
      className="transparent"
      confirm="Confirm"
      onClick={action('Delete')}
    >
      Delete
    </Button.Confirm>
  </>
);

storiesOf('Components|Lesson/Card', module)
  .addDecorator(storyFn => <div className="p-xxl">{storyFn()}</div>)
  .add('Admin: In Grid', () => (
    <GridLayout
      breakpoints={{lg: 1200, md: 996, sm: 768, xs: 480, xxs: 0}}
      cols={{lg: 12, md: 12, sm: 12, xs: 1, xxs: 1}}
      isDraggable={false}
      isResizable={false}
      items={50}
      rowHeight={248}
    >
      <div key="a" data-grid={{ x: 0, y: 0, w: 7, h: 1 }}>
        <LessonCard actions={actions} />
      </div>
      <div key="b" data-grid={{ x: 7, y: 0, w: 5, h: 1 }}>
        <LessonCard isDraft={true} isHomework={true} title={longTitle} />
      </div>
      <div key="c" data-grid={{ x: 0, y: 1, w: 5, h: 1 }}>
        <LessonCard title={longTitle} actions={actions} />
      </div>
      <div key="d" data-grid={{ x: 5, y: 1, w: 7, h: 1 }}>
        <LessonCard actions={actions} />
      </div>
    </GridLayout>
  ))
  .add('Admin: Single', () => (
    <LessonCard isDraft={true} isHomework={true} actions={actions} />
  ));
