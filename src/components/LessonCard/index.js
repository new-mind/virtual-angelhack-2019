import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import { withTranslation } from 'react-i18next';
import _ from 'lodash';

import styles from './LessonCard.module.scss';
import './i18n';

class LessonCard extends React.Component {
  style = {
    backgroundImage: `url(${require(`./images/${_.random(1, 4)}.svg`)})`,
  };

  static propTypes = {
    title: PropTypes.string.isRequired,
    isDraft: PropTypes.bool,
    isHomework: PropTypes.bool,
    actions: PropTypes.node,
  };

  static defaultProps = {
    title: 'Lorem ipsum dolor sit amet',
    isDraft: false,
    isHomework: false,
  };

  render() {
    const { props } = this;

    return (
      <div className={cn(styles.card, 'p-m2')} style={this.style}>
        <div className={styles.labels}>
          {props.isHomework && <div className={cn(styles.label, props.isHomework && styles.homework)}>{props.t('Homework')}</div>}
          {props.isDraft && <div className={cn(styles.label, props.isDraft && styles.draft)}>{props.t('Unpublished Changes')}</div>}
        </div>
        <h1 className={cn(styles.title, 'my-m2')}>{props.title}</h1>
        <footer className={styles.footer}>
          {props.actions && (
            props.actions
          )}
        </footer>
      </div>
    );
  }
}

export default withTranslation('lessonCard')(LessonCard);
