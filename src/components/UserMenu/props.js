import PropTypes from 'prop-types';
import * as consts from './consts';

export const propTypes = {
  role: PropTypes.string,
  menu: PropTypes.element,
};

export const defaultProps = {
  role: consts.ADMIN_ROLE,
  menu: null,
};
