import React from 'react';
import cn from 'classnames';
import _ from 'lodash';

import { withApollo, compose, Query } from 'react-apollo';
import { Dropdown } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import { withTranslation } from 'react-i18next';

import Error from '@lms/components/Error';
import { PORTAL } from '@lms/constants';
import Logout from '@lms/components/blocks/Logout';

import { GET_ORGANIZATIONS } from './api';
import './i18n';
import styles from './styles.module.scss';

class Toggle extends React.Component {
  render() {
    return (
      <div onClick={this.handleClick} className={cn(styles.currentRole, 'd-flex flex-row align-items-center nav-link p-0')}>
        {this.props.content}
      </div>
    );
  }

  handleClick = (e) => {
    e.preventDefault();

    this.props.onClick && this.props.onClick(e);
  }
}

class UserMenu extends React.Component {
  constructor(props) {
    super(props);
    this._ref = React.createRef();
  }

  handleRoleChange = () => {
    const switcher = {
      admin: 'TEACHER',
      teacher: 'ADMIN',
    };

    this.props.onRoleChange(switcher[this.props.currentRole.toLowerCase()]); // @todo refactor due to new case with student
  };

  renderSwitcher () {
    const { roles, currentRole, t } = this.props;
    const isAdmin = !!_.size(roles.admin);
    const isTeacher = isAdmin || _.size(roles.teacher);
    const isStudent = !!_.size(roles.student);
    if (isAdmin + isTeacher + isStudent < 2) return null;

    return (
      <div className={styles.switcher}>
        { isAdmin &&
            <div className={cn(styles.role, {active: currentRole === "ADMIN"})}
                onClick={() => this.props.onRoleChange('ADMIN')}>
              <span className={styles.pointer}></span>
              <span className={styles.title}>{t("Admin")}</span>
          </div>
        }
        { isTeacher &&
            <div className={cn(styles.role, {active: currentRole === "TEACHER"})}
                onClick={() => this.props.onRoleChange('TEACHER')}>
              <span className={styles.pointer}></span>
              <span className={styles.title}>{t("Teacher")}</span>
            </div> }
        { isStudent &&
            <div className={cn(styles.role, {active: currentRole === "STUDENT"})}
                onClick={() => this.props.onRoleChange('STUDENT')}>
              <span className={styles.pointer}></span>
              <span className={styles.title}>{t("Student")}</span>
            </div>
        }
      </div>
    );
  }

  render() {
    const { user, t } = this.props;

    return (
      <Dropdown>
        <Dropdown.Toggle as={Toggle} content={this.props.children}/>
        <Dropdown.Menu alignRight className={cn("px-s3 py-m1")} ref={this._ref}>
          { this.renderSwitcher() }
          <div className={cn(styles.name, 'mb-s1')}>{user.name} {user.surname}</div>
          <div className={cn(styles.email, 'mb-m1')}>({user.email})</div>
          {/*<span className={cn(styles.changeLink, 'd-block mb-m1')}>Change login/password</span>*/}
          <hr className={cn(styles.separator, 'mt-0 mb-m3')}/>
          {/*<div className={cn(styles.settings, 'mb-s2 d-flex flex-row align-items-center')}><i/> settings</div>*/}
          { this.renderOrganizations() }
          <Dropdown.Item as={NavLink} className={cn(styles.profile, 'p-0 mb-m1 d-flex flex-row align-items-center')} to="/profile">
            <i/> {t("profile")}
          </Dropdown.Item>
          <Logout className={cn(styles.logout, 'd-flex flex-row align-items-center')}>
            <i/>{t("Log out")}
          </Logout>
        </Dropdown.Menu>
      </Dropdown>
    );
  }

  renderOrganizations() {
    const { currentRole, roles } = this.props;
    const organizations = {
      [PORTAL.ADMIN]: roles.admin,
      [PORTAL.TEACHER]: _.extend([], roles.teacher, roles.admin)
    }[currentRole];

    if (_.size(organizations) < 2) return null;

    return (
      <Query query={GET_ORGANIZATIONS} variables={{ids: organizations}}>
        {({loading, error, data}) => {
          if (loading) return null;
          if (error) return <Error {...error} />;
          if (_.size(data.getOrganizations) < 2) return null;

          return (
            <>
              {_.map(data.getOrganizations, ({id, title}, i) => (
                <div
                    key={i}
                    onClick={() => {
                      this.props.onOrganizationChange(id);
                      this._ref.current.handleClose();
                    }}
                    className={cn(styles.org, 'mb-m1')}>
                  <span>{title || id}</span>
                </div>
              ))}
              <hr className={cn(styles.separator, 'mt-0 mb-m3')}/>
            </>
          );
        }}
      </Query>
    )
  }
}

export default compose(
  withApollo,
  withTranslation('components/UserMenu')
)(UserMenu);
