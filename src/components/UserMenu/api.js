import gql from 'graphql-tag';

export const GET_ORGANIZATIONS = gql`
  query GetOrganizations($ids: [ID!]!) {
    getOrganizations(ids: $ids) {
      id
      title
    }
  }
`;
