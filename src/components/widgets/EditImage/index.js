import React from 'react'
import ImageUploader from '@lms/components/blocks/ImageUploader';
import EditableText from '@lms/components/blocks/TextEditable/TextEditable.js';
import { v4 as uuid } from 'uuid';
import { Storage } from 'aws-amplify';

import styles from './styles.module.scss';
import Resizable from '../Resizable';

class EditImage extends React.Component {
  static get data() {
    return {
      type: "ImageWidget",
      title: "Image",
      width: "560",
      height: "auto"
    }
  }

  constructor(props) {
    super(props);

    this.state = {
      url: props.widget.url
    }
  }

  handleImagePick = async (file) => {
    const { widget, onChange, lessonId } = this.props;

    let match = /(\.[\w\d]*?)$/.exec(file.name);
    let ext = '';
    if (match) { ext = match[1]; }
    const {key}= await Storage.put(`${lessonId}/${uuid()}${ext}`, file, {
      contentType: file.type,
      ACL: 'public-read',
      level: 'public'
    });

    onChange && onChange({
      id: widget.id,
      type: 'ImageWidget',
      url: ImageUploader.getUrl(key)
    }, widget);
  };

  handleChangeTitle = (value) => {
    const { widget, onChange } = this.props;

    onChange && onChange({
      id: widget.id,
      type: 'ImageWidget',
      title: value
    }, widget)
  };

  render() {
    const { widget, collapsed } = this.props;
    const { title, width, height } = widget;

    return (
      <div className={styles.container}>
        <h3><EditableText onChange={this.handleChangeTitle}>{title}</EditableText></h3>
        { !collapsed ?
          <Resizable widget={widget} onChange={this.props.onChange}>
            <div style={{width, height}}>
            <ImageUploader
              imageUrl={this.state.url}
              width={width}
              height={height}
              onPick={this.handleImagePick}/>
            </div>
          </Resizable>
          : null
        }
      </div>
    );
  }
}

export default EditImage
