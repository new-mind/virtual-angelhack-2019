import React from 'react';
import { Form } from 'react-bootstrap';

import EditableText from '@lms/components/blocks/TextEditable/TextEditable';

class EditH5PWidget extends React.PureComponent {
  handleChangeTitle = () => {
  };

  handleChange = (content) => {
    const { id, onChange } = this.props;
    onChange && onChange({
      id,
      widget: {
        __typename: 'H5PWidget',
        content
      }
    })
  }

  render() {
    return (
      <div>
        <h3>
          <EditableText onChange={this.handleChangeTitle}>
            {this.props.title}
          </EditableText>
        </h3>
        <Form.Group>
          <Form.Control
            as="textarea"
            rows="3"
            defaultValue={this.props.content}
            onChange={e => this.handleChange(e.target.value)}/>
        </Form.Group>
      </div>
    )
  }
}

export default EditH5PWidget
