import React from 'react';
import convertToHtml from 'draftjs-to-html';

import styles from './Text.module.scss';

class TextWidget extends React.PureComponent {
  render() {
    const data = this.props.data ? JSON.parse(this.props.data) : null;
    return (
      <div className={styles.viewer} dangerouslySetInnerHTML={{__html: convertToHtml(data)}} />
    );
  }
}

export default TextWidget;
