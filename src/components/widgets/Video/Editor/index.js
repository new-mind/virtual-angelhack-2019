import React from 'react';
import cn from 'classnames'
import * as _ from 'lodash';
import { withTranslation } from 'react-i18next';
import hoistStatics from 'hoist-non-react-statics';

import VideoWidget from '@lms/components/widgets/Video/Viewer';
import Text from '@lms/components/blocks/Text';
import Resizable from '../../Resizable';

import '../i18n';
import styles from './VideoEditor.module.scss';
import Input from '@lms/components/blocks/Input/Input';

class EditVideoWidget extends React.PureComponent {
  static get data() {
    return {
      type: 'VideoWidget',
      title: 'Video',
      url: null,
      width: '560',
      height: '315',
    };
  }

  onUrlChange = _.debounce(value => {
    const { widget, onChange } = this.props;

    if (!value) return;

    onChange && onChange({
      id: widget.id,
      type: 'VideoWidget',
      url: value
    }, widget);
  }, 300, {leading: false, trailing: true});

  handleChangeTitle = (value) => {
    const { onChange, widget } = this.props;

    onChange && onChange({
      id: widget.id,
      type: 'VideoWidget',
      title: value
    }, widget);
  };

  render() {
    const { widget, collapsed, t } = this.props;
    const { id, title, width, height, url } = widget;

    return (
      <div>
        <h5 className={cn(styles.title, 'm-0')}>
          <Text.Editable onChange={this.handleChangeTitle}>
            {title}
          </Text.Editable>
        </h5>
        {!collapsed ?
          <>
            <Resizable widget={widget} onChange={this.props.onChange}>
              <Input
                name={`${id}-url`}
                type="text"
                className="my-m1"
                style={{ width: `${width}px` }}
                placeholder={t('placeholder')}
                defaultValue={url}
                onChange={(e) => this.onUrlChange(e.target.value)}
              />
              <div>
                {url ? (
                  <VideoWidget url={url} width={width} height={height}/>
                ) : (
                  <div
                    className={cn(styles.placeholder, 'mb-s3')}
                    style={{
                      width: `${width}px`,
                      height: `${height}px`,
                      fontSize: `${0.0816 * height}px`,
                    }}
                  >
                    {t('Video Preview Will Be Here')}
                  </div>
                )}
              </div>
            </Resizable>
          </>
          : null
        }
      </div>
    )
  }
}

export default hoistStatics(
  withTranslation('widgets/video')(EditVideoWidget),
  EditVideoWidget,
);
