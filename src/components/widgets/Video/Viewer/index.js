import React from 'react'
import Youtube from 'react-youtube'

class VideoWidget extends React.PureComponent {
  getVideoId() {
    const regs = [
      /^http.*?youtube.*?v=([\w\d-_]+).*/,
      /^http.*?youtu\.be\/([\w\d-_]+).*/
    ];

    for (let reg of regs) {
      const match = reg.exec(this.props.url);
      if (match)
        return match[1]
    }

    return null;
  }

  render() {
    const opts = {
      width: '100%', // parseInt(this.props.width), // @todo fix styles for mobile version
      height: parseInt(this.props.height)
    };
    const videoId = this.getVideoId();

    return (
      <div>
        { this.props.title ?  <h3>{this.props.title}</h3> : null }
        { videoId ? <Youtube videoId={videoId} opts={opts} /> : null }
      </div>
    )
  }
}

export default VideoWidget
