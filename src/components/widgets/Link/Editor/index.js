import React from 'react';
import cn from 'classnames';
import * as _ from 'lodash';
import { v4 as uuid } from 'uuid';

import LinkItem from './Item'
import Text from '@lms/components/blocks/Text';

import widgetsStyles from '../../widgets.module.scss';

class EditLink extends React.PureComponent {
  static defaultProps = {
    editableTitle: true
  };

  static get data() {
    return {
      type: "LinkWidget",
      title: "Link",
      items: [
        { id: uuid() }
      ]
    }
  }

  handleSubmit = (index, values) => {
    const { widget, onChange } = this.props;
    let { items } = widget;

    items.splice(index, 1, values);
    //TODO: hack
    _.forEach(items, (item) => delete item.__typename);

    onChange && onChange({
      id: widget.id,
      type: 'LinkWidget',
      items: items
    }, widget);
  };

  addRow = (i) => {
    const { widget, onChange } = this.props;
    let { items } = widget;
    items.splice(i + 1, 0, {
      id: uuid()
    });
    //TODO: hack
    _.forEach(items, (item) => delete item.__typename);

    onChange && onChange({
      id: widget.id,
      type: 'LinkWidget',
      items
    }, widget);
  };

  removeRow = (i) => {
    const { widget, onChange } = this.props;
    let { items } = widget;
    _.pullAt(items, i);
    //TODO: hack
    _.forEach(items, (item) => delete item.__typename);

    onChange && onChange({
      id: widget.id,
      type: 'LinkWidget',
      items
    }, widget);
  };

  handleChangeTitle = (value) => {
    let { widget, onChange } = this.props;

    onChange && onChange({
      id: widget.id,
      type: 'LinkWidget',
      title: value
    }, widget);
  };

  render() {
    const { widget, collapsed } = this.props;
    const { props } = this;

    return (
      <>
        <h5 className={cn(widgetsStyles.title, 'm-0')}>
          {props.editableTitle ?
            <Text.Editable onChange={this.handleChangeTitle}>
              {widget.title}
            </Text.Editable> :
            widget.title
          }
        </h5>
        {!collapsed ?
          _.map(widget.items, (item, i) => (
            <LinkItem
              {...item}
              index={i}
              key={item.id}
              canRemove={widget.items.length > 1}
              onChange={this.handleSubmit}
              onAddRow={this.addRow}
              onRemoveRow={this.removeRow}/>
          ))
          : null
        }
      </>
    )
  }
}

export default EditLink
