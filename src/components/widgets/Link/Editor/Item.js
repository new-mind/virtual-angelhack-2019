import React from 'react';
import { Form, Row, Col } from 'react-bootstrap';
import * as _ from 'lodash';
import { Formik } from 'formik';
import * as yup from 'yup';

import Button from '@lms/components/blocks/Button'
import styles from './styles.module.scss'

const schema = yup.object({
  url: yup.string().url("Must be a valid url").required("Must be a valid url"),
  title: yup.string().nullable()
});

class LinkItem extends React.PureComponent {
  handleSubmit = _.debounce((values) => {
    const { index, id, onChange } = this.props;
    onChange && onChange(index, {
      id,
      ...values
    });
  }, 500, {leading: false, trailing: true});

  handleChange = async (handleChange, submitForm, ...args) => {
    await handleChange.apply(null, args);
    submitForm();
  };

  render() {
    const { index, canRemove, title, url } = this.props;

    return (
      <Formik
        initialValues={{url, title}}
        validationSchema={schema}
        onSubmit={this.handleSubmit}>
      {({
        errors,
        handleChange,
        submitForm
      }) => (
        <Row className="mb-3">
          <Col md={3}>
            <Form.Group className={styles.link}>
              <Form.Control
                type="text"
                placeholder="Enter url"
                defaultValue={url}
                name="url"
                className="mr-3"
                onChange={ _.partial(this.handleChange, handleChange, submitForm) }
              />
              <Form.Control.Feedback type="invalid">
                {errors.url}
              </Form.Control.Feedback>
            </Form.Group>
          </Col>
          <Col md={3}>
          <Form.Group className={styles.link}>
            <Form.Control
              type="text"
              placeholder="Enter title"
              defaultValue={title}
              name="title"
              className="mr-3"
              onChange={ _.partial(this.handleChange, handleChange, submitForm) }
            />
          </Form.Group>
          </Col>
          <Col>
            <Button className="mr-3" onClick={_.partial(this.props.onAddRow, index)}>+</Button>
            {canRemove ? <Button.Confirm className="transparent" confirm="X" onClick={_.partial(this.props.onRemoveRow, index)}>-</Button.Confirm> : null }
          </Col>
        </Row>
      )}
      </Formik>
    )
  }
}

export default LinkItem;
