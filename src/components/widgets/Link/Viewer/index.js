import React from 'react';
import { Nav } from 'react-bootstrap';
import * as yup from 'yup';
import * as _ from 'lodash';

const schema = yup.string().url().required();

class LinkWidget extends React.Component {
  render() {
    return (
      <React.Fragment>
        { this.props.title ? <h3>{this.props.title}</h3> : null }
        {
          _.map(this.props.items, item => (
            <Link {...item} key={item.id} />
          ))
        }
      </React.Fragment>
    );
  }
}

class Link extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      url: null,
      title: null
    }

    schema.validate(this.props.url)
    .then(() => {
      this.setState({
        url: this.props.url,
        title: this.props.title
      })
    })
    .catch(_.noop);
  }

  render() {
    const {url, title} = this.state;
    if (!url) return null;

    return (
      <Nav.Link href={url} target="_blank" rel="noopener">
        <i className="fas fa-external-link-alt fa-lg mr-3"/>
        {title || url}
      </Nav.Link>
    );
  }
}

export default LinkWidget;
