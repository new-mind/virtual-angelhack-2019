import React from 'react';
import { Form, Overlay, Tooltip } from 'react-bootstrap';
import hoistStatics from 'hoist-non-react-statics';
import * as _ from 'lodash';
import { withTranslation } from 'react-i18next';

import TextEditor from '@lms/components/TextEditor';

import './i18n';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import styles from './styles.module.scss';

class EditText extends React.Component {
  static defaultProps = {
    editableTitle: true
  }
  static get data() {
    return {
      type: "TextWidget"
    };
  }

  constructor(props) {
    super(props);
    const { widget } = props;

    this.state = {
      target: null,
      showTooltip: false,
      data: widget.data ? JSON.parse(widget.data) : null
    }
  }

  handleChange = _.debounce((content) => {
    const { widget, onChange } = this.props;

    const data = JSON.stringify(content);
    if (data === this._cache) {
      return;
    }
    this._cache = data;

    onChange && onChange({
      id: widget.id,
      type: 'TextWidget',
      data
    }, widget)
  }, 1000, {leading: false, trailing: true});

  handleClickTitle = () => {
    this.setState({
      showTooltip: !this.state.showTooltip
    })
  }

  attachRef = (target) => {
    this.setState({ target });
  }

  getTitle() {
    const { widget, t, collapsed } = this.props;
    if (!_.isNil(widget.title)) return widget.title;

    if (!collapsed) return (<>&nbsp;</>); // TODO:

    return t("Text");
  }

  render() {
    const { target, showTooltip, data } = this.state;
    const { widget, collapsed } = this.props;
    this._cache = widget.data;

    return (
      <Form.Group className="mr-3">
        <div className={styles.header}>
          <h3>
            { this.props.editableTitle ?
                <span onClick={this.handleClickTitle} ref={this.attachRef}>{this.getTitle()}</span>
              :
                this.getTitle()
            }
          </h3>
        </div>

        { !collapsed ?
          <>
            <TextEditor
              onChange={this.handleChange}
              contentState={data}/>
            <Overlay target={target} show={showTooltip} placement="top">
              { props => (
                  <Tooltip {...props} show={props.show.toString()}>
                    You won't see the title in the output
                  </Tooltip>
              )}
            </Overlay>
          </>
          : null
        }
      </Form.Group>
    );
  }
}

export default hoistStatics(
  withTranslation('widgets/EditText')(EditText),
  EditText
);
