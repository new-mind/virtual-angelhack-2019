import React from 'react';
import PropTypes from 'prop-types';
import uuid from 'uuid/v4';
import { Auth, Storage } from 'aws-amplify';

import DropZone from 'react-dropzone-uploader';

import 'react-dropzone-uploader/dist/styles.css'

class EditAudio extends React.Component {
  static propTypes = {
    lessonId: PropTypes.string.isRequired,
    widget: PropTypes.shape({
      type: PropTypes.string,
      title: PropTypes.string,
      src: {
        id: PropTypes.string,
        fileKey: PropTypes.string,
        fileName: PropTypes.string,
        identityId: PropTypes.string,
      },
    }),
    onChange: PropTypes.func.isRequired,
  };

  static get data() {
    return {
      type: 'AudioWidget',
      title: 'Audio Record',
      src: {
        id: uuid(),
      },
    };
  }

  handleSubmit = async (files) => {
    const { widget } = this.props;

    const id = uuid();
    const { file, meta } = files[0];
    const ext = meta.name.split('.').pop();

    const { key } = await Storage.put(`${this.props.lessonId}/${id}${ext}`, file, {
      contentType: meta.type,
      level: 'protected',
    });

    const { identityId } = await Auth.currentCredentials();

    this.props.onChange && this.props.onChange({
      id: widget.id,
      type: 'AudioWidget',
      srs: {
        id,
        fileKey: key,
        fileName: meta.name,
        identityId,
      },
    }, widget);
  };

  render() {
    return (
      <DropZone
        accept="audio/*"
        maxFiles={1}
        multiple={false}
        inputContent="Select An Audio"
        onSubmit={this.handleSubmit}
      />
    );
  }
}

export default EditAudio;
