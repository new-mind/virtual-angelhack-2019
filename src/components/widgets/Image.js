import React from 'react';
import { Image } from 'react-bootstrap';

class ImageWidget extends React.PureComponent {
  render() {
    const { width, height } = this.props;
    return (
      <div>
        { this.props.title ?  <h3>{this.props.title}</h3> : null }
        <Image src={this.props.url} width={width || "auto"} height={height || "auto"} />
      </div>
    )
  }
}

export default ImageWidget;
