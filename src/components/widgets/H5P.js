import React from 'react';

class H5PWidget extends React.PureComponent {
  render() {
    return (
      <div dangerouslySetInnerHTML={{__html: this.props.content}}></div>
    );
  }
}

export default H5PWidget;
