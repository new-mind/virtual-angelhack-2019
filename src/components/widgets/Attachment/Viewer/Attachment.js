import React from 'react';
import * as _ from 'lodash';
import { Nav } from 'react-bootstrap';
import { MdAttachFile as AttachIcon } from 'react-icons/md';
import { Storage } from 'aws-amplify';


class AttachmentWidget extends React.Component {
  render() {
    return (
      <React.Fragment>
        { this.props.title ? <h3>{this.props.title}</h3> : null }
        {
          _.map(this.props.items, item => (
            <Attachment {...item} key={item.id} />
          ))
        }
      </React.Fragment>
    );
  }
}

class Attachment extends React.PureComponent {
  requestDownload = async () => {
    const { fileKey, identityId } = this.props;
    let url;
    if (identityId) {
      url = await Storage.get(fileKey, {level: 'protected', identityId});
    } else {
      url = await Storage.get(fileKey);
    }
    window.open(url, "_blank");
  };

  render() {
    const {title, fileName} = this.props;

    if (!fileName) {
      return null;
    }

    return (
      <Nav.Link onClick={this.requestDownload}>
        <AttachIcon className="mr-3 fa-lg"/>
        {title || fileName}
      </Nav.Link>
    );
  }
}

export default AttachmentWidget;
