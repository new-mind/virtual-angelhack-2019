import React from 'react';
import cn from 'classnames';
import * as _ from 'lodash';
import { v4 as uuid } from 'uuid';

import AttachmentItem from './Item';
import TextEditable from '@lms/components/blocks/TextEditable/TextEditable';

import widgetsStyles from '../../widgets.module.scss';
import styles from './AttachmentEditor.module.scss';

class AttachmentEditor extends React.PureComponent {
  static defaultProps = {
    editableTitle: true
  };

  static get data() {
    return {
      type: "AttachmentWidget",
      title: "Attachment",
      items: [{
        id: uuid()
      }]
    };
  }

  handleChange = _.debounce((index, values) => {
    const { widget, onChange } = this.props;
    let { items } = widget;

    items.splice(index, 1, values);
    //TODO: hack
    _.forEach(items, (item) => delete item.__typename);

    onChange && onChange({
      id: widget.id,
      type: 'AttachmentWidget',
      items: items
    }, widget);
  }, 300, { leading: false, trailing: true });

  addRow = (i) => {
    const { widget, onChange } = this.props;
    let { items } = widget;

    items.push({
      id: uuid()
    });
    //TODO: hack
    _.forEach(items, (item) => delete item.__typename);

    onChange && onChange({
      id: widget.id,
      type: 'AttachmentWidget',
      items
    }, widget);
  };

  removeRow = (i) => {
    const { widget, onChange } = this.props;
    let { items } = widget;

    _.pullAt(items, i);
    //TODO: hack
    _.forEach(items, (item) => delete item.__typename);

    onChange && onChange({
      id: widget.id,
      type: 'AttachmentWidget',
      items
    }, widget);
  };

  handleChangeTitle = (value) => {
    const { widget, onChange } = this.props;

    onChange && onChange({
      id: widget.id,
      type: 'AttachmentWidget',
      title: value || null,
    }, widget);
  };

  handleMoveLeft = (index) => {
    const { props } = this;
    const prev = props.widget.items[index - 1];
    const prevIndex = index - 1;
    const curr = props.widget.items[index];
    const currIndex = index;

    props.widget.items.splice(prevIndex, 1, curr);
    props.widget.items[currIndex] = prev;

    _.forEach(props.widget.items, (item) => delete item.__typename);

    props.onChange && props.onChange({
      id: props.widget.id,
      type: 'AttachmentWidget',
      items: props.widget.items,
    }, props.widget);
  };

  handleMoveRight = (index) => {
    const { props } = this;
    const next = props.widget.items[index + 1];
    const nextIndex = index + 1;
    const curr = props.widget.items[index];
    const currIndex = index;

    props.widget.items.splice(currIndex, 1, next);
    props.widget.items[nextIndex] = curr;

    _.forEach(props.widget.items, (item) => delete item.__typename);

    props.onChange && props.onChange({
      id: props.widget.id,
      type: 'AttachmentWidget',
      items: props.widget.items,
    }, props.widget);
  };

  render() {
    const { widget, collapsed } = this.props;

    return (
      <>
        <h5 className={cn(widgetsStyles.title, 'm-0')}>
          {this.props.editableTitle ? (
            <TextEditable onChange={this.handleChangeTitle}>
              {widget.title}
            </TextEditable>
          ) : (
            widget.title
          )}
        </h5>
        {!collapsed ?
          <div className={cn('my-s3 mx-ns3')}>
            {_.map(widget.items, (item, i) => (
              <AttachmentItem
                {...item}
                first={i === 0}
                last={i === widget.items.length - 1}
                index={i}
                key={item.id}
                lessonId={this.props.lessonId}
                canRemove={widget.items.length > 1}
                onChange={this.handleChange}
                onAddRow={this.addRow}
                onRemoveRow={this.removeRow}
                onMoveLeft={this.handleMoveLeft}
                onMoveRight={this.handleMoveRight} />
            ))}
            <div className={cn(styles.add, 'm-s3')} onClick={this.addRow} />
          </div>
          : null
        }
      </>
    );
  }
}

export default AttachmentEditor;
