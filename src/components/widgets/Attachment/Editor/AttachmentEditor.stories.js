import React from 'react';
import { storiesOf } from '@storybook/react';

import AttachmentEditor from './index';

storiesOf('Widgets|Attachment', module)
  .addDecorator(storyFn => <div className="p-xxl">{storyFn()}</div>)
  .add('Editor', () => (
    <>
      <h1>Attachment Editor</h1>
      <AttachmentEditor widget={AttachmentEditor.data} />
    </>
  ));
