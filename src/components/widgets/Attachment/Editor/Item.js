import React from 'react';
import { Auth } from 'aws-amplify';
import Uploader from 'react-images-upload';
import * as _ from 'lodash';
import cn from 'classnames';
import { Storage } from 'aws-amplify';

import Loader from '@lms/components/loader/Loader';
import styles from './AttachmentEditor.module.scss';

class Item extends React.Component {
  state = {
    loading: false,
    style: {},
  };

  handleFileChange = async (file, base64) => {
    const { index, id, lessonId, title, onChange } = this.props;
    this.setState({ loading: true });

    file = file[0];
    if (!file) {
      this.setState({ loading: false });
      return;
    }

    let match = /(\.[\w\d]*?)$/.exec(file.name);
    let ext = '';
    if (match) { ext = match[1]; }
    const { key } = await Storage.put(`${lessonId}/${id}${ext}`, file, {
      contentType: file.type,
      level: 'protected'
    });
    const { identityId } = await Auth.currentCredentials();

    onChange && onChange(index, {
      id,
      title,
      fileKey: key,
      fileName: file.name,
      identityId
    });

    this.setState({ loading: false });
  };

  handleTitleChange = (e) => {
    const value = e.target.value;
    const { index, id, fileKey, fileName, identityId, onChange } = this.props;

    onChange && onChange(index, {
      id,
      title: value || null,
      fileKey,
      fileName,
      identityId
    });

    this.setState({
      style: {
        minHeight: e.target.scrollHeight + 2,
        ...(e.target.scrollHeight < 96 ?
            {
              borderTop: '1px solid transparent',
              borderBottom: '1px solid transparent',
            } : {}
        )
      },
    });
  };

  handleTitleFocus = (e) => {
    this.setState({
      style: {
        minHeight: e.target.scrollHeight + 2,
        ...(e.target.scrollHeight < 96 ?
            {
              borderTop: '1px solid transparent',
              borderBottom: '1px solid transparent',
            } : {}
        )
      },
    });
  };

  handleTitleBlur = (e) => {
    this.setState({
      style: {
        height: '100%',
      },
    });
  };

  render() {
    const { index, canRemove, title, id, first, last } = this.props;
    return (
      <div className={cn(styles.attachment, 'm-s3 mb-s3')} key={id}>
        <div className={styles.card}>
          <div className={styles.uploader}>
            {this.renderUploader()}
          </div>
          <h5 className={cn(styles.title, 'my-s3')}>
            <textarea style={this.state.style} onChange={this.handleTitleChange} onFocus={this.handleTitleFocus} onBlur={this.handleTitleBlur} placeholder="Enter Title" defaultValue={title} />
          </h5>
        </div>
        <div className={styles.controls}>
          {!first ? <div className={styles.left} onClick={_.partial(this.props.onMoveLeft, index)}/> : null}
          {!last ? <div className={styles.right} onClick={_.partial(this.props.onMoveRight, index)}/> : null}
          {canRemove ? <div className={styles.remove} onClick={_.partial(this.props.onRemoveRow, index)} /> : null }
        </div>
      </div>
    );
  }

  renderUploader() {
    if (this.state.loading) return <Loader/>
    let title = "Upload file";
    if (this.props.fileName) {
      title = _.truncate(this.props.fileName, {length: 20});
    }

    return (
      <Uploader
        className={cn({ hasFile: this.props.fileName })}
        accept="*"
        withIcon={false}
        withLabel={false}
        singleImage={true}
        buttonText={title}
        buttonClassName={styles.uploader}
        imgExtension={['.*']}
        maxFileSize={52428800}
        onChange={this.handleFileChange}
        />
    );
  }
}

export default Item;
