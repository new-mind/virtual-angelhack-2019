import React from 'react';
import { Alert } from 'react-bootstrap';
import * as _ from 'lodash';

import AttachmentWidget from './Attachment/Viewer/Attachment';
import VideoWidget from './Video/Viewer';
import TextWidget from './Text';
import LinkWidget from './Link/Viewer';
import MultiChoiceWidget from './MultiChoice';
import ImageWidget from './Image';
import ResponseForm from './ResponseForm';

class Widget extends React.PureComponent {
  getWidget(widget) {
    switch (widget.__typename) {
      case 'AttachmentWidget':
        return <AttachmentWidget {...widget}/>;
      case 'VideoWidget':
        return <VideoWidget {...widget}/>;
      case 'TextWidget':
        return <TextWidget {...widget}/>;
      case 'ImageWidget':
        return <ImageWidget {...widget}/>;
      case 'LinkWidget':
        return <LinkWidget {...widget}/>;
      case 'MultiChoiceWidget':
        return <MultiChoiceWidget {...widget}/>;
      case 'ResponseFormWidget':
        return <ResponseForm {...widget} onChange={this.props.onChange} mode={this.props.mode}/>;
      default:
        return (
          <Alert variant="warning">
            <Alert.Heading>{_.upperCase(widget.__typename)} Preview is not implemented yet</Alert.Heading>
          </Alert>
        );
    }
  }

  render() {
    return (
      <div className="my-5">
        { this.getWidget(this.props) }
      </div>
    )
  }
}

export default Widget;
