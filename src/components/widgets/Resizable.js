import React from 'react';
import cn from 'classnames';
import * as _ from 'lodash';
import Input from '@lms/components/blocks/Input/Input';

import styles from './Resizable.module.scss';

class Resizable extends React.PureComponent {
  onWidthChange = _.debounce((value) => {
    const { widget, onChange } = this.props;

    onChange && onChange({
      id: widget.id,
      type: widget.__typename,
      width: value || '600',
      height: ((value || 600) * 0.5652).toString(),
    }, widget);
  }, 300, {leading: false, trailing: true});

  onHeightChange = _.debounce((value) => {
    const { widget, onChange } = this.props;

    onChange && onChange({
      id: widget.id,
      type: widget.__typename,
      height: value
    }, widget);
  }, 300, {leading: false, trailing: true});

  render() {
    const { id, width } = this.props.widget;

    return (
      <>
        {this.props.children}
        <label className={cn(styles.label, 'mb-m3')}>
          <Input
            className={cn(styles.input, 'mr-s2')}
            name={`${id}-width`}
            onChange={(e) => this.onWidthChange(e.target.value)}
            defaultValue={width}
            placeholder={600}
            size={4}
          />
          Type The Width
        </label>
      </>
    );
  }
}

export default Resizable;
