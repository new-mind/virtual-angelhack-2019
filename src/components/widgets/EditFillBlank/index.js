import React from 'react';

class EditFillBlank extends React.Component {
  static get data() {
    return {
      type: 'FillBlankWidget',
      title: 'Link',
    };
  }

  render() {
    return (
      <div>Editor for Fill the Blank Widget</div>
    );
  }
}

export default EditFillBlank;
