import React from 'react';
import { Container, Nav, TabContainer, TabContent, TabPane } from 'react-bootstrap';
import moment from 'moment';
import * as _ from 'lodash';
import { withTranslation } from 'react-i18next';

import './i18n';
import * as props from './props';
import styles from './styles.module.scss';
import EditTextWidget from '../EditText';
import EditLinkWidget from '../Link/Editor';
import EditAttachmentWidget from '../Attachment/Editor';
import TextWidget from '../Text';
import LinkWidget from '../Link/Viewer';
import AttachmentWidget from '../Attachment/Viewer/Attachment';

const MODE = {
  READ: 'READ',
  WRITE: 'WRITE',
};

class ResponseForm extends React.Component {
  static propTypes = props.propTypes;
  static defaultProps = props.defaultProps;

  getWidgetConstructor(mode) {
    const { formType } = this.props;
    switch (formType) {
      case 'TextWidget':
      default:
        return mode === MODE.WRITE ? EditTextWidget : TextWidget;
      case 'LinkWidget':
        return mode === MODE.WRITE ? EditLinkWidget : LinkWidget;
      case 'AttachmentWidget':
        return mode === MODE.WRITE ? EditAttachmentWidget : AttachmentWidget;
    }
  }

  handleChange = (data, section) => {
    const { onChange } = this.props;
    onChange && onChange({
      ...data,
      id: this.props.id,
    });
  }

  renderWidget(widget, mode) {
    const Widget = this.getWidgetConstructor(mode);
    //TODO: AttachmentWidget and ImageWidget depends on
    const { lessonId } = this.props ;
    if (!widget) {
      widget = _.extend(Widget.data, {
        id: this.props.id,
        title: ""
      });
    }

    //TODO: needs single interface for READ/WRITE widgets
    if (mode === MODE.WRITE) {
      return (
        <Widget
          widget={widget}
          editableTitle={false}
          onChange={this.handleChange}
          lessonId={lessonId}/>
      )
    } else {
      // TODO: *data* for TextWidget, *items* for LinkWidget and AttachmentWidget
      return <Widget data={widget.data} items={widget.items} lessonId={lessonId}/>
    }
  }

  render() {
    const { status, answers, mode } = this.props;

    if (mode === "PREVIEW_WITHOUT_DATA") {
      return this.renderPreview();
    }

    if (_.size(answers) > 1 || !(status === "OPEN" || status === "REOPEN")) {
      return this.renderWithTabs(answers);
    }

    return (
      <Container>
        <h2>{this.props.title}</h2>
        { this.renderWidget(_.first(answers), this.getWidgetMode(0)) }
      </Container>
    );
  }

  renderPreview() {
    const { formType, title, t } = this.props;
    return (
      <Container>
        <h2>{title}</h2>
        <TabContainer defaultActiveKey={0}>
          <Nav variant="tabs" className={styles.tabs}>
            <Nav.Link as={'div'} className={styles.tab} eventKey={0} key={0}>
              {moment().format('DD/MM')}
            </Nav.Link>
          </Nav>
          <TabContent className={styles.content}>
            <TabPane eventKey={0} key={0}>
              { getDescription() }
            </TabPane>
          </TabContent>
        </TabContainer>
      </Container>
    );

    function getDescription() {
      switch (formType) {
        case 'TextWidget':
          return (
            <span dangerouslySetInnerHTML={{__html: t("TextWidget")}}/>
          );
        case 'LinkWidget':
          return (
            <span dangerouslySetInnerHTML={{__html: t("LinkWidget")}}/>
          );
        case 'AttachmentWidget':
          return (
            <span dangerouslySetInnerHTML={{__html: t("AttachmentWidget")}}/>
          );
        default:
          break;
      }
    }
  }

  renderWithTabs(answers) {
    const { t, i18n } = this.props;
    return (
      <Container>
        <h2>{this.props.title}</h2>
        <TabContainer defaultActiveKey={0}>
          <Nav variant="tabs" className={styles.tabs}>
            {_.map(answers, (answer, i) => (
              <Nav.Link as={'div'} className={styles.tab} eventKey={i} key={i}>
                { renderTabTitle(answer, i) }
              </Nav.Link>
            ))}
          </Nav>
          <TabContent className={styles.content}>
            {_.map(answers, (answer, i) => (
              <TabPane eventKey={i} key={i}>
                {this.renderWidget(answer, this.getWidgetMode(i))}
              </TabPane>
            ))}
          </TabContent>
        </TabContainer>
      </Container>
    );

    function renderTabTitle(answer, tabIndex) {
      if (tabIndex === 0) {
        return t("Current answer");
      }
      const time = answer.updatedAt || answer.createdAt;
      const timeStr = time ? `(${moment(time).format('DD/MM')})` : "";
      if (i18n.language === "EN") {
        return `${getOrdinalNumber(tabIndex + 1)} answer ${timeStr}`;
      } else {
        return `${tabIndex + 1} ${t("answer")} ${timeStr}`
      }
    }
  }

  getWidgetMode(tabIndex) {
    const { status } = this.props;

    if (status !== "OPEN" && status !== "REOPEN") {
      return MODE.READ;
    }

    if (tabIndex === 0) {
      return MODE.WRITE;
    }
    return MODE.READ;
  }
}

export default withTranslation('components/widgets/ResponseForm')(ResponseForm);

// @todo refactor localization & internationalization
function getOrdinalNumber(num) {
  switch (true) {
    case num.toString().endsWith('11'):
    case num.toString().endsWith('12'):
    case num.toString().endsWith('13'):
      return `${num}th`;
    case num.toString().endsWith('1'):
      return `${num}st`;
    case num.toString().endsWith('2'):
      return `${num}nd`;
    case num.toString().endsWith('3'):
      return `${num}rd`;
    default:
      return `${num}th`;
  }
}
