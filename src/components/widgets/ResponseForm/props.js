import PropTypes from 'prop-types';

export const propTypes = {
  formType: PropTypes.string.isRequired,
  responses: PropTypes.arrayOf(
    PropTypes.shape({
      createdAt: PropTypes.string,
      content: PropTypes.shape({ // @todo contains oneOf appropriate widgets
        __typename: PropTypes.string,
      }),
    }),
  ),
  response: PropTypes.shape({
    __typename: PropTypes.string,
  }),
};

export const defaultProps = {
  answers: [],
};
