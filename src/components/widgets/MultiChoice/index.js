import * as _ from 'lodash';
import React from 'react';
import convertToHtml from 'draftjs-to-html';
import hoistStatics from 'hoist-non-react-statics';
import { withTranslation } from 'react-i18next';

import ImageUploader from '@lms/components/blocks/ImageUploader';
import { ContentType as H5PContentType } from 'eduway-h5p-lib';
import H5PMultiChoice from 'eduway-h5p-multichoice';
import $ from 'jquery';

import Editor from './Editor';
import './i18n';

class MultiChoice extends React.PureComponent {
  getQuestion() {
    const { question } = this.props;

    try {
      return convertToHtml(JSON.parse(question));
    } catch (e) {
      return null;
    }
  }
  async getContent() {
    const { answers, t, imageKey } = this.props;
    const content = {
      answers: _.map(answers, (answer) => ({
        ...answer,
        tipsAndFeedback: {
          tip: "",
          chosenFeedback: "",
          notChosenFeedback: ""
        }
      })),
      behaviour: {
          enableRetry: true,
          enableSolutionsButton: true,
          singlePoint: true,
          randomAnswers: true,
          showSolutionsRequiresInput: true,
          type: "auto",
          confirmCheckDialog: false,
          confirmRetryDialog: false,
          autoCheck: false,
          passPercentage: 100,
          showScorePoints: true,
          enableCheckButton: true
      },
      UI: {
        showSolutionButton: t("Show solution"),
        tryAgainButton: t("Retry"),
        checkAnswerButton: t("Check"),
        tipsLabel: t("Show tip"),
        scoreBarLabel: t("scoreBarLabel"),
        tipAvailable: "Tip available",
        feedbackAvailable: "Feedback available",
        readFeedback: "Read feedback",
        wrongAnswer: "Wrong answer",
        correctAnswer: "Correct answer",
        shouldCheck: "Should have been checked",
        shouldNotCheck: "Should not have been checked",
        noInput: "Please answer before viewing the solution"
      },
      singlePoint: false,
      question: this.getQuestion(),
      overallFeedback: [
        {
            from: 0,
            to: 0,
            feedback: t("Wrong")
        },
        {
            from: 1,
            to: 99,
            feedback: t("Almost!")
        },
        {
            from: 100,
            to: 100,
            feedback: t("Correct!")
        }
      ]
    };
    if (imageKey) {
      const url = ImageUploader.getUrl(imageKey);
      console.log(url);
      content.media = {
        "type": {
            "params": {
                "contentName": "Image",
                "file": {
                    "path": url,
                    "mime": "image/jpeg",
                    "width": 1588,
                    "height": 458,
                    "copyright": {
                        "license": "U"
                    }
                },
                "alt": "Blackcurrants"
            },
            "library": "H5P.Image 1.1",
            "subContentId": "f4e153ab-9205-422a-bf73-9eb6ab6f559e",
        },
        "disableImageZooming": true
      }
    }

    return content;
  }

  async componentDidMount() {
    const { id } = this.props;

    H5PMultiChoice.prototype = _.extend({},
      H5PContentType(false).prototype,
      H5PMultiChoice.prototype);

    let appId = `multichoice-${id}`;
    let instance = new H5PMultiChoice(await this.getContent(), appId);
    const $attachTo = $(`#${appId}`);
    $attachTo.toggleClass('h5p-standalone', false);
    instance.attach($attachTo);
  }

  render() {
    const { id } = this.props;
    return <div id={`multichoice-${id}`}></div>;
  }
}

MultiChoice.Editor = Editor;

export default hoistStatics(
  withTranslation('widgets/MultiChoice')(MultiChoice),
  MultiChoice
);
