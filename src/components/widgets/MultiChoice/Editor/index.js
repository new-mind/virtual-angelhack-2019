import React from 'react';
import * as _ from 'lodash';
import hoistStatics from 'hoist-non-react-statics';
import { Table, Form } from 'react-bootstrap';
import { withTranslation } from 'react-i18next';
import { v4 as uuid } from 'uuid';
import { Formik } from 'formik';
import { Storage } from 'aws-amplify';

import ImageUploader from '@lms/components/blocks/ImageUploader';
import TextEditor from '@lms/components/TextEditor';
import EditableText from '@lms/components/blocks/TextEditable/TextEditable.js';
import Item from './Item';

import styles from './styles.module.scss';
import './i18n';

class MultiChoiceEditor extends React.Component {
  static get data() {
    return {
      type: 'MultiChoiceWidget',
      title: null,
      answers: [
        { id: uuid() },
        { id: uuid() }
      ]
    }
  }

  constructor(props) {
    super(props);
    const { widget: {question} } = props;

    this.state = {
      question: question ? JSON.parse(question) : null
    }
  }

  addRow = (i) => {
    const { widget, onChange } = this.props;
    let { answers } = widget;
    answers.splice(i + 1, 0, {
      id: uuid()
    });
    //TODO: hack
    _.forEach(answers, (answer) => delete answer.__typename);

    onChange && onChange({
      id: widget.id,
      type: 'MultiChoiceWidget',
      answers
    }, widget);
  };

  removeRow = (i) => {
    const { widget, onChange } = this.props;
    let { answers } = widget;
    _.pullAt(answers, i);
    //TODO: hack
    _.forEach(answers, (answer) => delete answer.__typename);

    onChange && onChange({
      id: widget.id,
      type: 'MultiChoiceWidget',
      answers
    }, widget);
  };

  handleChangeAnswers = _.debounce((values) => {
    const { widget, onChange } = this.props;
    const { answers } = widget;
    _.forIn(values, (value, key) => {
      const [i, name] = key.split('-');
      answers[i][name] = value;
    });
    //TODO: hack
    _.forEach(answers, (answer) => delete answer.__typename);

    onChange && onChange({
      id: widget.id,
      type: 'MultiChoiceWidget',
      answers
    }, widget);
  }, 1000, {leading: false, trailing: true});

  handleChange = _.debounce((content) => {
    const { widget, onChange } = this.props;

    const question = JSON.stringify(content);
    if (question === this._cache) {
      return;
    }
    this._cache = question;
    onChange && onChange({
      id: widget.id,
      type: 'MultiChoiceWidget',
      question
    }, widget);
  }, 1000, {leading: false, trailing: true});

  handleChangeTitle = (value) => {
    let { widget, onChange } = this.props;

    onChange && onChange({
      id: widget.id,
      type: 'MultiChoiceWidget',
      title: value
    }, widget);
  };

  handleImagePick = async (file) => {
    const { widget, onChange, lessonId } = this.props;

    let match = /(\.[\w\d]*?)$/.exec(file.name);
    let ext = '';
    if (match) { ext = match[1]; }
    const {key}= await Storage.put(`${lessonId}/${uuid()}${ext}`, file, {
      contentType: file.type,
      ACL: 'public-read',
      level: 'public'
    });
    onChange && onChange({
      id: widget.id,
      type: 'MultiChoiceWidget',
      imageKey: key
    }, widget);
  };

  render() {
    const { t, widget, collapsed } = this.props;
    const { title, imageKey } = widget;
    const imageUrl = imageKey ? ImageUploader.getUrl(imageKey) : null;

    return (
      <div>
        <h3><EditableText onChange={this.handleChangeTitle}>{title}</EditableText></h3>
        { !collapsed ?
          <>
            <div className={styles.content}>
              <div className="mb-3">
                <ImageUploader
                  imageUrl={imageUrl}
                  onPick={this.handleImagePick} />
              </div>
              <TextEditor
                contentState={this.state.question}
                onChange={this.handleChange}
                wrapperClassName={styles.textEditorWrapper}
                />
            </div>
            <Formik onSubmit={this.handleChangeAnswers}>
              {({
                handleChange,
                submitForm,
              }) => {
                return (
                  <Form>
                    <Table>
                      <thead className={styles.tableHeader}>
                        <tr>
                          <th>
                          {t("Answer?")}
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        {_.map(widget.answers, (item, i) => (
                          <Item
                            {...item}
                            widgetId={widget.id}
                            key={item.id}
                            index={i}
                            onAddRow={this.addRow}
                            onRemoveRow={this.removeRow}
                            onChange={(e) => { handleChange(e); setTimeout(submitForm);}} // TODO setTimeout?
                            canRemove={widget.answers.length > 2}
                            />
                        ))}
                      </tbody>
                    </Table>
                  </Form>
                );
              }}
            </Formik>
          </>
          : null
        }
      </div>
    );
  }
}

export default hoistStatics(
  withTranslation( 'widgets/MultiChoice/Editor')(MultiChoiceEditor),
  MultiChoiceEditor
);
