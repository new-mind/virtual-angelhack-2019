import React from 'react';
import * as _ from 'lodash';
import { withTranslation } from 'react-i18next';
import { Col, Form } from 'react-bootstrap';
import Button from '@lms/components/blocks/Button'

import styles from './styles.module.scss';
import './i18n';

class MultiChoiceItem extends React.PureComponent {
  render() {
    const { t, canRemove, index, correct, text, widgetId } = this.props;

    return (
      <tr className={styles.item}>
        <td className={styles.checkbox}>
          <Form.Check
            custom
            inline
            type="checkbox"
            defaultChecked={correct}
            label=""
            onChange={this.props.onChange}
            name={`${index}-correct`}
            id={`${widgetId}-multichoice_item_checkbox-${index}`}
          />
        </td>
        <td className={styles.variant}>
          <Form.Control
            type="text"
            placeholder={t("Enter variant")}
            defaultValue={text}
            name={`${index}-text`}
            onChange={this.props.onChange}
          />
        </td>
        <td>
          <Col className="align-center">
          </Col>
          <Col>
            <Button className="mr-3" onClick={_.partial(this.props.onAddRow, index)}>+</Button>
            {canRemove ? <Button.Confirm className="transparent" confirm="X" onClick={_.partial(this.props.onRemoveRow, index)}>-</Button.Confirm> : null }
          </Col>
        </td>
      </tr>
    )
  }
}

export default withTranslation(
  'widgets/MultiChoice/Editor'
)(MultiChoiceItem);
