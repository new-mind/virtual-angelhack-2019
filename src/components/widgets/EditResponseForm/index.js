import React from 'react';
import { Container, Nav, TabContainer, TabContent, TabPane } from 'react-bootstrap';
import * as _ from 'lodash';
import { withTranslation } from 'react-i18next';
import hoistStatics from 'hoist-non-react-statics';

import * as props from './props';
import TextEditable from '@lms/components/blocks/TextEditable/TextEditable';

import './i18n';
import styles from './styles.module.scss';

class ResponseForm extends React.Component {
  static propTypes = props.propTypes;
  static defaultProps = props.defaultProps;

  static get data() {
    return {
      type: "ResponseFormWidget",
      title: "Response Form",
      formType: "TextWidget"
    };
  }

  handleChange = _.debounce((type) => {
    const { widget, onChange } = this.props;
    onChange && onChange({
      id: widget.id,
      type: 'ResponseFormWidget',
      formType: type
    }, widget);
  }, 1000, {leading: false, trailing: true});

  handleChangeTitle = (value) => {
    let { widget, onChange } = this.props;

    onChange && onChange({
      id: widget.id,
      type: 'ResponseFormWidget',
      title: value,
    }, widget);
  }

  render() {
    const { widget, t, collapsed } = this.props;

    return (
      <Container className="mb-3 px-0">
        <h3><TextEditable onChange={this.handleChangeTitle}>{widget.title}</TextEditable></h3>
        { !collapsed ?
          <TabContainer defaultActiveKey={widget.formType} onSelect={this.handleChange}>
            <Nav variant="tabs" className={styles.tabs}>
              <Nav.Link as={'div'} className={styles.tab} eventKey="TextWidget">{t("Text")}</Nav.Link>
              <Nav.Link as={'div'} className={styles.tab} eventKey="LinkWidget">{t("Link")}</Nav.Link>
              <Nav.Link as={'div'} className={styles.tab} eventKey="AttachmentWidget">{t("Attachment")}</Nav.Link>
            </Nav>
            <TabContent className={styles.content}>
              <TabPane eventKey="TextWidget" dangerouslySetInnerHTML={{__html: t("TextWidget")}}/>
              <TabPane eventKey="LinkWidget" dangerouslySetInnerHTML={{__html: t("LinkWidget")}}/>
              <TabPane eventKey="AttachmentWidget" dangerouslySetInnerHTML={{__html: t("AttachmentWidget")}}/>
            </TabContent>
          </TabContainer>
          : null
        }
      </Container>
    );
  }
}

export default hoistStatics(
  withTranslation('components/widgets/EditResponseForm')(ResponseForm),
  ResponseForm
)
