import PropTypes from 'prop-types';

export const propTypes = {
  widget: PropTypes.shape({
    id: PropTypes.string.isRequired,
    formType: PropTypes.string.isRequired,
    title: PropTypes.string,
  }).isRequired,
};

export const defaultProps = {};
