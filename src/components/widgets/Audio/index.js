import React from 'react';
import PropTypes from 'prop-types';
import ReactPlayer from 'react-player';

import styles from './Audio.module.scss';

// @todo implement custom Player component https://github.com/CookPete/react-player#adding-custom-players
const Player = ReactPlayer;

class Audio extends React.Component {
  static propTypes = {
    src: PropTypes.string.isRequired,
  };

  render() {
    return (
      <Player
        className={styles.widget}
        url={this.props.src}
        controls={true}
        height={56}
      />
    );
  }
}

export default Audio;
