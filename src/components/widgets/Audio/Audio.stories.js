import React from 'react';
import uuid from 'uuid/v4';
import { storiesOf } from '@storybook/react';

import AudioViewer from './index';
import AudioEditor from '../EditAudio';

const AudioEditorHOC = ({ widget: _widget, ...props }) => {
  const [ widget, onChange ] = React.useState(_widget);

  return <AudioEditor {...props} widget={widget} onChange={onChange} />
};

storiesOf('Widgets|Audio', module)
  .addDecorator(storyFn => <div className="p-xxl">{storyFn()}</div>)
  .add('Editor', () => (
    <>
      <h1>Audio Editor</h1>
      <AudioEditorHOC
        widget={{
          type: 'AudioWidget',
          title: 'Audio Record',
          src: {
            id: uuid(),
          }
        }}
        lessonId='12345'
      />
    </>
  ))
  .add('Viewer', () => (
    <>
      <h1>Audio Viewer</h1>
      <AudioViewer src='/audio.mp3' />
    </>
  ))
  .add('In Conjunction', () => (
    <>
      <h1>Audio Editor</h1>
      <AudioEditorHOC
        widget={{
          type: 'AudioWidget',
          title: 'Audio Record',
          src: {
            id: uuid(),
          }
        }}
        lessonId='12345'
      />
      <hr />
      <h1>Audio Viewer</h1>
      <AudioViewer src='/audio.mp3' />
    </>
  ));

