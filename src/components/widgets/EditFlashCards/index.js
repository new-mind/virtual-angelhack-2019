import React from 'react';

class EditFlashCards extends React.Component {
  static get data() {
    return {
      type: 'FlashCardsWidget',
      title: 'Link',
    };
  }

  render() {
    return (
      <div>Editor for Flash Cards Widget</div>
    );
  }
}

export default EditFlashCards;
