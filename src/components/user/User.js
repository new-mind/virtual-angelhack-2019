import React, { Component } from 'react'
import { Breadcrumb, Container } from 'react-bootstrap';
import qs from 'qs';
import { NavLink } from 'react-router-dom';

export default class User extends Component {
  render() {
    const { type } = qs.parse(this.props.location.search, { ignoreQueryPrefix: true });

    return (
      <Container>
        <Breadcrumb>
          <Breadcrumb.Item>
            <NavLink to="/users">Users</NavLink>
          </Breadcrumb.Item>
          <Breadcrumb.Item>
            {type}
          </Breadcrumb.Item>
        </Breadcrumb>
        <div>
          Here will be {type}
        </div>
      </Container>
    );
  }
}
