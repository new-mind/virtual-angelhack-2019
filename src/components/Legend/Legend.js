import React from 'react';
import cn from 'classnames';

import styles from './styles.module.scss';

class Legend extends React.Component {
  render() {
    return (
      <section className={cn(styles.legend, 'd-flex flex-row align-items-center')}>
        {this.props.items.map((item, index) => (
          <div className={cn('mx-2 text-left d-flex flex-row align-items-center')} key={index}>
            <img src={item.icon} alt={item.label} className="mr-2" />
            {item.label}
          </div>
        ))}
      </section>
    );
  }
}

export default Legend;
