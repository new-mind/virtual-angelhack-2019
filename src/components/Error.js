import React from 'react';
import { Alert } from 'react-bootstrap';

export default class Error extends React.PureComponent {
  render() {
    return <Alert variant="danger">
      <Alert.Heading>GraphQL Error</Alert.Heading>
      <hr/>
      <div><pre>{this.props.message}</pre></div>
    </Alert>
  }
}
