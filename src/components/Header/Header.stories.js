import React from 'react';
import { storiesOf } from '@storybook/react';
import StoryRouter from 'storybook-react-router';

import Header from './index';

storiesOf('Layout|Header', module)
  .addDecorator(StoryRouter())
  .addDecorator((storyFn) => <div className="p-xxl">{storyFn()}</div>)
  .add('Default', () => (
    <Header />
  ));
