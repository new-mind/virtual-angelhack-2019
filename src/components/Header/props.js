import PropTypes from 'prop-types';

export const propTypes = {
  inverse: PropTypes.bool,
  fluid: PropTypes.bool,
  items: PropTypes.arrayOf(
    PropTypes.shape({
      url: PropTypes.string,
      title: PropTypes.string,
    }),
  ),
  avatar: PropTypes.element,
  logo: PropTypes.bool,
};

export const defaultProps = {
  inverse: false,
  fluid: false,
  items: [],
  avatar: null,
  logo: true,
};
