import React, { Component } from 'react';
import { Container, Nav } from 'react-bootstrap';
import cn from 'classnames'
import { withTranslation } from 'react-i18next';
import { NavLink } from 'react-router-dom';

import Logout from '@lms/components/blocks/Logout';

import styles from './styles.module.scss';

import * as props from './props';

import './i18n';

class Header extends Component {
  static propTypes = props.propTypes;
  static defaultProps = props.defaultProps;

  state = {
    isMobileMenuVisible: false,
  };

  render() {
    return (
      <Container fluid className={cn(styles.layer, this.props.className, 'p-0')}>
        <Container className="p-0">
          <header className={styles.header}>
            {this.props.logo && <NavLink to="/courses" className={cn(styles.logo, 'pl-s2 pr-xl')}>EduWay</NavLink>}
            <div className={styles.menu}>
              <Nav>
                {this.props.items.map((item, i) => (
                  <NavLink to={item.url} key={i} className="nav-link px-m2">{item.title}</NavLink>
                ))}
              </Nav>
            </div>
            <div className={cn(styles.mobileMenu, 'ml-auto')}>
              <div
                className={styles.mobileMenuButton}
                onClick={() => this.setState({ isMobileMenuVisible: !this.state.isMobileMenuVisible })}
              />
              <div
                className={cn('mobileMenuContent', !this.state.isMobileMenuVisible && styles.hidden)}
                onClick={() => this.setState({ isMobileMenuVisible: !this.state.isMobileMenuVisible })}
              >
                <svg className="close" width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path
                    d="M11.6516 11.6516C11.1871 12.1161 10.4129 12.1161 9.94839 11.6516L6 7.70324L2.05161 11.6516C1.58711 12.1161 0.812875 12.1161 0.348375 11.6516C-0.116125 11.1871 -0.116125 10.4129 0.348375 9.94839L4.29676 6L0.348375 2.05161C-0.116125 1.58711 -0.116125 0.812875 0.348375 0.348375C0.812875 -0.116125 1.58711 -0.116125 2.05161 0.348375L6 4.29676L9.94839 0.348375C10.4129 -0.116125 11.1871 -0.116125 11.6516 0.348375C12.1161 0.812875 12.1161 1.58711 11.6516 2.05161L7.70324 6L11.6516 9.94839C12.1161 10.4129 12.1161 11.1871 11.6516 11.6516Z"
                  />
                </svg>
                <Nav>
                  {this.props.items.map((item, i) => (
                    <NavLink to={item.url} key={i} className="nav-link p-0">{item.title}</NavLink>
                  ))}
                  <div className="nav-link p-0">
                    <Logout>
                      {this.props.t('Log out')}
                    </Logout>
                  </div>
                </Nav>
              </div>
            </div>
            <div className={styles.avatar}>
              {this.props.avatar}
            </div>
          </header>
        </Container>
      </Container>
    );
  }
}

export default withTranslation('header')(Header);
