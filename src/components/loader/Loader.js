import React, { Component } from 'react';
import { Container, Spinner } from 'react-bootstrap';
import cn from 'classnames';

export default class Loader extends Component {
  render() {
    return (
      <Container className={cn("h-100 d-flex align-items-center justify-content-center", this.props.className)} fluid>
        <Spinner animation="border" />
      </Container>
    );
  }
}
