import * as React from 'react';
import cn from 'classnames';
import { I18n } from 'aws-amplify';
import { ForgotPassword } from 'aws-amplify-react';

import Button from '@lms/components/blocks/Button';
import Input from '@lms/components/blocks/Input/Input';

import styles from '@lms/components/forms/formsCommon.module.scss';

class ResetPasswordForm extends ForgotPassword {
  constructor(props) {
    super(props);
    this._validAuthStates = ['forgotPassword'];
  }

  handleSend = () => {
    this.inputs.username = this.inputs.username && this.inputs.username.trim().toLowerCase();
    super.send();
  };

  handleSubmit = () => {
    super.submit();
  };

  submitView() {
    return (
      <div className="d-flex flex-column">
        {/* Hack for old native android browser to cancel auto completion */}
        <Input autoComplete="username" className={styles.hidden} />
        <Input autoComplete="password" className={styles.hidden} />
        <Input
          placeholder={I18n.get('code')}
          className="mb-s3"
          key="code"
          name="code"
          autoComplete="off"
          onChange={this.handleInputChange}
        />
        <Input
          placeholder={I18n.get('newPassword')}
          type="password"
          key="password"
          name="password"
          autoComplete="off"
          onChange={this.handleInputChange}
        />
      </div>
    );
  }

  sendView() {
    return (
      <>
        <Input
          id="username"
          autoFocus
          placeholder={I18n.get('email')}
          key="username"
          name="username"
          onChange={this.handleInputChange}
        />
      </>
    );
  }

  showComponent(theme) {
    const { authData = {} } = this.props;

    return (
      <div className={cn(styles.background, 'mx-auto w-full max-w-xs')}>
        <form
          className={cn(styles.form, 'w-auto')}
          autoComplete="off"
          onSubmit={(e) => e.preventDefault()}
        >
          <h1 className={cn(styles.title, 'mb-xl')}>{I18n.get('resetPassword')}</h1>
          { this.state.delivery || authData.username ? this.submitView() : this.sendView() }
          <div className="d-flex mt-l2">
            { this.state.delivery || authData.username ? (
              <>
                <Button link className="mr-auto" onClick={this.handleSend}>{I18n.get('resendCode')}</Button>
                <Button className="ml-l2" onClick={this.handleSubmit}>{I18n.get('submit')}</Button>
              </>
            ) : (
              <>
                <Button link className="mr-auto" onClick={() => super.changeState('signIn')}>
                  {I18n.get('signIn')}
                </Button>
                <Button onClick={this.handleSend}>{I18n.get('sendCode')}</Button>
              </>
            ) }
          </div>
        </form>
      </div>
    );
  }
}

export default ResetPasswordForm;
