import React from 'react';
import cn from 'classnames';
import _ from 'lodash';
import { I18n } from 'aws-amplify';
import { SignIn } from "aws-amplify-react";
import { Spinner } from 'react-bootstrap';

import Button from '@lms/components/blocks/Button';
import Input from '@lms/components/blocks/Input/Input';

import styles from '../formsCommon.module.scss';

class SignInForm extends SignIn {
  constructor(props) {
    super(props);
    this._validAuthStates = ["signIn", "signedOut", "signedUp"];
  }

  handleSignIn = (e) => {
    this.inputs.username = this.inputs.username && this.inputs.username.trim().toLowerCase();
    super.signIn(e);
  };

  showComponent(theme) {
    const loading = _.get(this.state, 'loading', false);

    return (
      <div className={cn(styles.background, 'mx-auto w-full max-w-xs')}>
        <form className={styles.form} onSubmit={this.handleSignIn}>
          <h1 className={cn(styles.title, 'mb-xl')}>EduWay</h1>
            <Input
              className={cn(styles.input, 'mb-m3')}
              id="username"
              key="username"
              name="username"
              onChange={this.handleInputChange}
              type="text"
              placeholder={I18n.get('email')}
              disabled={loading}
            />
            <Input
              className={cn(styles.input, 'mb-l2')}
              id="password"
              key="password"
              name="password"
              onChange={this.handleInputChange}
              type="password"
              placeholder={I18n.get('password')}
              disabled={loading}
            />
            <Button
              className="w-100 mb-l2 text-uppercase position-relative"
              type="button"
              onClick={this.handleSignIn}
              disabled={loading}
            >
              {loading ? (
                <div className="d-flex align-items-center justify-content-center">
                  <Spinner
                    as="span"
                    animation="grow"
                    size="sm"
                    role="status"
                    aria-hidden="true"
                    className='mr-sm-2'
                  />
                  {I18n.get('loading')}
                </div>
              ) : (
                <>
                  {I18n.get('signIn')}
                </>
              )}
            </Button>
            <p className="d-flex flex-row">
              <Button className="text-left" link onClick={() => super.changeState("signUp")}>
                {I18n.get('signUp')}
              </Button>
              <Button className="ml-auto text-right" link onClick={() => super.changeState("forgotPassword")}>
                {I18n.get('forgotPassword')}
              </Button>
            </p>
        </form>
      </div>
    );
  }
}

export default SignInForm;
