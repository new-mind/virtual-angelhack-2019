import React from 'react';
import cn from 'classnames';
import _ from 'lodash';
import { I18n } from 'aws-amplify';
import { SignUp } from "aws-amplify-react";
import { Form, Spinner } from 'react-bootstrap';

import Button from '@lms/components/blocks/Button';

import styles from '../formsCommon.module.scss';

class SignUpForm extends SignUp {
  constructor(props) {
    super(props);
    this._validAuthStates = ['signUp'];
    this.signUpFields = [{
      label: 'School Name',
      key: 'schoolName',
      required: true,
      placeholder: 'School Name',
    }, {
      label: 'Username',
      key: 'username',
      required: true,
      placeholder: 'Username',
    }, {
      label: 'Email',
      key: 'email',
      required: true,
      placeholder: 'Email',
      type: 'email',
    }, {
      label: 'Password',
      key: 'password',
      required: true,
      placeholder: 'Password',
      type: 'password',
    }, {
      label: 'Confirm Password',
      key: 'confirmPassword',
      required: true,
      placeholder: 'Confirm Password',
      type: 'password',
    }];
  }

  validate() {
    const invalids = [];
    this.signUpFields.forEach((el) => {
      if (el.required && !this.inputs[el.key]) {
        el.invalid = true;
        invalids.push(el.label);
      } else {
        el.invalid = false;
      }
    });
    return invalids;
  }

  handleSignUp = (e) => {
    e.preventDefault();

    this.inputs.username = this.inputs.username && this.inputs.username.trim().toLowerCase();
    this.inputs.email = this.inputs.email && this.inputs.email.trim().toLowerCase();

    if (this.inputs.password !== this.inputs.confirmPassword) {
      return super.error('Password mismatch');
    }

    super.signUp();
  };

  showComponent(theme) {
    const loading = _.get(this.state, 'loading', false);

    return (
      <div className={cn(styles.background, 'mx-auto w-full max-w-xs')}>
        <form className={styles.form} onSubmit={this.handleSignUp}>
          <h1 className={cn(styles.title, 'mb-xl')}>EduWay</h1>
          <h3 className={cn('mb-xl text-center')}>{I18n.get('title')}</h3>
          <Form.Control
            className={cn(styles.input, 'mb-m3')}
            id="schoolName"
            key="schoolName"
            name="schoolName"
            onChange={this.handleInputChange}
            type="text"
            placeholder={I18n.get('school')}
            required
          />
          <Form.Control
            className={cn(styles.input, 'mb-m3')}
            id="username"
            key="username"
            name="username"
            onChange={this.handleInputChange}
            type="text"
            placeholder={I18n.get('name')}
            required
          />
          <Form.Control
            className={cn(styles.input, 'mb-m3')}
            id="email"
            key="email"
            name="email"
            onChange={this.handleInputChange}
            type="text"
            placeholder={I18n.get('email')}
            required
          />
          <Form.Control
            className={cn(styles.input, 'mb-m3')}
            id="password"
            key="password"
            name="password"
            onChange={this.handleInputChange}
            type="password"
            placeholder={I18n.get('password')}
            required
          />
          <Form.Control
            className={cn(styles.input, 'mb-l2')}
            id="confirmPassword"
            key="confirmPassword"
            name="confirmPassword"
            onChange={this.handleInputChange}
            type="password"
            placeholder={I18n.get('confirmPassword')}
            required
          />
          <Button
            className="w-100 mb-l2 text-uppercase position-relative"
            type="button"
            onClick={this.handleSignUp}
            disabled={loading}
          >
            {loading ? (
              <>
                <Spinner
                  as="span"
                  animation="grow"
                  size="sm"
                  role="status"
                  aria-hidden="true"
                  className='mr-sm-1'
                />
                {I18n.get('loading')}
              </>
            ) : (
              <>
                {I18n.get('signUp')}
              </>
            )}
          </Button>
          <p className="d-flex flex-row">
            <Button link onClick={() => super.changeState("signIn")}>
              {I18n.get('signIn')}
            </Button>
            <Button className="ml-auto" link>
              {I18n.get('terms')}
            </Button>
          </p>
        </form>
      </div>
    );
  }
}

export default SignUpForm;
