import React from 'react';
import { ListGroup } from 'react-bootstrap';
import cn from 'classnames';
import styles from './styles.module.scss'

const List = (props) => {
  return (
    <ListGroup className={cn(props.className, 'mb-3')}>
      {props.children}
    </ListGroup>
  );
};

const Item = (props) => {
  return (
    <ListGroup.Item className={cn(props.className, styles.item)}>
      {props.children}
    </ListGroup.Item>
  );
};

List.Item = Item;

export default List;
