import React from 'react';
import { storiesOf } from '@storybook/react';
import image from './avatar.jpg'

import DefaultBadge from './index';

storiesOf('Blocks|User Badge', module)
  .addDecorator(storyFn => <div className="p-xxl">{storyFn()}</div>)
  .add('Default', () => (
    <>
      <h1>User Badge</h1>
      <p>
        <small>
          <a
            href="https://www.figma.com/file/ZWpMW2yTyrLht0WfdKJJzj5u/Design?node-id=604%3A130"
            target="_blank"
            rel="noopener noreferrer"
          >
            Look Sources in Design
          </a>
        </small>
      </p>
      <DefaultBadge url={image} user_name={"John Maracul"} />
    </>
  ));
