import React from 'react';
import cn from 'classnames';
import styles from './styles.module.scss';

class UserBadge extends React.Component {
  render() {
    return (
      <>
        <img
          className={styles.avatar}
          src={this.props.url}
        />
        <span className={cn(styles.name, 'ml-l1')}>{this.props.user_name}</span>
      </>
    );
  }
}

export default UserBadge;

