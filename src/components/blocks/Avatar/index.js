import React from 'react';
import cn from 'classnames';
import PropTypes from 'prop-types';

import styles from './styles.module.scss';

class Avatar extends React.PureComponent {
  static propTypes = {
    className: PropTypes.string,
    name: PropTypes.string,
    photo: PropTypes.string,
  };

  render() {
    const { props } = this;

    return (
      <div className={cn(styles.avatar, props.className)}>
        {props.photo && <img src={props.photo} alt={props.name} />}
      </div>
    )
  }
}


export default Avatar;
