import React from 'react';
import { storiesOf } from '@storybook/react';

import Avatar from './index';

import avatar from './images/avatar.jpg';

storiesOf('Blocks|Avatar', module)
  .addDecorator(storyFn => (
    <div className="p-xxl">
      <h1>Avatar Without Photo</h1>
      <p>
        <small>
          <a
            href="https://www.figma.com/file/ZWpMW2yTyrLht0WfdKJJzj5u/EduWay-Design?node-id=1618%3A0"
            target="_blank"
            rel="noopener noreferrer"
          >
            Look Sources in Design
          </a>
        </small>
      </p>
      {storyFn()}
    </div>
  ))
  .add('Without photo', () => (
    <Avatar name="Yegor Litvyakov" />
  ))
  .add('With photo', () => (
    <Avatar name="Yegor Litvyakov" photo={avatar} />
  ));
