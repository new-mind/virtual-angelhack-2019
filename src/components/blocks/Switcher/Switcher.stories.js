import React from 'react';

import { storiesOf } from '@storybook/react';

import Switcher from './index';

const StorySwitcher = function () {
  const [value, setValue] = React.useState('ADMIN');

  return <Switcher options={[{ id: 'ADMIN', title: 'Admin' }, { id: 'TEACHER', title: 'Teacher' }]} value={value} onChange={setValue} />
};

storiesOf('Blocks:Switcher', module)
  .add('By default', () => (<div className="p-xxl"><StorySwitcher /></div>));
