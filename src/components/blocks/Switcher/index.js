import React from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import cn from 'classnames';

import styles from './styles.module.scss';

class Switcher extends React.Component {
  static propTypes = {
    options: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      title: PropTypes.string,
    })),
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    onChange: PropTypes.func,
  };

  handleChange = (option) => {
    if (!option || !option.id) return;

    this.props.onChange(option.id);
  };

  render() {
    const { options, value, className } = this.props;

    return (
      <div className={cn(className, 'd-flex align-items-center flex-row')}>
        <div className={styles.label} onClick={() => this.handleChange(options[0])}>{options[0].title}</div>
        <div
          className={cn(styles.toggler, options[0].id === value ? styles.on : styles.off, 'mx-m1')}
          onClick={() => this.handleChange(_.find(options, (opt) => opt.id !== value))}
        />
        <div className={styles.label} onClick={() => this.handleChange(options[1])}>{options[1].title}</div>
      </div>
    );
  }
}

export default Switcher;
