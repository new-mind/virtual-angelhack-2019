import React from 'react';
import { storiesOf } from '@storybook/react';

import Checkbox from './index';

const StoryCheckbox = function (props) {
  const { checked: _checked, ..._props } = props;
  const [checked, setChecked] = React.useState(_checked);

  return <Checkbox checked={checked} onChange={() => setChecked(!checked)} {..._props} />
};

storiesOf('Blocks|Checkbox', module)
  .addDecorator(storyFn => <div className="p-xxl">{storyFn()}</div>)
  .add('Default', () => (
    <>
      <div className="p-s1">
        <StoryCheckbox label="Homework" />
      </div>
      <div className="p-s1">
        <StoryCheckbox label="Homework" checked={true} />
      </div>
    </>
  ));
