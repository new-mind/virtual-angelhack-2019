import React from 'react';
import cn from 'classnames';

import styles from './Checkbox.module.scss';

class Checkbox extends React.Component {
  render() {
    const { props } = this;
    const className=cn(styles.checkbox, props.checked && styles.checked, 'm-0', props.className);

    return (
      <label className={className} onClick={props.onChange}>
        {props.label && (
          <span className={cn(styles.label, 'ml-m2')}>{props.label}</span>
        )}
      </label>
    );
  }
}

export default Checkbox;
