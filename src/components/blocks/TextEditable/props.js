import PropTypes from 'prop-types';

export const propTypes = {
  children: PropTypes.any,
  maxLength: PropTypes.number,
};

export const defaultProps = {
  children: '',
  maxLength: null,
};
