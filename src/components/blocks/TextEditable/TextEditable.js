import React, { Component } from 'react';
import _ from 'lodash';
import ContentEditable from 'react-contenteditable';
import cn from 'classnames';
import * as props from './props';

import styles from './styles.module.scss'

const MODE = {
  VIEW: 'VIEW',
  EDIT: 'EDIT',
};

class TextEditable extends Component {
  static propTypes = props.propTypes;
  static defaultProps = props.defaultProps;

  input = React.createRef();
  state = {
    mode: MODE.VIEW
  };

  get text() {
    const { props, state } = this;

    if (!props.children) return '';

    if (props.maxLength) {
      return state.mode === MODE.VIEW ?
        _.truncate(props.children, { length: props.maxLength }) :
        props.children;
    }

    return props.children;
  }

  handleKeyDown = (e) => {
    switch (e.keyCode) {
      case 13: // Enter
        e.preventDefault();
        this.input.current.blur();
        break;
      case 27: // Escape
        this.input.current.innerText = this.text;
        this.input.current.blur();
        break;
      default:
        break;
    }
  };

  handleFocus = () => {
    this.setState({ mode: MODE.EDIT });
  };

  handleBlur = () => {
    const { onChange } = this.props;
    const text = this.input.current.innerText;

    if (this.text !== text) {
      onChange && onChange(text || null);
    }
    this.setState({ mode: MODE.VIEW });
  };

  handleClick = () => {
    this.input.current.spellcheck = false;
    this.input.current.focus();
    setEndOfContentEditable(this.input.current);
  };

  render() {
    return (
      <div className={cn(styles.container, this.state.mode === MODE.EDIT && styles.focus)}>
        <ContentEditable
          className={cn(styles.input, {[styles.emptyInput]: !this.text})} style={{}}
          tagName={"span"}
          innerRef={this.input}
          html={this.text}
          onKeyDown={this.handleKeyDown}
          onBlur={this.handleBlur}
          onFocus={this.handleFocus}
          onChange={() => {}}
          disabled={false}
          spellCheck={false}
        />
      </div>
    );
  }
}

TextEditable.propTypes = {};

export default TextEditable;

function setEndOfContentEditable(contentEditableElement) {
  let range, selection;

  if(document.createRange) {
    range = document.createRange();
    range.selectNodeContents(contentEditableElement);
    range.collapse(false);
    selection = window.getSelection();
    selection.removeAllRanges();
    selection.addRange(range);
  } else if(document.selection) {
    range = document.body.createTextRange();
    range.moveToElementText(contentEditableElement);
    range.collapse(false);
    range.select();
  }
}
