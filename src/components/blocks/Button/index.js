import ConfirmButton from './Confirm';
import Button from './Button';

Button.Confirm = ConfirmButton;

export default Button;
