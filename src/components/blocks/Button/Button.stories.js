import React from 'react';
import { storiesOf } from '@storybook/react';

import Button from './Button';

storiesOf('Blocks|Button', module)
  .addDecorator(storyFn => <div className="p-xxl">{storyFn()}</div>)
  .add('Default', () => (
    <>
      <div className="m-s3">
        <Button>Edit</Button>
      </div>
      <div className="m-s3">
        <Button transparent>Edit</Button>
      </div>
    </>
  ))
  .add('Small', () => (
    <Button small transparent>Edit</Button>
  ));
