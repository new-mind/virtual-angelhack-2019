import React from 'react';
import Button from './Button';
import cn from 'classnames'

class ConfirmButton extends React.Component {
  state = {
    pressed: false
  }

  handleClick = (...args) => {
    this._timeout && clearTimeout(this._timeout);
    if (!this.state.pressed) {
      this.setState({pressed: true});
      this._timeout = setTimeout(() => {
        this.setState({pressed: false});
      }, 2000);
    } else {
      this.setState({pressed: false});
      this.props.onClick.call(this, args);
    }
  }

  render() {
    const props = this.props;
    const className = cn(props.className, {
      animated: true,
      bounceIn: this.state.pressed
    })


    return (
      <Button {...props} onClick={this.handleClick} className={className}>
        { this.state.pressed ? props.confirm : props.children }
      </Button>
    );
  }
}

export default ConfirmButton;
