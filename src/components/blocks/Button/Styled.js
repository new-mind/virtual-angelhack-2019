import React from 'react';
import { Button as BootstrapButton } from 'react-bootstrap';
import cn from 'classnames';
import * as _ from 'lodash';
import PropTypes from 'prop-types';

import styles from './styles.module.scss';

class Button extends React.PureComponent {
  static propTypes = {
    noHover: PropTypes.bool
  };

  render() {
    const props = _.extend({}, this.props);
    //TODO
    delete props.noHover;

    return (
      <BootstrapButton
        {...props}
        className={
          cn({
              'no_hover': this.props.noHover
            }, styles.button, props.className, props.variant && styles[props.variant])
        } /> /*@todo provide variant bootstrap button prop*/
    );
  }
}

export default Button;
