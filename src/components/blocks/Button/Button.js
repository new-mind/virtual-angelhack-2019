import React from 'react';
import cn from 'classnames';

import styles from './Button.module.scss';

class Button extends React.Component {
  render() {
    const { props } = this;
    const className = cn(props.className, styles.button, props.small && styles.small, props.transparent && styles.transparent, props.link && styles.link);

    return (
      <button className={className} onClick={props.onClick} disabled={props.disabled}>
        {props.children}
      </button>
    );
  }
}

export default Button;
