import { Modal } from 'react-bootstrap';
import styled from 'styled-components';

const StyledModal = styled(Modal)`
  .modal-content {
    border-radius: 0;
    border: 1px solid #979797;  
    box-sizing: border-box;
    box-shadow: 0px 0px 6px rgba(0, 0, 0, 0.2);
    padding-left: 1em;
    padding-right: 1em;
    padding-bottom: 1.5em;
  }
`;

StyledModal.Header = styled(Modal.Header)`
  border: none;
  .close {
    font-size: 2rem;
    opacity: 1;
  }
`;

StyledModal.Footer = styled(Modal.Footer)`
  border: none;
`;

StyledModal.Title = styled(Modal.Title)`
  font-size: 1.875em;
  font-weight: bold;
`;

export default StyledModal;