import React from 'react';
import { storiesOf } from '@storybook/react';

import Input from './Input';

storiesOf('Blocks|Input', module)
  .addDecorator(storyFn => <div className="p-xxl">{storyFn()}</div>)
  .add('Default', () => (
    <>
      <div className="m-s3">
        <Input placeholder="Placeholder" />
      </div>
      <div className="m-s3">
        <Input defaultValue="Input text" />
      </div>
      <div className="m-s3">
        <Input defaultValue="Input text" disabled />
      </div>
    </>
  ));
