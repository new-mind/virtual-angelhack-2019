import React from 'react';
import cn from 'classnames';

import styles from './Input.module.scss';

class Input extends React.Component {
  render() {
    const { className, ...props } = this.props;
    return (
      <input className={cn(className, styles.input)} {...props} />
    );
  }
}

export default Input;
