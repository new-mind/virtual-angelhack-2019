import React from 'react';
import { ReactMultiEmail } from 'react-multi-email/dist';
import cn from 'classnames';

import styles from './styles.module.scss';

class EmailsInput extends React.PureComponent {
  render() {
    const { t, onChange, emails } = this.props;
    return (
      <ReactMultiEmail
        emails={emails}
        className={cn(styles.input, "form-control")}
        placeholder={t("inviteForm.placeholder")}
        onChange={onChange}
        getLabel={(email, index, removeEmail) => (
          <div data-tag key={index}>
            {email}
            <span data-tag-handle onClick={() => removeEmail(index)}>×</span>
          </div>
        )}
      />
    );
  }
}

export default EmailsInput;
