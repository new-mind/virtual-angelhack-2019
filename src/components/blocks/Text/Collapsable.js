import React from 'react';

import styles from './styles.module.scss';
import { Button } from 'react-bootstrap';

class CollapsableText extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      collapsed: true
    }
  }

  render() {
    if (!this.props.enable) {
      return (
        <React.Fragment>
          { this.props.children }
        </React.Fragment>
      )
    }

    if (this.state.collapsed) {
      return (
        <div className={styles.collapsable}>
          <div className={styles.container} onClick={() => { this.setState({collapsed: false}); }}>
            { this.props.collapsed }
          </div>
        </div>
      )
    }

    return (
      <div className={styles.collapsable}>
        <div>
          <Button variant="link" onClick={() => {this.setState({collapsed: true});}}>Collapse</Button>
        </div>
        { this.props.children }
      </div>
    )
  }
}

export default CollapsableText;
