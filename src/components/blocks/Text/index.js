import Collapsable from './Collapsable';
import Editable from '../TextEditable/TextEditable';

const Text = {
  Editable,
  Collapsable
};

export default Text;
