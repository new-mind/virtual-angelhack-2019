import React from 'react';
import cn from 'classnames';

import styles from './styles.module.scss';

class Separator extends React.Component {
  render() {
    const { collapsed, onClick } = this.props;

    return (
      <hr
        onClick={onClick}
        className={cn(styles.separator, {collapsed})}/>
    );
  }
}

export default Separator;
