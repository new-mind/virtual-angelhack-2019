import React from 'react';
import { storiesOf } from '@storybook/react';

import PageTitle from './index';

storiesOf('Blocks|Title', module)
  .addDecorator(storyFn => <div className="p-xxl">{storyFn()}</div> )
  .add('default', () => (
    <PageTitle>Page Title</PageTitle>
  ));
