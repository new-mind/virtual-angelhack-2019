import React from 'react';
import cn from 'classnames';

import styles from './styles.module.scss';

export const PageTitle = ({ children, level = 1, className }) => {
  const Heading = `h${level}`;

  return (
    <Heading className={cn(styles.pageTitle, 'm-0', className)}>{children}</Heading>
  )
};

export default PageTitle;
