import React from 'react';
import { SignOut } from 'aws-amplify-react';

class Logout extends SignOut {
  render() {
    return (
      <div className={this.props.className} onClick={this.signOut}>
        {this.props.children}
      </div>
    );
  }
}

export default Logout;
