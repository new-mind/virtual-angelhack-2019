import React from 'react';
import cn from 'classnames';

import styles from './Badge.module.scss';

class Badge extends React.Component {
  render() {
    const className = cn(
      styles.badge,
      this.props.primary && styles.primary,
      this.props.secondary && styles.secondary,
      'py-xs px-s3',
    );

    return (
      <div className={className}>
        <span className={styles.text}>{this.props.children}</span>
      </div>
    );
  }
}

export default Badge;
