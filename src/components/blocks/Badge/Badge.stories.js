import React from 'react';
import { storiesOf } from '@storybook/react';

import Badge from './index';

storiesOf('Blocks|Badge', module)
  .addDecorator(storyFn => (
    <div className="p-xxl">
      <h1>Badge</h1>
      <p>
        <small>
          <a
            href="https://www.figma.com/file/k7RC6brNRp3mLjAZW0ZNxA/EduWay-2.0?node-id=458%3A5"
            target="_blank"
            rel="noopener noreferrer"
          >
            Look Sources in Design
          </a>
        </small>
      </p>
      {storyFn()}
    </div>
  ))
  .add('Primary', () => (
    <Badge primary>Homework</Badge>
  ))
  .add('Secondary', () => (
    <Badge secondary>Unsaved Changes</Badge>
  ));
