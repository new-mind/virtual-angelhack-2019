import { Card } from 'react-bootstrap';
import styled from 'styled-components';

//TODO: needs to do through scss theme (?)
const StyledCard = styled(Card)`
  background: #F9F9F9;
  box-sizing: border-box;
  box-shadow: 0px 3px 6px rgba(0, 0, 0, 0.2);
  overflow: hidden;
`

StyledCard.Text = styled(Card.Text)`
  color: #231F20;
  font-style: normal;
  font-weight: 300;
  font-size: 16px;
  line-height: 20px;
  letter-spacing: 1.5px;
`

StyledCard.Title = styled(Card.Title)`
  & > a {
    font-style: normal;
    font-weight: bold;
    font-size: 30px;
    line-height: 37px;
    letter-spacing: 5px;
    color: #231F20;
  }
`

export default StyledCard;
