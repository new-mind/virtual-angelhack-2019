import React from 'react';
import cn from 'classnames';
import styles from './styles.module.scss';
import { Nav } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap'

export const Link = (props) => {
  return (
    <LinkContainer
      as={props.as}
      activeClassName={styles['link_active']}
      to={props.to}
      className={cn(props.className, styles.link, 'nav-link')}
    >
      <Nav.Link>
        <span className={styles['link__content']}>
          {props.children}
        </span>
      </Nav.Link>
    </LinkContainer>
  );
};

export default Link;
