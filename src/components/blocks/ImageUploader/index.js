import React from 'react';
import * as _ from 'lodash';
import { Image } from 'react-bootstrap';
import Uploader from 'react-images-upload';

import awsconfig from '@lms/aws-exports';
import Button from '../Button';

import styles from './styles.module.scss';

class ImageUploader extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      imageUrl: this.props.imageUrl
    }
  }

  static getUrl(key) {
    const storage = awsconfig.aws_user_files_s3_bucket;
    return `https://${storage}.s3.amazonaws.com/public/${key}`;
  }

  onImageDrop = (file, base64) => {
    if (!_.size(file)) return;
    this.setState({imageUrl: base64});
    if (this.props.onPick) {
      this.props.onPick(file[0], base64);
    }
  };

  render() {
    const { imageUrl } = this.state;
    const { width, height } = this.props;

    return (
      <div className={styles.container}>
      { imageUrl ?
        <div>
          <div>
            <Image src={imageUrl} thumbnail className="mb-3" width={width || "auto"} height={height || "auto"}/>
          </div>
          <Button className="transparent" onClick={() => {this.setState({imageUrl: null})}}>
            Delete
          </Button>
        </div>
        :
      <Uploader
        withIcon={true}
        buttonText="Choose foreground image"
        buttonClassName="btn btn-dark"
        maxFileSize={5242880}
        onChange={this.onImageDrop}
        singleImage={true}
        />
      }
      </div>
    )
  }
}

export default ImageUploader;
