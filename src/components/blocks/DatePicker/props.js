import PropTypes from 'prop-types';

export const propTypes = {
  className: PropTypes.string,
  label: PropTypes.string,
};

export const defaultProps = {
  label: 'Set Deadline',
};
