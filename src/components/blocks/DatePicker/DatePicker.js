import React from 'react';
import cn from 'classnames';
import _DatePicker from 'react-datepicker';

import "react-datepicker/dist/react-datepicker.css";

import * as props from './props';

import styles from './styles.module.scss';

class DatePicker extends React.Component {
  static propTypes = props.propTypes;
  static defaultProps = props.defaultProps;

  render() {
    return (
      <div className={cn(this.props.className)}>

        <div className={styles.label}>
          <_DatePicker
            selected={this.props.deadline}
            onChange={this.props.onChange}
            customInput={<DateInput />}
            placeholderText={this.props.label}
            isClearable
          />
        </div>
      </div>
    );
  }
}

export default DatePicker;

function DateInput({ value, onClick, placeholder }) {
  return (
    <div onClick={onClick}>
      <i className={styles.icon} />
      { value || placeholder }
    </div>
  );
}
