import React from 'react';
import { Form } from 'react-bootstrap';
import cn from 'classnames';

import styles from './styles.module.scss';

const BootstrapControl = Form.Control;
class Control extends React.Component {
  render() {
    return (
      <BootstrapControl {...this.props} className={cn(styles.control, this.props.className)}/>
    );
  }
}

//TODO
Form.CustomControl = Control;

export default Form;
