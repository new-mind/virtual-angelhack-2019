import React from 'react';
import { Container } from 'react-bootstrap';

import Widget from '@lms/components/widgets';
import AssignmentForm from './AssignmentForm/AssignmentForm';

class Lesson extends React.Component {
  render() {
    const { sections, mode } = this.props;
    return (
      <Container>
        { sections.map(section => (
          <React.Fragment key={section.id}>
            <Widget {...section} mode={mode}/>
          </React.Fragment>
          ))
        }
      </Container>
    )
  }
}

Lesson.AssignmentForm = AssignmentForm;

export default Lesson;
