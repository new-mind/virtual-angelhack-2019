import React from 'react';

import { storiesOf } from '@storybook/react';
import LessonAssignmentForm from './AssignmentForm';

const AssignmentForm = function () {
  const [deadline, setDeadline] = React.useState(new Date());

  return <LessonAssignmentForm deadline={deadline} onChange={setDeadline} />
};

storiesOf('Lesson:AssignmentForm', module)
  .add('By default', () => (<div className="p-xxl"><AssignmentForm /></div>));
