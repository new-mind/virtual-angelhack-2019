import React from 'react';
import cn from 'classnames';
import { ListGroup } from 'react-bootstrap';
import * as _ from 'lodash';

import Button from '@lms/components/blocks/Button';

import styles from './styles.module.scss';
import "react-datepicker/dist/react-datepicker.css";

class AssignmentForm extends React.Component {
  state = {
    _assignedStudents: {},
    _unassignedStudents: {},
  };

  getStudentStatus(student) {
    const { assignedStudents } = this.props;
    const { _assignedStudents, _unassignedStudents } = this.state;

    return {
      assigned: !!assignedStudents[student.id],
      assign: !!_assignedStudents[student.id],
      unassign: !!_unassignedStudents[student.id],
    };
  }

  getAssignedStudents() {
    return _.map(this.state._assignedStudents, (v, k) => k);
  }

  getUnassignedStudents() {
    return _.map(this.state._unassignedStudents, (v, k) => k);
  }

  toggleStudent = (student) => {
    const { assignedStudents } = this.props;
    const { _assignedStudents, _unassignedStudents } = this.state;

    if (assignedStudents[student.id])
      _unassignedStudents[student.id] = !_unassignedStudents[student.id];
    else
      _assignedStudents[student.id] = !_assignedStudents[student.id];

    this.setState({
      _unassignedStudents,
      _assignedStudents,
    })
  };

  render() {
    const { students, noHeader, noFooter } = this.props;

    return (
      <>
        { noHeader ? null :
          <h1 className={cn(styles.title, 'my-2')}>Assign to students</h1>
        }
        <ListGroup className="my-1">
          {_.map(students, (student, i) => (
            <ListGroup.Item
                key={i}
                onClick={() => this.toggleStudent(student) }
                className={cn(styles.item, this.getStudentStatus(student))}>
              <span className={styles.studentName}>{ this.renderStudentInfo(student) }</span>
              <span className={styles.bullet} />
            </ListGroup.Item>
          ))}
        </ListGroup>
        { noFooter ? null :
          <>
            <Button className="my-2 w-100">Assign</Button>
            {!this.props.deadline && (<div className={styles.errorMessage}>
              Deadline not selected
            </div>)}
          </>
        }
      </>
    )
  }

  renderStudentInfo(student) {
    const {name, surname, email} = student;
    if (name) return `${name} ${surname}`;

    if (surname) return surname;
    return email;
  }
}

export default AssignmentForm;
