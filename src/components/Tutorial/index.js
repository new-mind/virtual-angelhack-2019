import React from 'react';
import {withTranslation} from "react-i18next";

import './i18n';
import Modal from '@lms/components/blocks/Modal';
import Button from '@lms/components/blocks/Button';

const VIDEOS = {
  adminLesson: 'https://drive.google.com/file/d/13ZBBDR8oJGThhLqIWxG3YY7T47oUqCXv/preview',
};

class Tutorial extends React.Component {
  state = {
    show: false
  };

  componentDidMount() {
    const tutorialDecl = JSON.parse(localStorage.getItem('tutorial') || '{}');
    if (!tutorialDecl.adminLesson) {
      this.setState({show: true});
    }
  };

  stopShowTutorial = () => {
    const tutorialDecl = JSON.parse(localStorage.getItem('tutorial') || '{}');
    tutorialDecl.adminLesson = true;
    localStorage.setItem('tutorial', JSON.stringify(tutorialDecl));
    this.setState({show: false});
  };

  get video() {
    const { type } = this.props;
    return VIDEOS[type];
  }

  render() {
    const { t } = this.props;
    return (
      <Modal show={this.state.show} size={'lg'} onHide={() => this.setState({show: false})}>
        <Modal.Header>
          <Modal.Title>{t('Tutorial')}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <iframe src={this.video} width="100%" height="300px"/>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={this.stopShowTutorial}>
            {t("Don't show anymore")}
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default withTranslation('components/Tutorial')(Tutorial);
