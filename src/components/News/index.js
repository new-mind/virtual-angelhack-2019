import React from 'react';
import * as _ from 'lodash';
import moment from 'moment';
import cn from 'classnames';

import styles from './styles.module.scss';
import Title from '@lms/components/blocks/PageTitle';

const news = [{
  title: 'Teacher is out July, 3 - 15',
  content: '<strong>I am on vacations July, 3 - 15. We will get back on track as soon as I am back.</strong>',
  createdAt: (new Date()).valueOf()
}, {
  title: 'Spanish business breakfast',
  content: '<strong>Topic:</strong> how to startt your business on the Spanish market. More information is in your inbox.',
  createdAt: (new Date()).valueOf()
}]

class News extends React.Component {
  renderDateTime(timestamp) {
    const m = moment(timestamp);

    return (
      <div className="timestamp">
        <span>{m.format('L')}</span>
        <span className="delim">/</span>
        <span>{m.format('H:mm')}</span>
      </div>
    )
  }

  render() {
    return (
      <div className={cn(styles.container, this.props.className)}>
        <h2><Title>Stay updated</Title></h2>
        {_.map(news, (item, i) => (
          <div key={i}>
            <h3>{item.title}</h3>
            <div dangerouslySetInnerHTML={{__html: item.content}}></div>
            { this.renderDateTime(item.createdAt) }
          </div>
        ))}
      </div>
    );
  }
}

export default News;
