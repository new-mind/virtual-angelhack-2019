import React from 'react';
import { Col, Row } from 'react-bootstrap';
import { Formik } from 'formik';
import cn from 'classnames';
import { withTranslation } from 'react-i18next';

import './i18n';
import Button from '@lms/components/blocks/Button';
import Title from '@lms/components/blocks/PageTitle';
import Form from '@lms/components/blocks/forms';
import * as API from './api';

import styles from './styles.module.scss';

class Profile extends React.Component {
  handleSubmit = (values) => {
    this.props.updateUser({input: values});
  };

  render() {
    const { user, t } = this.props;
    const { name, surname, age, phone } = user;

    return (
      <Row>
        <Col xs={12} sm={12} md={6} lg={6} xl={6}>
          <Row className="my-xl" noGutters>
            <Col xs={12} md={6} lg={5} className={styles.profile}>
              <Title>{t("Profile")}</Title>
            </Col>
          </Row>
          <Row noGutters>
            <Col>
              <Formik onSubmit={this.handleSubmit}>
                {({
                    handleSubmit,
                    handleChange
                  }) => (
                  <Form className={cn(styles.form, "my-5")} noValidate onSubmit={handleSubmit}>
                    <Form.Group>
                      <Form.CustomControl
                        type="text"
                        name="name"
                        defaultValue={name}
                        onChange={handleChange}
                        placeholder={t("First name")}/>
                    </Form.Group>
                    <Form.Group>
                      <Form.CustomControl
                        type="text"
                        name="surname"
                        defaultValue={surname}
                        onChange={handleChange}
                        placeholder={t("Second name")}/>
                    </Form.Group>
                    <Form.Group>
                      <Form.CustomControl
                        type="text"
                        name="phone"
                        defaultValue={phone}
                        onChange={handleChange}
                        placeholder={t("Mobile phone")}/>
                    </Form.Group>
                    <Form.Group>
                      <Form.CustomControl
                        type="text"
                        name="age"
                        defaultValue={age}
                        onChange={handleChange}
                        placeholder={t("Age")}/>
                    </Form.Group>
                    <Form.Group>
                      <Button type="submit">{t("Save")}</Button>
                    </Form.Group>
                  </Form>
                )}
              </Formik>
            </Col>
          </Row>
        </Col>
      </Row>
    );
  }
}

export default
  API.updateUser(
    withTranslation('components/Profile')(Profile)
  );
