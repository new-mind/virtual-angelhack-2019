import gql from 'graphql-tag';
import { graphql } from 'react-apollo';

const UPDATE_USER = gql`
  mutation UpdateUser($input: UpdateUserInput!) {
    updateUser(input: $input) {
      id
      name
      surname
      phone
      age
    }
  }
`;

export const updateUser = graphql(UPDATE_USER, {
  props: ({mutate, ownProps}) => ({
    updateUser: (variables) => {
      return mutate({
        variables: {
          input: {
            id: ownProps.user.id,
            ...variables.input
          }
        },
        optimisticResponse: {
          __typename: "Mutation",
          updateUser: {
            ...ownProps.user,
            ...variables.input
          }
        }
      });
    }
  })
});
