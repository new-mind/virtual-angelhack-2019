import React from 'react';
import { withCookies } from 'react-cookie';

import awsconfig from '@lms/aws-exports';

class Tawk extends React.Component {
  deleteTawkElement() {
    const tawkElement = document.getElementById("tawkElement");
    if (!tawkElement) return;

    window.Tawk_API && window.Tawk_API.hideWidget();
    tawkElement.parentNode.removeChild(tawkElement);

    // Delete anything related to tawk, otherwise new widget would not be loaded
    for (const name in window) {
      if ( window.hasOwnProperty(name) && (name.includes('tawk') || name.includes('Tawk'))) {
        delete window[name];
      }
    }
  }

  renderTawkElement() {
    const { cookies } = this.props;

    this.deleteTawkElement();
    window.Tawk_API = {};
    window.Tawk_LoadStart = new Date();

    const s1 = document.createElement("script");
    s1.id = "tawkElement";
    s1.async = true;
    s1.charset="UTF-8";
    s1.setAttribute("crossorigin", "*");

    const lng = cookies.get("language");
    switch (lng) {
      case "ru": {
        s1.src = awsconfig.tawk_ru;
        break;
      }
      case "en":
      default:
        s1.src = awsconfig.tawk_en;
        break;
    }
    document.body.appendChild(s1);
  }

  render() {
    this.renderTawkElement()
    return null
  }
}

export default withCookies(Tawk);
