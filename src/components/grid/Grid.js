import React, { Component } from 'react';
import chunk from 'lodash/chunk';

const col = {
  display: 'flex',
  flexDirection: 'column' ,
  padding: '2px',
};

const row = {
  display: 'flex',
};

class Row extends Component {
  render() {
    return (
      <div style={row}>
        {this.props.children}
      </div>
    );
  }
}

class Grid extends Component {
  constructor(props) {
    super(props);

    this.state = {};

  }

  render() {
    const { columns = 3, children } = this.props;
    const rows = chunk(children, columns);

    return (
      <div style={col} {...this.props}>
        {rows.map((row, i) => <Row key={i}>{row}</Row>)}
      </div>
    );
  }
}

Grid.propTypes = {};

export default Grid;
