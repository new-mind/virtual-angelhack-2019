import React from 'react';
import { Route as ReactRoute } from 'react-router-dom';

class Route extends React.PureComponent {
  render() {
    const { organizationId, user, path, exact } = this.props;
    const Component = this.props.component;

    return (
      <ReactRoute
        exact={!!exact}
        path={path}
        render={(props) => <Component {...props} user={user} organizationId={organizationId} /> }
      />
    );
  }
}

export default Route
