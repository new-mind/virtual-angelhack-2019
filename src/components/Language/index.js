import React from 'react';
import { withTranslation } from 'react-i18next';
import { withCookies } from 'react-cookie';

import './i18n';
import Switcher from '@lms/components/blocks/Switcher';

class Language extends React.Component {
  setLang(lng) {
    const { i18n, cookies } = this.props;
    i18n.changeLanguage(lng);
    cookies.set("language", lng);
  }

  render() {
    const { t, i18n, className } = this.props;

    return (
      <Switcher
        className={className}
        options={[
          { id: 'ru', title: t('Ru') },
          { id: 'en', title: t('En') }
        ]}
        value={i18n.language}
        onChange={lang => this.setLang(lang)}
      />
    );
  }
}

export default withTranslation('components/Language')(
  withCookies(Language)
);
