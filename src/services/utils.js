export const requireAll = (requireCtx, cb) => {
   requireCtx.keys().forEach(path => cb && typeof cb === 'function' ? cb(requireCtx, path) : requireCtx(path));
};

export const basename = (path) => {
  return path.split('/')[1].split('.')[0];
};
