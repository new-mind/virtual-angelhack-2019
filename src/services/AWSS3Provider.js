import { AWSS3Provider } from '@aws-amplify/storage';
import {
    Hub
} from '@aws-amplify/core';


const AMPLIFY_SYMBOL = ((typeof Symbol !== 'undefined' && typeof Symbol.for === 'function') ?
    Symbol.for('amplify_default') : '@@amplify_default');

const dispatchStorageEvent = (track, event, attrs, metrics, message) => {
  if (track) {
    Hub.dispatch(
      'storage',
      {
        event,
        data: { attrs, metrics },
        message
      },
      'Storage',
      AMPLIFY_SYMBOL
    );
  }
};

export default class CustomAWSS3Provider extends AWSS3Provider {
  async put(key, object, config) {
    const credentialsOK = await this._ensureCredentials();
    if (!credentialsOK) { return Promise.reject('No credentials'); }

    const opt = Object.assign({}, this._config, config);
    const { bucket, track, progressCallback } = opt;
    const { ACL, contentType, contentDisposition, cacheControl, expires, metadata, tagging } = opt;
    const { serverSideEncryption, SSECustomerAlgorithm, SSECustomerKey, SSECustomerKeyMD5, SSEKMSKeyId } = opt;
    const type = contentType ? contentType : 'binary/octet-stream';

    const prefix = this._prefix(opt);
    const final_key = prefix + key;
    const s3 = this._createS3(opt);

    const params: any = {
      Bucket: bucket,
      Key: final_key,
      Body: object,
      ContentType: type
    };
    if (ACL) { params.ACL = ACL; }
    if (cacheControl) { params.CacheControl = cacheControl; }
    if (contentDisposition) { params.ContentDisposition = contentDisposition; }
    if (expires) { params.Expires = expires; }
    if (metadata) { params.Metadata = metadata; }
    if (tagging) { params.Tagging = tagging; }
    if (serverSideEncryption) {
      params.ServerSideEncryption = serverSideEncryption;
      if (SSECustomerAlgorithm) { params.SSECustomerAlgorithm = SSECustomerAlgorithm; }
      if (SSECustomerKey) { params.SSECustomerKey = SSECustomerKey; }
      if (SSECustomerKeyMD5) { params.SSECustomerKeyMD5 = SSECustomerKeyMD5; }
      if (SSEKMSKeyId) { params.SSEKMSKeyId = SSEKMSKeyId; }
    }

    try {
      const upload = s3
        .upload(params)
        .on('httpUploadProgress', progress => {
          if (progressCallback) {
            if (typeof progressCallback === 'function') {
              progressCallback(progress);
            }
          }
        });
      const data = await upload.promise();

      dispatchStorageEvent(
        track,
        'upload',
        { method: 'put', result: 'success' },
        null,
        `Upload success for ${key}`
      );

      return {
        key: data.Key.substr(prefix.length)
      };
    } catch (e) {
      dispatchStorageEvent(
        track,
        'upload',
        { method: 'put', result: 'failed' },
        null,
        `Error uploading ${key}`
      );

      throw e;
    }
  }
}
