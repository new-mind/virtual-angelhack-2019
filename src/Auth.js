class Auth {
  constructor() {
    this.isAuthenticated = true;
  }
}

export default new Auth();
