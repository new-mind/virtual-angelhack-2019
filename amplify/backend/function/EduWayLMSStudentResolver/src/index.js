/* Amplify Params - DO NOT EDIT
You can access the following resource attributes as environment variables from your Lambda function
var environment = process.env.ENV
var region = process.env.REGION
var authCognitoebf636cfUserPoolId = process.env.AUTH_COGNITOEBF636CF_USERPOOLID

Amplify Params - DO NOT EDIT */

const { CognitoIdentityServiceProvider, DynamoDB } = require('aws-sdk');
const { mapUser, mapAttrs } = require('./mappers');

const cognito = new CognitoIdentityServiceProvider();
const dynamodb = new DynamoDB({apiVersion: '2012-08-10'});

const COGNITO_USERPOOL_ID = process.env.AUTH_COGNITOEBF636CF_USERPOOLID;
if (!COGNITO_USERPOOL_ID) {
  throw new Error(`Function requires environment variable: 'COGNITO_USERPOOL_ID'`);
}

//TODO
const STUDENT_DYNAMODB_TABLE =  `StudentInOrganization-x2dsbfp4pjhylfqeh4vgktmumi-${process.env.ENV}`;//process.env.STUDENT_DYNAMODB_TABLE;
if (!STUDENT_DYNAMODB_TABLE) {
  throw new Error(`Function requires environment variable: 'STUDENT_DYNAMODB_TABLE'`);
}

// https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/CognitoIdentityServiceProvider.html
const resolvers = {
  Query: {
    listStudents: async ctx => {
      const params = {
        UserPoolId: COGNITO_USERPOOL_ID,
        GroupName: 'Students',
        Limit: ctx.arguments.limit || 10,
        NextToken: ctx.arguments.nextToken
      }

      const data = await cognito.listUsersInGroup(params).promise()

      const students = data.Users.map(mapUser);

      return {
        items: students,
        nextToken: data.NextToken
      }
    },

    getStudent: async ctx => {
      const params = {
        UserPoolId: COGNITO_USERPOOL_ID,
        Username: ctx.arguments.username
      }

      const res = await cognito.adminGetUser(params).promise();
      return mapUser(res);
    }
  },

  Mutation: {
    createStudent: async ctx => {
      const input = ctx.arguments.input;
      let params = {
        UserPoolId: COGNITO_USERPOOL_ID,
        Username: input.username,
        TemporaryPassword: input.password,
        DesiredDeliveryMediums: ["EMAIL"],
        UserAttributes: mapAttrs({
          email: input.email,
          name: input.name,
          family_name: input.surname
        })
      }

      const res = await cognito.adminCreateUser(params).promise();

      params = {
        GroupName: 'Students',
        UserPoolId: COGNITO_USERPOOL_ID,
        Username: input.username
      }
      await cognito.adminAddUserToGroup(params).promise();

      await dynamodb.putItem({
        TableName: STUDENT_DYNAMODB_TABLE,
        Item: {
          "id": { "S": res.User.Username }
        }
      }).promise();
      return mapUser(res.User);
    },

    deleteStudent: async ctx => {
      const input = ctx.arguments.input;
      let params = {
        UserPoolId: COGNITO_USERPOOL_ID,
        Username: input.username
      }

      const res = await cognito.adminDeleteUser(params).promise();
      await dynamodb.deleteItem({
        TableName: STUDENT_DYNAMODB_TABLE,
        Key: {
          "id": { "S": input.username }
        }
      }).promise();

      return {
        username: input.username,
        email: null,
        enabled: true,
        name: null,
        status: null,
        surname: null
      }
    }
  }
}

// event
// {
//   "typeName": "Query", /* Filled dynamically based on @function usage location */
//   "fieldName": "me", /* Filled dynamically based on @function usage location */
//   "arguments": { /* GraphQL field arguments via $ctx.arguments */ },
//   "identity": { /* AppSync identity object via $ctx.identity */ },
//   "source": { /* The object returned by the parent resolver. E.G. if resolving field 'Post.comments', the source is the Post object. */ },
//   "request": { /* AppSync request object. Contains things like headers. */ },
//   "prev": { /* If using the built-in pipeline resolver support, this contains the object returned by the previous function. */ },
// }
exports.handler = async function (event, ctx) { //eslint-disable-line
  // TODO: handle permission
  const typeHandler = resolvers[event.typeName];
  if (typeHandler) {
    const resolver = typeHandler[event.fieldName];
    if (resolver) {
      let res = await resolver(event);
      return res;
    }
  }
  throw new Error(`Resolver not found. ${event.typeName}::${event.fieldName}`);
};
