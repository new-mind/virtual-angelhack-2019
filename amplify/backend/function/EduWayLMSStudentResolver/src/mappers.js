module.exports = {
  mapUser,
  mapAttrs
}

function mapUser(item) {
  return {
    username: item.Username,
    status: item.UserStatus,
    enabled: item.Enabled,

    email: getAttribute(item, 'email'),
    name: getAttribute(item, 'name'),
    surname: getAttribute(item, 'family_name')
  }
}

function mapAttrs(obj) {
  let list = [];
  for (let k in obj) {
    if (obj[k]) {
      list.push({
        Name: k,
        Value: obj[k]
      })
    }
  }

  return list;
}

function getAttribute(item, key) {
  const attrs = item.Attributes || item.UserAttributes;
  for (let i of attrs) {
    if (i.Name === key) return i.Value;
  }

  return null;
}
