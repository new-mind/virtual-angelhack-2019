module.exports = {
  mapUser,
  mapAttrs
}

function mapUser(item) {
  return {
    username: item.Username,
    status: item.UserStatus,
    enabled: item.Enabled,
    createdAt: item.UserCreateDate,
    updatedAt: item.UserLastModifiedDate,

    email: getAttribute(item, 'email'),
    name: getAttribute(item, 'name'),
    surname: getAttribute(item, 'family_name'),
    phone: getAttribute(item, 'custom:phone'),
    age: getAttribute(item, 'custom:age')
  }
}

function mapAttrs(obj) {
  let list = [];
  for (let k in obj) {
    if (obj[k]) {
      list.push({
        Name: k,
        Value: obj[k]
      })
    }
  }
  return list;
}

//TODO:
function getAttribute(item, key) {
  const attrs = item.Attributes || item.UserAttributes;
  for (let i of attrs) {
    if (i.Name === key) return i.Value;
  }

  return null;
}
