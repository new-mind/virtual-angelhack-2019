/* Amplify Params - DO NOT EDIT
You can access the following resource attributes as environment variables from your Lambda function
var environment = process.env.ENV
var region = process.env.REGION
var authCognitoebf636cfUserPoolId = process.env.AUTH_COGNITOEBF636CF_USERPOOLID

Amplify Params - DO NOT EDIT */

const { CognitoIdentityServiceProvider, DynamoDB } = require('aws-sdk');
const { mapUser, mapAttrs } = require('./mappers');

const cognito = new CognitoIdentityServiceProvider();
const dynamodb = new DynamoDB({apiVersion: '2012-08-10'});

const COGNITO_USERPOOL_ID = process.env.AUTH_COGNITOEBF636CF_USERPOOLID;
if (!COGNITO_USERPOOL_ID) {
  throw new Error(`Function requires environment variable: 'COGNITO_USERPOOL_ID'`);
}

//TODO
const EMPLOYEE_DYNAMODB_TABLE =  `EmployeeInOrganization-x2dsbfp4pjhylfqeh4vgktmumi-${process.env.ENV}`;//process.env.STUDENT_DYNAMODB_TABLE;
if (!EMPLOYEE_DYNAMODB_TABLE) {
  throw new Error(`Function requires environment variable: 'STUDENT_DYNAMODB_TABLE'`);
}
// https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/CognitoIdentityServiceProvider.html
const resolvers = {
  Query: {
    listEmployees: async ctx => {
      const params = {
        UserPoolId: COGNITO_USERPOOL_ID,
        GroupName: 'Employees',
        Limit: ctx.arguments.limit || 10,
        NextToken: ctx.arguments.nextToken
      }

      const data = await cognito.listUsersInGroup(params).promise()
      // TODO: handle errors

      const employees = data.Users.map(mapUser);

      return {
        items: employees,
        nextToken: data.NextToken
      }
    },

    getEmployee: async ctx => {
      const params = {
        UserPoolId: COGNITO_USERPOOL_ID,
        Username: ctx.arguments.username
      }

      const res = await cognito.adminGetUser(params).promise();
      return mapUser(res);
    }
  },

  Mutation: {
    createEmployee: async ctx => {
      const input = ctx.arguments.input;
      let params = {
        UserPoolId: COGNITO_USERPOOL_ID,
        Username: input.username,
        TemporaryPassword: input.password,
        DesiredDeliveryMediums: ["EMAIL"],
        UserAttributes: mapAttrs({
          email: input.email,
          name: input.name,
          family_name: input.surname
        })
      }

      const res = await cognito.adminCreateUser(params).promise();

      params = {
        GroupName: 'Employees',
        UserPoolId: COGNITO_USERPOOL_ID,
        Username: input.username
      }
      await cognito.adminAddUserToGroup(params).promise();

      await dynamodb.putItem({
        TableName: EMPLOYEE_DYNAMODB_TABLE,
        Item: {
          "id": { "S": res.User.Username },
          "teacher": { "BOOL": true },
          "admin": { "BOOL": false }
        }
      }).promise();
      return mapUser(res.User);
    },

    disableEmployee: async ctx => {
      const input = ctx.arguments;
      const params = {
        UserPoolId: COGNITO_USERPOOL_ID,
        Username: input.username
      }

      const res = await cognito.adminDisableUser(params).promise();
      if (res.error)
        throw new Error(res.error);
      return false;
    },

    enableEmployee: async ctx => {
      const input = ctx.arguments;
      const params = {
        UserPoolId: COGNITO_USERPOOL_ID,
        Username: input.username
      }

      const res = await cognito.adminEnableUser(params).promise();
      if (res.error)
        throw new Error(res.error);
      return false;
    },

    setEmployeePassword: async ctx => {
      const input = ctx.arguments.input;
      const params = {
        UserPoolId: COGNITO_USERPOOL_ID,
        Username: input.username,
        Password: input.password,
        Permanent: false
      }

      const res = await cognito.adminEnableUser(params).promise();
      if (res.error)
        throw new Error(res.error);
      return false;
    },

    updateEmployee: async ctx => {
      const input = ctx.arguments.input;
      let params = {
        Username: input.username,
        UserPoolId: COGNITO_USERPOOL_ID,
        UserAttributes: mapAttrs({
          email: input.email,
          "custom:phone": input.phone,
          "name": input.name,
          "family_name": input.surname,
          "custom:age": input.age
        })
      }

      let res = await cognito.adminUpdateUserAttributes(params).promise();
      if (res.error)
        throw new Error(res.error);

      params = {
        UserPoolId: COGNITO_USERPOOL_ID,
        Username: input.username
      }

      res = await cognito.adminGetUser(params).promise();
      return mapUser(res);
    }
  }
};

// event
// {
//   "typeName": "Query", /* Filled dynamically based on @function usage location */
//   "fieldName": "me", /* Filled dynamically based on @function usage location */
//   "arguments": { /* GraphQL field arguments via $ctx.arguments */ },
//   "identity": { /* AppSync identity object via $ctx.identity */ },
//   "source": { /* The object returned by the parent resolver. E.G. if resolving field 'Post.comments', the source is the Post object. */ },
//   "request": { /* AppSync request object. Contains things like headers. */ },
//   "prev": { /* If using the built-in pipeline resolver support, this contains the object returned by the previous function. */ },
// }
exports.handler = async function (event, ctx) { //eslint-disable-line
  // TODO: handle permission
  const typeHandler = resolvers[event.typeName];
  if (typeHandler) {
    const resolver = typeHandler[event.fieldName];
    if (resolver) {
      return await resolver(event);
    }
  }
  throw new Error(`Resolver not found. ${event.typeName}::${event.fieldName}`);
};
